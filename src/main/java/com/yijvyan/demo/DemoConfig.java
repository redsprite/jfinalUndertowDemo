
/**
 * @Title: DemoConfig.java
 * @Package com.yijvyan.demo
 * @Description: TODO(1.jfinal在undertow下开发；)
 * @author Administrator
 * @date 2020-7-30 17:22:10
 * @version V1.0
 */

package com.yijvyan.demo;

import java.util.HashMap;

import org.apache.jasper.deploy.JspPropertyGroup;
import org.apache.jasper.deploy.TagLibraryInfo;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.server.undertow.UndertowServer;
// import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

import io.undertow.jsp.HackInstanceManager;
import io.undertow.jsp.JspServletBuilder;

// import io.undertow.jsp.HackInstanceManager;
// import io.undertow.jsp.JspServletBuilder;

/**
 * @ClassName: DemoConfig
 * @Description: TODO(2.jfinal的启动文件；)
 * @author Administrator
 * @date 2020-7-30
 *
 */
// 18.基于JFinal的web项目需要创建一个继承自JFinalConfig类的子类，该类用于对整个web项目进行配置；JFinalConfig子类需要实现六个抽象方法；
public class DemoConfig extends JFinalConfig {
	/**
	 * 注意：用于启动的 main 方法可以在任意 java 类中创建，在此仅为方便演示 才将 main 方法放在了 DemoConfig 中
	 *
	 * 开发项目时，建议新建一个 App.java 或者 Start.java 这样的专用 启动入口类放置用于启动的 main 方法
	 */
	public static void main(String[] args) {
		// UndertowServer.start(DemoConfig.class, 80, true);

		/*
		 * UndertowServer.create(DemoConfig.class).onStart(builder -> {
		 * builder.setServerOption(UndertowOptions.ALLOW_UNESCAPED_CHARACTERS_IN_URL,
		 * true); }).start();
		 */

		/*
		 * UndertowServer.create(DemoConfig.class).configWeb(builder -> { //
		 * 12.2.1.配置Filter； builder.addFilter("myFilter", "com.abc.MyFilter");
		 * builder.addFilterUrlMapping("myFilter", "/*");
		 * builder.addFilterInitParam("myFilter", "key", "value");
		 * 
		 * // 12.2.2.配置Servlet； builder.addServlet("myServlet", "com.abc.MyServlet");
		 * builder.addServletMapping("myServlet", "*.do");
		 * builder.addServletInitParam("myServlet", "key", "value");
		 * 
		 * // 12.2.3.配置Listener； builder.addListener("com.abc.MyListener");
		 * 
		 * // 12.2.4.配置 WebSocket,MyWebSocket 需要使用 ServletEndpoint 注解；
		 * builder.addWebSocketEndpoint("com.yijvyan.websocket.MyWebSocket");
		 * }).start();
		 */

		/*
		 * UndertowServer.create(DemoConfig.class).configWeb(builder -> {
		 * builder.addWebSocketEndpoint("com.yijvyan.websocket.MyWebSocket");
		 * }).start();
		 */

		// 14.2.jfinal_undertow 支持 jsp 功能需要类似下面的配置；
		// https://localhost:4430/abc/index.jsp 最终的访问地址。
		UndertowServer.create(DemoConfig.class).configWeb(wb -> {
			wb.getDeploymentInfo().addServlet(JspServletBuilder.createServlet("Default Jsp Servlet", "*.jsp"));
			HashMap<String, TagLibraryInfo> tagLibraryInfo = new HashMap<String, TagLibraryInfo>();
			JspServletBuilder.setupDeployment(wb.getDeploymentInfo(), new HashMap<String, JspPropertyGroup>(),
					tagLibraryInfo, new HackInstanceManager());
			wb.addWebSocketEndpoint("com.yijvyan.websocket.MyWebSocket");
		}).start();

		// 16.4.启动jetty；支持jsp访问，不需要进行上面undertow的配置，简单很多。
		// http://localhost/hello
		// JFinal.start("src/main/webapp", 80, "/", 5);

		// System.out.println("13.1.websocket：系统服务启动完成......");
	}

	@Override
	public void configConstant(Constants me) {
		// 19.1.配置开发模式，true
		// 值为开发模式；在开发模式下，JFinal会对每次请求输出报告，如输出本次请求的URL、Controller、Method以及请求所携带的参数。
		me.setDevMode(true);
		// 14.1.配置在浏览器地址栏中无法输入 .jsp 文件名去访问 jsp 文件；设为false后，可以直接访问；配置是否拒绝访问 JSP，是指直接访问
		// .jsp 文件，与 renderJsp(xxx.jsp) 无关
		me.setDenyAccessJsp(false);

		// 19.2.配置 aop 代理使用 cglib，否则将使用 jfinal 默认的动态编译代理方案；
		// me.setToCglibProxyFactory();
		// 19.3.配置依赖注入；
		// me.setInjectDependency(true);
		// 19.4.配置依赖注入时，是否对被注入类的超类进行注入；
		// me.setInjectSuperClass(false);
		// 19.5.配置为 slf4j 日志系统，否则默认将使用 log4j
		// 还可以通过 me.setLogFactory(...) 配置为自行扩展的日志系统实现类；
		// me.setToSlf4jLogFactory();
		// 19.6.设置 Json 转换工厂实现类，更多说明见第 12 章；
		// me.setJsonFactory(new MixedJsonFactory());
		// 19.7.配置视图类型，默认使用 jfinal enjoy 模板引擎；
		// me.setViewType(ViewType.JFINAL_TEMPLATE);
		// 19.8.配置基础下载路径，默认为 webapp 下的 download；
		// me.setBaseDownloadPath("...");
		// 19.9.配置基础上传路径，默认为 webapp 下的 upload；
		// me.setBaseUploadPath("...");
		// 19.10.配置 404、500 页面；
		// me.setError404View("/common/404.html");
		// me.setError500View("/common/500.html");
		// 19.11.配置 encoding，默认为 UTF8；
		// me.setEncoding("UTF-8");
		// 19.12.配置 json 转换 Date 类型时使用的 data parttern；
		// me.setJsonDatePattern("yyyy-MM-dd HH:mm");
		// 19.13.配置上传文件最大数据量，默认 10M；
		// me.setMaxPostSize(10 * 1024 * 1024);
		// 19.14.配置验证码缓存 cache，配置成集中共享缓存可以支持分布式与集群；
		// me.setCaptchaCache("...");
		// 19.15.配置 urlPara 参数分隔字符，默认为 "-"；
		// me.setUrlParaSeparator("-");

		/*
		 * 94.Json 配置 jfinal 官方提供了 Json
		 * 抽象类的三个实现：JFinalJson、FastJson、Jackson，如果不进行配置，那么默认使用 JFinalJson 实现，指定为其它实现需要在
		 * configConstant 进行如下配置：
		 * 
		 * public void configConstant(Constants me) { me.setJsonFactory(new
		 * FastJsonFactory()); } 上面配置将系统默认使用的 JFinalJson 切换到了 FastJson。还可以通过扩展 Json
		 * 抽象类以及 JsonFactory 来实现定制的 Json 实现。
		 * 
		 * 假定用户扩展出了一个 MyJson 与 MyJsonFactory ，那么可以通过如下的方式切换到自己的实现上去：
		 * 
		 * public void configConstant(Constants me) { me.setJsonFactory(new
		 * MyJsonFactory()); }
		 * 
		 * 
		 * 此外，jfinal 官方还提供了 MixedJson、MixedJsonFactory 实现，这个实现让转 json string 时使用
		 * JFinalJson，反向转成对象则使用 FastJson。
		 * 
		 * 
		 * 
		 * 如果希望在非 web 下进行配置，需要使用 JsonManager，例如：
		 * 
		 * JsonManager.me().setDefaultJsonFactory(new MixedJsonFactory());
		 * 
		 * 
		 * 还可以配置 Date 类型转 json 后的格式：
		 * 
		 * public void configConstant(Constants me) {
		 * me.setJsonDatePattern("yyyy-MM-dd"); }
		 * 
		 * 
		 * 注意，在使用 MixedJsonFactory、FastJsonFactory、JacksonFactory 时需要添加其依赖，具体依赖参考下一小节内容。
		 */

		/*
		 * 95.Json 的四个实现 jfinal 官方默认给出了四种 json
		 * 实现：JFinalJson、FastJson、Jackson、MixedJson，可以满足绝大多数需求。
		 * 
		 * 1、JFinalJson JFinalJson 是 jfinal 官方最早的一个实现，这个实现最重要一点就是在转换 jfinal 的 Model
		 * 时是先获取 Model 中的 Map attrs 属性，然后再去转换这个 Map 对象。即便你的 Model 生成了 getter
		 * 方法，也不会被转换时调用。
		 * 
		 * 针对 Model.attrs 属性进行转换而不是利用 getter 方法进行转换有如下几个原因：
		 * 
		 * A：支持多表关联查询结果的转换
		 * 
		 * 无论是 Model 还是传统 Java Bean，其 getter 方法都是固定的，而多表关联查询的 sql 语句中的 select
		 * 中的字段是动态的，通常还包含关联表中的字段，而这些字段值没有相关的 getter 方法，这些字段就无法被转换
		 * 
		 * B：早期的 jfinal 用户没有为 Model 生成 getter 方法
		 * 
		 * 注意：JFinalJson 只支持对象转 json string，不支持反向的 json string 转对象，可以通过使用 MixedJson
		 * 来支持反向转换：me.setJsonFactory(new MixedJsonFactory());
		 * 
		 * 
		 * 
		 * JFinal 4.9 版本对 JFinalJson 进行了彻底的重构与优化，新增了一些功能。
		 * 
		 * A、Model、Record 字段名转换为驼峰格式 JFinalJson.setModelAndRecordFieldNameToCamelCase();
		 * 大量开发者将数据库字段名命名成下划线的格式，如："user_id"，这就造成了与 java 变量名风格的不统一，对代码质量有一定损害。
		 * 
		 * 上述配置只有在碰到下划线时才会对其进行转换，否则原样保留，而 oracle 用户习惯使用大写的字段名，所以需要使用如下配置：
		 * 
		 * JFinalJson.setModelAndRecordFieldNameConverter(fieldName -> { return
		 * StrKit.toCamelCase(fieldName, true); }); 上述代码中的第二个参数 true
		 * 表示将字段名转成小写字母，而无论是否出现下划线。
		 * 
		 * B、新增 setSkipNullValueField 配置方法 该方法用于去除 null 值字段的转换：
		 * 
		 * JFinalJson.setSkipNullValueField(true); C、新增 addToJson 扩展方法
		 * 该方法可以细粒度地对任意类型的转换进行扩展：
		 * 
		 * JFinalJson.addToJson(Timestamp.class, (value, depth, ret) -> {
		 * ret.addLong(((Timestamp)value).getTime()); }); 以上扩展代码，将 Timestamp 类型转换成 long
		 * 值。
		 * 
		 * 
		 * 
		 * 2、FastJson FastJson 是对第三方的 fastjson 进行的二次封装，该实现最重要的一点就是转换依赖于 Model、java bean
		 * 的 getter 方法。使用 fastjson 可以按照其官方文档去配置 fastjson 的各种转换参数。
		 * 
		 * 使用 FastJson 封装时，需要添加其依赖：
		 * 
		 * <dependency> <groupId>com.alibaba</groupId> <artifactId>fastjson</artifactId>
		 * <version>1.2.68</version> </dependency>
		 * 
		 * 
		 * 3、Jackson 该实现与 FastJson 类似，是对第三方的 jackson 的二次封装
		 * 
		 * 使用 Jackson 封装时，需要添加其依赖：
		 * 
		 * <dependency> <groupId>com.fasterxml.jackson.core</groupId>
		 * <artifactId>jackson-databind</artifactId> <version>2.11.0</version>
		 * </dependency>
		 * 
		 * 
		 * 4、MixedJson MixedJson 是对 JFinalJson、FastJson 的再一次封装，Object 转 json string 时使用
		 * JFinalJson 的实现，而反向 json string 转 Object 使用 FastJson。
		 * 
		 * 这个实现结合了 JFinalJson 与 FastJson 两者的优势。 前者不支持 json string 到 Object 的转换，后者不支持关联表
		 * sql 查询动态字段的转换。
		 * 
		 * 使用 MixedJson 封装时需要添加 FastJson 封装的依赖：
		 * 
		 * <dependency> <groupId>com.alibaba</groupId> <artifactId>fastjson</artifactId>
		 * <version>1.2.68</version> </dependency>
		 */

		/*
		 * 96.Json 转换用法 json 转换在 jfinal 中的使用分为两类用法，第一类是使用配置的 json 转换，第二类是指定某个实现进行 json
		 * 转换。
		 * 
		 * 1、使用配置的 json 实现转换
		 * 
		 * 如下代码将使用前面章节中介绍的配置的 json 实现进行转的换：
		 * 
		 * // 在 Controller 中使用 renderJson 进行 json 转换，并渲染给客户端 renderJson();
		 * renderJson(key, object); renderJson(new String[]{...});
		 * 
		 * // 使用 JsonKit 工具类进行 json 转换 JsonKit.toJson(...); JsonKit.parse(...); 2、使用指定的
		 * json 实现转换
		 * 
		 * 如果下代码将使用指定的 json 实现去转换：
		 * 
		 * // 临时指定使用 FastJson 实现 FastJson.getJson().toJson(...);
		 * FastJson.getJson().parse(...);
		 * 
		 * // 为 Controller.renderJson(..) 方法直接传入转换好的 json string
		 * renderJson(FastJson.getJson().toJson(...)); 上面这种用法可以临时摆脱配置的 json 实现，从而使用指定的
		 * json 实现。
		 */

	}

	// 20.此方法用来配置访问路由，如下代码配置了将 "/hello"
	// 映射到HelloController这个控制器，通过以下的配置，http://localhost/abc/hello 将访问
	// HelloController.index() 方法，而http://localhost/abc/hello/methodName 将访问到
	// HelloController.methodName() 方法。
	@Override
	public void configRoute(Routes me) {
		// 20.1.添加路由；
		me.add("/hello", HelloController.class);

		// 20.2.如果要将控制器超类中的 public 方法映射为 action 配置成 true，一般不用配置；
		// me.setMappingSuperClass(false);
		// 20.3. 配置 baseViewPath，可以让 render(...) 参数省去 baseViewPath 这部分前缀；
		// me.setBaseViewPath("/view");
		// 20.4.配置作用于该 Routes 对象内配置的所有 Controller 的拦截器；
		// me.addInterceptor(new FrontInterceptor());

		/*
		 * 20.5. Routes.setBaseViewPath(baseViewPath) 方法用于为该 Routes 内部的所有 Controller
		 * 设置视图渲染时的基础路径，该基础路径与Routes.add(…, viewPath) 方法传入的viewPath以及
		 * Controller.render(view) 方法传入的 view 参数联合组成最终的视图路径，规则如下：
		 * 
		 * finalView = baseViewPath + viewPath + view
		 * 
		 * 注意：当view以 “/” 字符打头时表示绝对路径，baseViewPath 与 viewPath 将被忽略。
		 */

		/*
		 * 20.6.路由配置 API。 Routes 类中添加路由的方法有两个：
		 * 
		 * public Routes add(String controllerKey, Class<? extends Controller>
		 * controllerClass, String viewPath)
		 * 
		 * public Routes add(String controllerKey, Class<? extends Controller>
		 * controllerClass)
		 * 
		 * 第一个参数 controllerKey 是指访问某个 Controller 所需要的一个字符串，该字符串唯一对应一个
		 * Controller，controllerKey仅能定位到Controller。
		 * 
		 * 第二个参数 controllerClass 是该 controllerKey 所对应到的 Controller 。
		 * 
		 * 第三个参数viewPath是指该Controller返回的视图的相对路径(该参数具体细节将在Controller相关章节中给出)。
		 * 当viewPath未指定时默认值为controllerKey。
		 */

		/*
		 * 20.7.极简路由规则。 JFinal 仅有四种路由，路由规则如下表：图略。 从表中可以看出，JFinal 访问一个确切的
		 * Action(Action定义见3.2节) 需要使用 controllerKey 与 method 来精确定位，当 method 省略时默认值为
		 * index。
		 * 
		 * urlPara是为了能在url中携带参数值，urlPara可以在一次请求中同时携带多个值，JFinal默认使用减号“-”来分隔多个值（
		 * 可通过constants. setUrlParaSeparator(String)设置分隔符），在 Controller 中可以通过
		 * getPara(int index) 分别取出这些值。controllerKey、method、urlPara 这三部分必须使用正斜杠“/”分隔。
		 * 
		 * 注意，controllerKey自身也可以包含正斜杠“/”，如“/admin/article”，这样实质上实现了struts2的namespace功能。
		 */

		/*
		 * 20.8.路由拆分、模块化。 JFinal路由还可以进行拆分配置，这对大规模团队开发十分有用，以下是代码示例： public class
		 * FrontRoutes extends Routes { public void config() {
		 * setBaseViewPath("/view/front"); add("/", IndexController.class); add("/blog",
		 * BlogController.class); } }
		 * 
		 * public class AdminRoutes extends Routes { public void config() {
		 * setBaseViewPath("/view/admin"); addInterceptor(new AdminInterceptor());
		 * add("/admin", AdminController.class); add("/admin/user",
		 * UserController.class); } }
		 * 
		 * public class MyJFinalConfig extends JFinalConfig { public void
		 * configRoute(Routes me) { me.add(new FrontRoutes()); // 前台路由 me.add(new
		 * AdminRoutes()); // 后台路由 }
		 * 
		 * public void configConstant(Constants me) {} public void configEngine(Engine
		 * me) {} public void configPlugin(Plugins me) {} public void
		 * configInterceptor(Interceptors me) {} public void configHandler(Handlers me)
		 * {} }
		 * 
		 * 如上三段代码，FrontRoutes 类中配置了系统前台路由，AdminRoutes
		 * 配置了系统后台路由，MyJFinalConfig.configRoute(…)方法将拆分后的这两个路由合并起来。
		 * 使用这种拆分配置不仅可以让MyJFinalConfig文件更简洁，而且有利于大规模团队开发，避免多人同时修改MyJFinalConfig时的版本冲突。
		 * 
		 * FrontRoutes 与 AdminRoutes 中分别使用 setBaseViewPath(…) 设置了各自
		 * Controller.render(view) 时使用的 baseViewPath。
		 * 
		 * AdminRoutes 还通过 addInterceptor(new AdminInterceptor()) 添加了 Routes
		 * 级别的拦截器，该拦截器将拦截 AdminRoutes 中添加的所有 Controller，会在 class
		 * 拦截器之前被调用。这种用法可以避免在后台管理这样的模块中的所有 class
		 * 上使用@Before(AdminInterceptor.class)，减少代码冗余。
		 */

		/*
		 * 20.9.控制器父类中的action映射。 jfinal 3.6 新增了如下配置方法：
		 * 
		 * public void configRoute(Routes me) { me.setMappingSuperClass(false); }
		 * 
		 * 该方法用于配置是否要将控制器父类中的 public 方法映射成 action。默认配置为 false，也即父类中的所有方法都不会成为 action。
		 * 
		 * 注意：由于该配置是 3.6 版才引入，所以老版本 jfinal 项目升级时，如果控制器父类中存在 action 的需要开启这个配置为 true。因为
		 * MsgController 中的 index() 需要被映射成 action 才能正常分发微信服务端的消息。
		 * 
		 * 引入该配置本质是一个性能优化。可以加快项目启动速度。如果 Routes 被拆分成了多个子 Routes，建议在需要该配置的子 Routes
		 * 中进行配置，因为该配置可以在子 Routes 内独立生效，其它没有该配置的 Routes 仍然可以使用到该性能优化，
		 */

		/*
		 * 45.Routes 级别拦截器 Routes级别拦截器是指在Routes中添加的拦截器，如下是示例：
		 **
		 * 
		 * 后端路由
		 * 
		 * public class AdminRoutes extends Routes {
		 * 
		 * public void config() { // 此处配置 Routes 级别的拦截器，可配置多个 addInterceptor(new
		 * AdminAuthInterceptor());
		 * 
		 * add("/admin", IndexAdminController.class, "/index"); add("/admin/project",
		 * ProjectAdminController.class, "/project"); add("/admin/share",
		 * ShareAdminController.class, "/share"); } } 在上例中，AdminAuthInterceptor
		 * 将拦截IndexAdminController、ProjectAdminController、ShareAdminController 中所有的
		 * action 方法。
		 * 
		 * Routes 拦截器在功能上通过一行代码，同时为多个 Controller 配置好相同的拦截器，减少了代码冗余。Routes 级别拦截器将在 Class
		 * 级别拦截器之前被调用。
		 * 
		 */
	}

	// 21.此方法用来配置Template Engine，以下是代码示例：
	@Override
	public void configEngine(Engine me) {
		/*
		 * 21.1.public void configEngine(Engine me) {
		 * me.addSharedFunction("/view/common/layout.html");
		 * me.addSharedFunction("/view/common/paginate.html");
		 * me.addSharedFunction("/view/admin/common/layout.html"); }
		 * 
		 * 上面的方法向模板引擎中添加了三个定义了 template function 的模板文件，更详细的介绍详见 Template Engine 那一章节的内容。
		 * 
		 * 注意：me.setToClassPathSourceFactory()、me.setBaseTemplatePath(...)、me.setDevMode
		 * (...) 这三个配置要放在最前面，因为后续的 me.addSharedFunction(...) 等配置对前面三个配置有依赖。
		 */

		/*
		 * 64.引擎配置 1､配置入口 Enjoy 引擎的配置入口统一在 Engine 类中。Engine 类中提供了一系列 setter
		 * 方法帮助引导配置，减少记忆负担，例如：
		 * 
		 * // 支持模板热加载，绝大多数生产环境下也建议配置成 true，除非是极端高性能的场景 engine.setDevMode(true);
		 * 
		 * // 添加共享模板函数 engine.addSharedFunction("_layout.html");
		 * 
		 * // 配置极速模式，性能提升 13% Engine.setFastMode(true);
		 * 
		 * // jfinal 4.9.02 新增配置：支持中文表达式、中文变量名、中文方法名、中文模板函数名
		 * Engine.setChineseExpression(true);
		 * 注意：me.setToClassPathSourceFactory()、me.setBaseTemplatePath(...)、me.setDevMode
		 * (...) 这三个配置要放在最前面，因为后续的 me.addSharedFunction(...) 等配置对前面三个配置有依赖。
		 * 
		 * jfinal 4.9 新增了 HTML 压缩功能，其配置如下：
		 * 
		 * // 开启 HTML 压缩功能，分隔字符参数可配置为：'\n' 与 ' ' engine.setCompressorOn(' ');
		 * 该功能对于超高并发访问的 web 应用会有益处，节约流量、提升效率。配置完以后生成的 html 内容如下所示：
		 * 
		 * compressed.png
		 * 
		 * 注意：该功能只支持 HTML 压缩，如果 HTML 中混杂着 javascript
		 * 脚本，且其中含中单行注释，或者某些语句缺少分号，则会引起错误，例如下面的 js 代码：
		 * 
		 * // 这里是 js 的注释 var s1 = "hello" var s2 = "james" s3 = s1 + s2 以上 js
		 * 代码中出现了单行注释，还出现了语句结尾缺少分号字符 ';' 的情况，带有这类情况的 HTML 模板，需要使用换行符作为压缩分隔符：
		 * 
		 * // 使用换行字符为分隔字符 engine.setCompressorOn('\n'); 该配置与使用空格字符的压缩率是完全一样的，而且压缩出来的
		 * HTML 可读性也更好，建议使用。如果 HTML 中混杂着 js 脚本，强烈建议使用该配置。
		 * 
		 * jfinal 4.9 新增 addEnum(...) 支持枚举类型
		 * 
		 * // 假定有如下枚举定义 public enum UserType { ADMIN, USER;
		 * 
		 * public String hello() { return "hello"; } }
		 * 
		 * // 添加对该枚举的配置 engine.addEnum(UserType.class);
		 * 
		 * 
		 * // 在模板中的使用方法如下 ### 以下的对象 u 通过 Controller 中的 setAttr("u", UserType.ADMIN) 传递
		 * #if(u == UserType.ADMIN) #(UserType.ADMIN)
		 * 
		 * ### 以下两行代码演示直接方法调用 #(UserType.ADMIN.hello()) #end jfinal 4.9.02
		 * 新增配置，支持中文表达式、中文变量名、中文方法名、中文模板函数名
		 * 
		 * Engine.setChineseExpression(true);
		 * 
		 * 
		 * 2、配置多个 Engine 对象 由于 Enjoy 引擎被设计为多种用途，同一个项目中的不同模块可以使用不同的 Engine
		 * 对象，分别用于不同的用途，那么则需要分别配置，各个 engine 对象的配置彼此独立互不干扰。
		 * 
		 * 例如 jfinal 中的 Controller.render(String) 以及 SQL 管理功能 Db.getSqlPara(...)
		 * 就分别使用了两个不同的 Engine 对象，所以这两个对象需要独立配置，并且配置的地点是完全不同的。
		 * 
		 * 应用于 Controller.render(String) 的 Engine 对象的配置在 configEngine(Engine me) 中进行：
		 * 
		 * public void configEngine(Engine me) { // devMode 配置为 true，将支持模板实时热加载
		 * me.setDevMode(true); }
		 * 
		 * 
		 * 应用于 SQL 管理的 Engine 对象的配置在 configPlugin(Plugins me) 中进行：
		 * 
		 * public void configPlugin(Plugins me) { ActiveRecordPlugin arp = new
		 * ActiveRecordPlugin(...); Engine engine = arp.getEngine();
		 * 
		 * // 上面的代码获取到了用于 sql 管理功能的 Engine 对象，接着就可以开始配置了
		 * engine.setToClassPathSourceFactory(); engine.addSharedMethod(new StrKit());
		 * 
		 * me.add(arp); }
		 * 
		 * 
		 * 常见错误：经常有同学在配置 SQL 管理的 Engine 对象时，是在 configEngine(Engine me) 中进行配置，造成配置错配的问题。
		 * 
		 * 
		 * 
		 * 同理，自己创建出来的 Engine 对象也需要独立配置：
		 * 
		 * Engine engine = Engine.create("myEngine"); engine.setDevMode(true);
		 * 
		 * 
		 * 3、多 Engine 对象管理 使用 Engine.create(engineName) 创建的 engine 对象，可以在任意地方通过
		 * Engine.use(engineName) 很方便获取，管理多个 engine 对象十分方便，例如：
		 * 
		 * // 创建一个 Engine 对象并进行配置 Engine forEmail = Engine.create("forEmail");
		 * forEmail.addSharedMethod(EmailKit.class);
		 * forEmail.addSharedFunction("email-function.txt");
		 * 
		 * // 创建另一个 Engine 对象并进行配置 Engine forWeixin = Engine.create("forWeixin");
		 * forWeixin.addSharedMethod(WeixinKit.class);
		 * forWeixin.addSharedFunction("weixin-function.txt"); 上面代码创建了两个 Engine
		 * 对象，并分别取名为 "forEmail" 与 "forWeixin"，随后就可以分别使用这两个 Engine 对象了：
		 * 
		 * // 使用名为 "forEmail" 的 engine 对象 String ret =
		 * Engine.use("forEmail").getTemplate("email-template.txt").renderToString(...);
		 * System.out.print(ret);
		 * 
		 * // 使用名为 "forWeixin" 的 engine 对象 String ret =
		 * Engine.use("forWeixin").getTemplate("weixin-template.txt").renderToString(...
		 * ); System.out.print(ret); 如上代码所示，通过 Engine.use(...) 方法可以获取到
		 * Engine.create(...) 创建的对象，既很方便独立配置，也很方便独立获取使用。
		 * 
		 * 
		 * 
		 * 4､模板热加载配置 为了达到最高性能 Enjoy 引擎默认对模板解析结果进行缓存，所以模板被加载之后的修改不会生效，如果需要实时生效需要如下配置：
		 * 
		 * engine.setDevMode(true); 配置成 devMode 模式，开发环境下是提升开发效率必须的配置。
		 * 
		 * 由于 Enjoy 的模板解析速度是 freemarker、velocity 这类模板引擎的 7
		 * 倍左右，并且模板解析前也会判断是否被修改，所以绝大部分情况下建议配置成热加载模式：setDevMode(true)，除非你的项目是极端高性能的应用场景。
		 * 
		 * 
		 * 
		 * 5、共享模板函数配置 如果模板中通过 #define 指令定义了 template function，并且希望这些 template function
		 * 可以在其它模板中直接调用的话，可以进行如下配置：
		 * 
		 * // 添加共享函数，随后可在任意地方调用这些共享函数 me.addSharedFunction("/view/common/layout.html");
		 * 以上代码添加了一个共享函数模板文件 layout.html，这个文件中使用了#define指令定义了template
		 * function。通过上面的配置，可以在任意地方直接调用 layout.html 里头的 template function。
		 * 
		 * 
		 * 
		 * 6、从 class path 和 jar 包加载模板配置 如果模板文件在项目的 class path 路径或者 jar 包之内（注意：maven 项目的
		 * src/main/resources 也属于 class path），可以通过me.setToClassPathSourceFactory() 以及
		 * me.setBaseTemplatePath(null) 来实现，以下是代码示例：
		 * 
		 * public void configEngine(Engine me) { me.setDevMode(true);
		 * 
		 * me.setBaseTemplatePath(null); me.setToClassPathSourceFactory();
		 * 
		 * me.addSharedFunction("/view/common/layout.html"); } Enjoy 引擎默认提供了
		 * FileSourceFactory、与 ClassPathSourceFactory 来配置模板加载来源的策略，前者从文件系统中加载模板，后者从
		 * class path 以及 jar 包加载模板。其中前者是默认配置，后者可通过 engine.setToClassPathSourceFactory()
		 * 进行配置。
		 * 
		 * 还可以通过实现 ISourceFactory 接口，扩展出从任意来源加载模板的策略。然后通过 engine.setSourceFactory(...)
		 * 切换到自己扩展的策略上去。
		 * 
		 * 还可以通过实现 ISource 接口，然后通过 engine.get(new MySource(...)) 去加载模板内容。目前已有用户实现
		 * DbSource 从数据库加载模板的功能。
		 * 
		 * 
		 * 
		 * 7、Eclipse 下开发 在 Eclipse 下开发时，可以将 Validation 配置中的 Html Syntax Validator
		 * 中的自动验证去除勾选，因为 eclipse 无法识别 Enjoy 使用的指令，从而会在指令下方显示黄色波浪线，影响美观。具体的配置方式见下图：
		 * 
		 * 16.png
		 * 
		 * 配置完成后注意重启 eclipse 生效，这个操作不影响使用，仅为了代码美观，关爱处女座完美主义者。
		 */
	}

	@Override
	public void configPlugin(Plugins me) {
		/*
		 * 22. 此方法用来配置JFinal的Plugin，如下代码配置了Druid数据库连接池插件与ActiveRecord数据库访问插件。通过以下的配置，
		 * 可以在应用中使用ActiveRecord非常方便地操作数据库。
		 * 
		 * public void configPlugin(Plugins me) { DruidPlugin dp = new
		 * DruidPlugin(jdbcUrl, userName, password); me.add(dp);
		 * 
		 * ActiveRecordPlugin arp = new ActiveRecordPlugin(dp); arp.addMapping("user",
		 * User.class); me.add(arp); }
		 * 
		 * JFinal插件架构是其主要扩展方式之一，可以方便地创建插件并应用到项目中去。
		 */

		/*
		 * 26.1.如下是 PropKit 代码示例：
		 */
		PropKit.use("config.txt");
		String userName = PropKit.get("userName");
		String email = PropKit.get("email");

		// Prop 配合用法
		Prop p = PropKit.use("config.txt");
		Boolean devMode = p.getBoolean("devMode");
		System.out.println("26.2.输出PropKit从config.txt中获取的参数：" + userName + "--" + email + "--" + devMode);

		/*
		 * 26.2.如下是在项目中具体的使用示例：
		 * 
		 * public class AppConfig extends JFinalConfig { public void
		 * configConstant(Constants me) { //
		 * 第一次使用use加载的配置将成为主配置，可以通过PropKit.get(...)直接取值
		 * PropKit.use("a_little_config.txt");
		 * me.setDevMode(PropKit.getBoolean("devMode")); }
		 * 
		 * public void configPlugin(Plugins me) { //
		 * 非第一次使用use加载的配置，需要通过每次使用use来指定配置文件名再来取值 String redisHost =
		 * PropKit.use("redis_config.txt").get("host"); int redisPort =
		 * PropKit.use("redis_config.txt").getInt("port"); RedisPlugin rp = new
		 * RedisPlugin("myRedis", redisHost, redisPort); me.add(rp);
		 * 
		 * // 非第一次使用 use加载的配置，也可以先得到一个Prop对象，再通过该对象来获取值 Prop p =
		 * PropKit.use("db_config.txt"); DruidPlugin dp = new
		 * DruidPlugin(p.get("jdbcUrl"), p.get("user")…); me.add(dp); } }
		 * 
		 * 如上代码所示，PropKit可同时加载多个配置文件，第一个被加载的配置文件可以使用PorpKit.get(…)方法直接操作，
		 * 非第一个被加载的配置文件则需要使用PropKit.use(…).get(…)来操作。
		 * 
		 * PropKit 的使用并不限于在 YourJFinalConfig
		 * 中，可以在项目的任何地方使用。此外PropKit.use(…)方法在加载配置文件内容以后会将数据缓存在内存之中，可以通过PropKit.useless(…
		 * )将缓存的内容进行清除。
		 */

		/*
		 * 48.ActiveRecordPlugin
		 * ActiveRecord是作为JFinal的Plugin而存在的，所以使用时需要在JFinalConfig中配置ActiveRecordPlugin。
		 * 
		 * 以下是Plugin配置示例代码：
		 * 
		 * public class DemoConfig extends JFinalConfig { public void
		 * configPlugin(Plugins me) { DruidPlugin dp = new
		 * DruidPlugin("jdbc:mysql://localhost/db_name", "userName", "password");
		 * me.add(dp); ActiveRecordPlugin arp = new ActiveRecordPlugin(dp); me.add(arp);
		 * arp.addMapping("user", User.class); arp.addMapping("article", "article_id",
		 * Article.class); } }
		 * 以上代码配置了两个插件：DruidPlugin与ActiveRecordPlugin，前者是druid数据源插件，后者是ActiveRecrod支持插件。
		 * ActiveReceord中定义了addMapping(String tableName, Class<? extends Model>
		 * modelClass>)方法，该方法建立了数据库表名到Model的映射关系。
		 * 
		 * 另外，以上代码中arp.addMapping("user", User.class)，表的主键名为默认为 "id"，如果主键名称为 "user_id"
		 * 则需要手动指定，如：arp.addMapping("user", "user_id", User.class)。
		 * 
		 * 重要：以上的 arp.addMapping(...) 映射配置，可以让 jfinal
		 * 生成器自动化完成，不再需要手动添加这类配置，具体用法见文档：https://www.jfinal.com/doc/5-4
		 */

		/*
		 * 55.Dialect多数据库支持
		 * 目前ActiveRecordPlugin提供了MysqlDialect、OracleDialect、PostgresqlDialect、
		 * SqlServerDialect、Sqlite3Dialect、AnsiSqlDialect实现类。
		 * MysqlDialect与OracleDialect分别实现对Mysql与Oracle的支持，AnsiSqlDialect实现对遵守ANSI
		 * SQL数据库的支持。以下是数据库Dialect的配置代码：
		 * 
		 * public class DemoConfig extends JFinalConfig { public void
		 * configPlugin(Plugins me) { ActiveRecordPlugin arp = new
		 * ActiveRecordPlugin(…); me.add(arp); // 配置Postgresql方言 arp.setDialect(new
		 * PostgresqlDialect()); } }
		 */

		/*
		 * 74.EhCachePlugin
		 * EhCachePlugin是作为JFinal的Plugin而存在的，所以使用时需要在JFinalConfig中配置EhCachePlugin，
		 * 以下是Plugin配置示例代码：
		 * 
		 * public class DemoConfig extends JFinalConfig { public void
		 * configPlugin(Plugins me) { me.add(new EhCachePlugin()); } }
		 */

		/*
		 * 80.RedisPlugin
		 * RedisPlugin是作为JFinal的Plugin而存在的，所以使用时需要在JFinalConfig中配置RedisPlugin，
		 * 以下是RedisPlugin配置示例代码：
		 * 
		 * public class DemoConfig extends JFinalConfig { public void
		 * configPlugin(Plugins me) { // 用于缓存bbs模块的redis服务 RedisPlugin bbsRedis = new
		 * RedisPlugin("bbs", "localhost"); me.add(bbsRedis);
		 * 
		 * // 用于缓存news模块的redis服务 RedisPlugin newsRedis = new RedisPlugin("news",
		 * "192.168.3.9"); me.add(newsRedis); } }
		 * 以上代码创建了两个RedisPlugin对象，分别为bbsRedis和newsRedis。
		 * 最先创建的RedisPlugin对象所持有的Cache对象将成为主缓存对象，主缓存对象可通过Redis.use()直接获取，
		 * 否则需要提供cacheName参数才能获取，例如：Redis.use("news")
		 */

		/*
		 * 84.Cron4jPlugin 1､Cron4jPlugin
		 * Cron4jPlugin是作为JFinal的Plugin而存在的，所以使用时需要在JFinalConfig中配置，如下是代码示例：
		 * 
		 * Cron4jPlugin cp = new Cron4jPlugin(); cp.addTask("* * * * *", new MyTask());
		 * me.add(cp); 如上所示创建插件、addTask 传入参数，并添加到 JFinal
		 * 即完成了基本配置，上图所示红色箭头所指的第一个字符串参数是用于任务调度的cron表达式，第二个参数是 Runnable
		 * 接口的一个实现类，Cron4jPlugin会根据cron表达式调用 MyTask 中的run方法。
		 * 
		 * 上例中的 MyTask 除了可以使用 Runnable 实现类以外，还可以使用 ITask 接口的实现类，该接中拥有一个 stop()
		 * 方法，会在调度停止时回调它。
		 * 
		 * 2、cron 表达式 cron 表达式用于定制调度规则。与 quartz 的 cron 表达式不同，Cron4jPlugin 的 cron
		 * 表达式最多只允许五部分，每部分用空格分隔开来，这五部分从左到右依次表示分、时、天、月、周，其具体规则如下：
		 * 
		 * 分 ：从 0 到 59
		 * 
		 * 时 ：从 0 到 23
		 * 
		 * 天 ：从 1 到 31，字母 L 可以表示月的最后一天
		 * 
		 * 月 ：从 1 到
		 * 12，可以别名：jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "
		 * oct", "nov" and "dec"
		 * 
		 * 周 ：从 0 到 6，0 表示周日，6 表示周六，可以使用别名： "sun", "mon", "tue", "wed", "thu", "fri" and
		 * "sat"
		 * 
		 * 
		 * 
		 * 如上五部分的分、时、天、月、周又分别支持如下字符，其用法如下：
		 * 
		 * 数字 n：表示一个具体的时间点，例如 5 * * * * 表示 5 分这个时间点时执行
		 * 
		 * 逗号 , ：表示指定多个数值，例如 3,5 * * * * 表示 3 和 5 分这两个时间点执行
		 * 
		 * 减号 -：表示范围，例如 1-3 * * * * 表示 1 分、2 分再到 3 分这三个时间点执行
		 * 
		 * 星号 *：表示每一个时间点，例如 * * * * * 表示每分钟执行
		 * 
		 * 除号 /：表示指定一个值的增加幅度。例如 * /5表示每隔5分钟执行一次（序列：0:00, 0:05, 0:10, 0:15 等等）。再例如3-18/5
		 * * * * * 是指在从3到18分钟值这个范围之中每隔5分钟执行一次（序列：0:03, 0:08, 0:13, 0:18, 1:03, 1:08 等等）。
		 * 
		 * 在制定 cron 规则时，建议边对照上面的规则边制定表达式。
		 * 
		 * 
		 * 
		 * 常见错误：cron4j在表达式中使用除号指定增加幅度时与linux稍有不同。例如在linux中表达式 10/3 * * * *
		 * 的含义是从第10分钟开始，每隔三分钟调度一次，而在cron4j中需要使用 10-59/3 * * * *
		 * 来表达。避免这个常见错误的技巧是：当需要使用除号指定增加幅度时，始终指定其范围。
		 * 
		 * 基于上面的技巧，每隔2分钟调度一次的表达式为：0-59/2 * * * * 或者 * /2 * * * * ， 而不能是0/2 * * * *
		 * 
		 * 以上规则不是JFinal创造，是linux通用的cron表达式规则（注意不是quartz规则），如果开发者本身具有这方面的知识，用起来会得心应手。
		 * 原始文档链接：http://www.sauronsoftware.it/projects/cron4j/manual.php
		 * 
		 * 
		 * 
		 * 两大疑问：第一个疑问是当某个任务调度抛出了异常，那么这个任务在下次被调度的时间点上还会不会被调度，答案是肯定的，不管什么时候出现异常，
		 * 时间一到调度仍然会被执行。
		 * 
		 * 第二个疑问是假如某个任务执行时间很长，如果这个任务上次调度后直到本次调度到来的时候还没执行完，那么本次调度是否还会进行，答案也是肯定的。
		 * 
		 * 总结一句话就是：每次调度都是独立的，上次调度是否抛出异常、是否执行完，都与本次调度无关。
		 * 
		 * 
		 * 
		 * 特别提醒：Cron4jPlugin的cron表达式与linux一样只有5个部分，与quartz这个项目的7个部分不一样，
		 * 但凡在网上搜索到的7部分cron表达式都不要试图应用在Cron4jPlugin之中。
		 */
	}

	@Override
	public void configInterceptor(Interceptors me) {
		/*
		 * 23.1.配置全局拦截器。configInterceptor 方法用来配置全局拦截器，全局拦截器分为两类：控制层、业务层，以下是配置示例：
		 * 
		 * public void configInterceptor(Interceptors me) { // 以下两行代码配置作用于控制层的全局拦截器
		 * me.add(new AuthInterceptor()); me.addGlobalActionInterceptor(new
		 * AaaInterceptor());
		 * 
		 * // 以下一行代码配置业务层全局拦截器 me.addGlobalServiceInterceptor(new BbbInterceptor()); }
		 * 
		 * 以上 me.add(...) 与 me.addGlobalActionInterceptor(...) 两个方法是完全等价的，都是配置拦截所有
		 * Controller 中 action 方法的拦截器。而 me.addGlobalServiceInterceptor(...)
		 * 配置的拦截器将拦截业务层所有 public 方法。
		 * 
		 * 注意：以上描述中所谓的 "业务层" 本质上是指除了 "控制层" 以外的含义，不一定要求是业务层，也可以是一个除了 controller 以外的任何一个类。
		 * 
		 * 以上方式配置的拦截器可以在方法定义之处通过 @Clear 注解进行移除，具体用法见后续有关 @Clear 注解的章节。
		 */

		/*
		 * 23.2.拦截器配置层次/粒度。Interceptor 配置粒度分为 Global、Routes、Class、Method
		 * 四个层次，其中以上小节配置粒度为全局。Routes、Class 与 Method 级的配置将在后续章节中详细介绍。
		 */
	}

	@Override
	public void configHandler(Handlers me) {
		// 13.2.不拦截websocket；
		// me.add(new UrlSkipHandler("^/myapp.ws", false));

		/*
		 * 24. 此方法用来配置JFinal的Handler，如下代码配置了名为ResourceHandler的处理器，Handler可以接管所有web请求，
		 * 并对应用拥有完全的控制权，可以很方便地实现更高层的功能性扩展。
		 * 
		 * public void configHandler(Handlers me) { me.add(new ResourceHandler()); }
		 * 
		 * 注意：Handler 是全局共享的，所以要注意其中声明的属性的线程安全问题
		 */
	}

	/*
	 * 25.在 JFinalConfig 继承类中可以添加 onStart() 与 onStop()，JFinal
	 * 会在系统启动完成之后以及系统关闭之前分别回调这两个方法：
	 * 
	 * // 系统启动完成后回调 public void onStart() { }
	 * 
	 * // 系统关闭之前回调 public void onStop() { }
	 * 
	 * 这两个方法可以很方便地在项目启动后与关闭前让开发者有机会进行额外操作，如在系统启动后创建调度线程或在系统关闭前写回缓存。
	 * 
	 * 注意：jfinal 3.6 版本之前这两个方法名为：afterJFinalStart() 与
	 * beforeJFinalStop()。为减少记忆成本、代码输入量以及输入手误的概率，jfinal 3.6
	 * 版本改为了目前更简短的方法名。老方法名仍然被保留，仍然可以使用，方便老项目升级到 jfinal 最新版本。
	 */
	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
	}
}
