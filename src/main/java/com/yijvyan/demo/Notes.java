
/**
 * @Title: Notes.java
 * @Package com.yijvyan.demo
 * @Description: TODO(38.1.只是为了学习jfinal新建的文件；)
 * @author yijvyan
 * @date 2020-10-4 1:42:50
 * @version V1.0
 */

package com.yijvyan.demo;

/**
 * @ClassName: Notes
 * @Description: TODO(38.2.用txt格式的话，注释不好看，所以就用java文件了；)
 * @author yijvyan
 * @date 2020-10-4
 *
 */

public class Notes {

	/*
	 * 39.AOP概述：传统AOP实现需要引入大量繁杂而多余的概念，例如：Aspect、Advice、Joinpoint、Poincut、
	 * Introduction、Weaving、Around等等，并且需要引入IOC容器并配合大量的XML或者annotation来进行组件装配。
	 * 
	 * 传统AOP不但学习成本极高，开发效率极低，开发体验极差，而且还影响系统性能，尤其是在开发阶段造成项目启动缓慢，极大影响开发效率。
	 * 
	 * JFinal采用极速化的AOP设计，专注AOP最核心的目标，将概念减少到极致，仅有三个概念：Interceptor、Before、Clear，
	 * 并且无需引入IOC也无需使用啰嗦的XML。
	 */

	/*
	 * 40. Interceptor 1、基本用法 Interceptor 可以对方法进行拦截，并提供机会在方法的前后添加切面代码，实现 AOP
	 * 的核心目标。Interceptor 接口仅仅定义了一个方法 public void intercept(Invocation inv)。以下是简单示例：
	 * 
	 * public class DemoInterceptor implements Interceptor { public void
	 * intercept(Invocation inv) { System.out.println("Before method invoking");
	 * inv.invoke(); System.out.println("After method invoking"); } } 以上代码中的
	 * DemoInterceptor 将拦截目标方法，并且在目标方法调用前后向控制台输出文本。inv.invoke()
	 * 这一行代码是对目标方法的调用，在这一行代码的前后插入切面代码可以很方便地实现AOP。
	 * 
	 * 注意：必须调用 inv.invoke() 方法，才能将当前调用传递到后续的 Interceptor 与 Action。
	 * 
	 * 
	 * 
	 * 常见错误：目前为止仍有很多同学忘了调用 inv.invoke() 方法，造成 controller 中的 action
	 * 不会被执行。在此再次强调一次，一定要调用一次 inv.invoke()，除非是刻意不去调用剩下的拦截器与 action，这种情况仍然需要使用
	 * inv.getController().render()/renderJson() 调用一下相关的 render() 方法为客户端响应数据。
	 * 
	 * 
	 * 
	 * Invocation 作为 Interceptor 接口 intercept 方法中的唯一参数，提供了很多便利的方法在拦截器中使用。以下为
	 * Invocation 中的方法：
	 * 
	 * 15.png
	 * 
	 * 
	 * 
	 * 更正一下上面截图中倒数第三行的一处手误：setArg(int) 应该改为 setArg(int, Object)
	 * 
	 * 
	 * 
	 * 2､ 全局共享，注意线程安全问题 Interceptor 是全局共享的，所以如果要在其中使用属性需要保证其属性是线程安全的，如下代码将是错误的：
	 * 
	 * public class MyInterceptor implements Interceptor {
	 * 
	 * private int value = 123;
	 * 
	 * public void intercept(Invocation inv) { // 多线程将会并发访问 value 值，造成错乱 value++;
	 * 
	 * inv.invoke(); } } 如上代码所示，其中的 value 属性将会被多线程访问到，从而引发线程安全问题。
	 */

	/*
	 * 41.Before注解用来对拦截器进行配置，该注解可配置Class、Method级别的拦截器，以下是代码示例：
	 * 
	 * // 配置一个Class级别的拦截器，她将拦截本类中的所有方法
	 * 
	 * @Before(AaaInter.class) public class BlogController extends Controller {
	 * 
	 * // 配置多个Method级别的拦截器，仅拦截本方法
	 * 
	 * @Before({BbbInter.class, CccInter.class}) public void index() { }
	 * 
	 * // 未配置Method级别拦截器，但会被Class级别拦截器AaaInter所拦截 public void show() { } }
	 * 如上代码所示，Before可以将拦截器配置为Class级别与Method级别，前者将拦截本类中所有方法，后者仅拦截本方法。
	 * 此外Before可以同时配置多个拦截器，只需用在大括号内用逗号将多个拦截器进行分隔即可。
	 * 
	 * 除了 Class 与 Method 级别的拦截器以外，JFinal 还支持全局拦截器以及 Routes
	 * 拦截器，全局拦截器分为控制层全局拦截器与业务层全局拦截器，前者拦截控制 层所有 Action 方法，后者拦截业务层所有方法。
	 * 
	 * 全局拦截器需要在 YourJFinalConfig 进行配置，以下是配置示例：
	 * 
	 * public class YourJFinalConfig extends JFinalConfig { public void
	 * configInterceptor(Interceptors me) { // 添加控制层全局拦截器
	 * me.addGlobalActionInterceptor(new GlobalActionInterceptor());
	 * 
	 * // 添加业务层全局拦截器 me.addGlobalServiceInterceptor(new GlobalServiceInterceptor());
	 * 
	 * // 为兼容老版本保留的方法，功能与addGlobalActionInterceptor完全一样 me.add(new
	 * GlobalActionInterceptor()); } }
	 * 当某个Method被多个级别的拦截器所拦截，拦截器各级别执行的次序依次为：Global、Routes、Class、Method，如果同级中有多个拦截器，
	 * 那么同级中的执行次序是：配置在前面的先执行。
	 */

	/*
	 * 42.Clear 拦截器从上到下依次分为Global、Routes、Class、Method四个层次，Clear用于清除自身所处层次以上层的拦截器。
	 * 
	 * Clear声明在Method层时将针对Global、Routes、Class进行清除。Clear声明在Class层时将针对Global、Routes
	 * 进行清除。Clear注解携带参数时清除目标层中指定的拦截器。
	 * 
	 * Clear用法记忆技巧：
	 * 
	 * 一共有Global、Routes、Class、Method 四层拦截器
	 * 
	 * 清除只针对Clear本身所处层的向上所有层，本层与下层不清除
	 * 
	 * 不带参数时清除所有拦截器，带参时清除参数指定的拦截器
	 * 
	 * 
	 * 
	 * 在某些应用场景之下，需要移除Global或Class拦截器。例如某个后台管理系统，配置了一个全局的权限拦截器，但是其登录action就必须清除掉她，
	 * 否则无法完成登录操作，以下是代码示例：
	 * 
	 * // login方法需要移除该权限拦截器才能正常登录
	 * 
	 * @Before(AuthInterceptor.class) public class UserController extends Controller
	 * { // AuthInterceptor 已被Clear清除掉，不会被其拦截
	 * 
	 * @Clear public void login() { }
	 * 
	 * // 此方法将被AuthInterceptor拦截 public void show() { } }
	 * 
	 * 
	 * Clear注解带有参数时，能清除指定的拦截器，以下是一个更加全面的示例：
	 * 
	 * @Before(AAA.class) public class UserController extends Controller {
	 * 
	 * @Clear
	 * 
	 * @Before(BBB.class) public void login() { //
	 * Global、Class级别的拦截器将被清除，但本方法上声明的BBB不受影响 }
	 * 
	 * @Clear({AAA.class, CCC.class})// 清除指定的拦截器AAA与CCC
	 * 
	 * @Before(CCC.class) public void show() { //
	 * 虽然Clear注解中指定清除CCC，但她无法被清除，因为清除操作只针对于本层以上的各层 } }
	 * 
	 * 
	 * 上面的清除都用在了 method 上，还可以将其用于 class 之上，例如：
	 * 
	 * @Clear(AAA.class) public class UserController { public void index() { ... } }
	 * 如上所示，@Clear(AAA.class) 将清除上层也就是 Global、Route 这两层中配置的 AAA.java 这个拦截器。
	 */

	/*
	 * 43.Inject 依赖注入 使用 @Inject 注解可以向 Controller 以及 Interceptor
	 * 中注入依赖对象，使用注入功能需要如下配置：
	 * 
	 * public void configConstant(Constants me) { // 开启对 jfinal web 项目组件
	 * Controller、Interceptor、Validator 的注入 me.setInjectDependency(true);
	 * 
	 * // 开启对超类的注入。不开启时可以在超类中通过 Aop.get(...) 进行注入 me.setInjectSuperClass(true); }
	 * 以上的 me.setInjectDependency(true) 仅是针于 jfinal 的 web 组件而言的配置。而
	 * Aop.get(...)、Aop.inject(...) 无需配置即可支持注入。
	 * 
	 * 配置完成以后就可以在控制器中使用了，例如：
	 * 
	 * public class AccountController {
	 * 
	 * @Inject AccountService service; // 此处会注入依赖对象
	 * 
	 * public void index() { service.justDoIt(); // 调用被注入对象的方法 } }
	 * 
	 * 
	 * @Inject 还可以用于拦截器的属性注入，例如：
	 * 
	 * public class MyInterceptor implements Interceptor {
	 * 
	 * @Inject Service service; // 此处会注入依赖对象
	 * 
	 * public void intercept(Invocation inv) { service.justDoIt(); // 调用被注入对象的方法
	 * inv.invoke(); } }
	 * 
	 * 
	 * 特别注意：使用 Inject 注入的前提是使用 @Inject 注解的类的对象的创建是由 jfinal 接管的，这样 jfinal
	 * 才有机会对其进行注入。例如 Controller、Interceptor、Validator 的创建是 jfinal
	 * 接管的，所以这三种组件中可以使用 @Inject 注入。
	 * 
	 * 此外：注入动作可以向下传递。例如在 Controller 中使用 @Inject 注入一个 AaaService，那么在 AaaService
	 * 中可以使用 @Inject 注入一个 BbbService，如此可以一直向下传递进行注入.
	 * 
	 * 
	 * 
	 * 如果需要创建的对象并不是 jfinal 接管的，那么可以使用 Aop.get(...) 方法进行依赖对象的创建以及注入，例如：
	 * 
	 * public class MyKit {
	 * 
	 * static Service service = Aop.get(Service.class);
	 * 
	 * public void doIt() { service.justDoIt(); } } 由于 MyKit 的创建并不是 jfinal
	 * 接管的，所以不能使用 @Inject 进行依赖注入。 而 Controller、Interceptor 的创建和组装是由 jfinal
	 * 接管的，所以可以使用 @Inject 注入依赖。
	 * 
	 * 有了 Aop.get(...) 就可以在任何地方创建对象并且对创建的对象进行注入。此外还可以使用 Aop.inject(...)
	 * 仅仅向对象注入依赖但不创建对象。
	 * 
	 * @Inject 注解还支持指定注入的实现类，例如下面的代码，将为 Service 注入 MyService 对象：
	 * 
	 * @Inject(MyService.class) Service service; 当 @Inject(...) 注解不指定被注入的类型时，还可以通过
	 * AopManager.me().addMapping(...) 事先添加映射来指定被注入的类型，例如：
	 * 
	 * AopManager.me().addMapping(Service.class, MyService.class); 通过上面的映射，下面的代码将会为
	 * Service 注入 MyService
	 * 
	 * public class IndexController {
	 * 
	 * @Inject Service service;
	 * 
	 * public void index() { service.justDoIt(); } }
	 */

	/*
	 * 44.Aop 工具 1、Aop 1.1、get(...) Aop.get(...) 可以在任意时空创建对象并且对其进行依赖注入，例如：
	 * 
	 * Service service = Aop.get(Service.class); 以上代码会创建 Service 对象，如果 Service
	 * 中使用了 @Before 配置过拦截器，那么会生效，如果 Service 中的属性使用了 @Inject，则会被注入依赖对象。
	 * 
	 * 1.2、inject(...) Aop.inject(...) 可以在任意时空对目标对象进行注入，该方法相对于 Aop.get(...)
	 * 方法少一个对象创建功能：
	 * 
	 * Service service = new Service(...); Aop.inject(service); 以上代码将会对 Service
	 * 类中使用 @Inject 注解过的属性进行依赖注入。
	 * 
	 * 
	 * 
	 * 2、AopManager AopManager 用于管理 Aop 的各种配置
	 * 
	 * 2.1、addMapping(...) addMapping 用于建立接口、抽象类到其实现类之间的映射关系，例如：
	 * 
	 * AopManager.me().addMapping(Service.class, MyService.class); 通过上面的映射，下面的代码将会为
	 * Serivce 创建 MyService 对象，而非 Service 对象：
	 * 
	 * // 这里获取到的是 MyService 对象 Aop.get(Service.class);
	 * 
	 * // 这里被注入的是 MyService 对象
	 * 
	 * @Inject Service service; AopManager.me().addMapping(...)
	 * 的用途是为接口、抽象类指定被注入的具体实现类。
	 * 
	 * 2.2、addSingletonObject(...) 由于 Aop 创建对象时不支持为构造方法传递参数，所以还需提供
	 * addSingletonObject(...) 添加单例对象：
	 * 
	 * // Service 类的构造方法中传入了两个参数 Service service = new Service(paraAaa, paraBbb);
	 * AopManager.me().addSingletonObject(service); 上面代码添加完成以后，可以在任何地方通过下面的方式获取单例对象：
	 * 
	 * // 获取时使用单例对象 service = Aop.get(Service.class);
	 * 
	 * // 注入时也可以使用前面配置的单例对象
	 * 
	 * @Inject Service service; 在添加为单例对象之前还可以先为其注入依赖对象：
	 * 
	 * Service service = new Service(paraAaa, paraBbb); // 这里是对 Service 进行依赖注入
	 * Aop.inject(service);
	 * 
	 * // 为单例注入依赖以后，再添加为单例供后续使用 AopManager.me().addSingletonObject(service);
	 * 2.3、setAopFactory(...) setAopFactory(...) 用于用户扩展出 AopFactory 实现类，实现更多扩展性功能，例如
	 * jboot
	 * 项目中对于注入远程访问对象的扩展：https://gitee.com/fuhai/jboot/blob/master/src/main/java/io/
	 * jboot/aop/JbootAopFactory.java ，JbootAopFactory.java 中的 doInjectRPC
	 * 即注入远程过程调用的实现类。
	 */

	/*
	 * 46.Proxy 动态代理是 jfinal AOP 的底层实现机制。jfinal 4.0 版本新增了 com.jfinal.proxy 模块用于消除对
	 * cglib/asm 的依赖来实现动态代理。
	 * 
	 * proxy 模块需要运行在 JDK 环境之下，如果需要运行在 JRE 之下，可以添加如下配置来支持：
	 * 
	 * public void configConstant(Constants me) {
	 * 
	 * // 4.6 之前的版本的配置方式: me.setProxyFactory(new CglibProxyFactory());
	 * me.setToCglibProxyFactory(); // 4.6 版本新增配置方式 } 上面的配置将切换到 cglib 对 proxy
	 * 模块的实现，需要在 pom.xml 中添加其 maven 依赖：
	 * 
	 * <dependency> <groupId>cglib</groupId> <artifactId>cglib-nodep</artifactId>
	 * <version>3.2.5</version> </dependency>
	 * 
	 * 
	 * 如果是在 "非 web 环境" 下使用，配置方法如下：
	 * 
	 * // 4.6 之前的版本的配置方式: ProxyManager.me().setProxyFactory(new
	 * CglibProxyFactory()); ProxyManager.me().setToCglibProxyFactory();
	 */

	/*
	 * 47.ActiveRecord概述 重大更新：自 jfinal 3.0 起，添加了 sql 管理模块，比 mybatis 使用 XML 管理 sql
	 * 的方案要爽得多，快速查看：http://www.jfinal.com/doc/5-13
	 * 
	 * ActiveRecord 是 JFinal 最核心的组成部分之一，通过 ActiveRecord 来操作数据库，将极大地减少代码量，极大地提升开发效率。
	 * 
	 * ActiveRecord 模式的核心是：一个 Model 对象唯一对应数据库表中的一条记录，而对应关系依靠的是数据库表的主键值。
	 * 
	 * 因此，ActiveRecord 模式要求数据库表必须要有主键。当数据库表没有主键时，只能使用 Db + Record 模式来操作数据库。
	 */

	/*
	 * 49.Model：1、基本用法 Model是ActiveRecord中最重要的组件之一，它充当MVC模式中的Model部分。以下是Model定义示例代码：
	 * 
	 * public class User extends Model<User> { public static final User dao = new
	 * User().dao(); }
	 * 以上代码中的User通过继承Model，便立即拥有的众多方便的操作数据库的方法。在User中声明的dao静态对象是为了方便查询操作而定义的，
	 * 该对象并不是必须的。基于ActiveRecord的Model无需定义属性，无需定义getter、setter方法，无需XML配置，
	 * 无需Annotation配置，极大降低了代码量。
	 * 
	 * 以下为Model的一些常见用法：
	 * 
	 * // 创建name属性为James,age属性为25的User对象并添加到数据库 new User().set("name",
	 * "James").set("age", 25).save();
	 * 
	 * // 删除id值为25的User User.dao.deleteById(25);
	 * 
	 * // 查询id值为25的User将其name属性改为James并更新到数据库 User.dao.findById(25).set("name",
	 * "James").update();
	 * 
	 * // 查询id值为25的user, 且仅仅取name与age两个字段的值 User user =
	 * User.dao.findByIdLoadColumns(25, "name, age");
	 * 
	 * // 获取user的name属性 String userName = user.getStr("name");
	 * 
	 * // 获取user的age属性 Integer userAge = user.getInt("age");
	 * 
	 * // 查询所有年龄大于18岁的user List<User> users =
	 * User.dao.find("select * from user where age>18");
	 * 
	 * // 分页查询年龄大于18的user,当前页号为1,每页10个user Page<User> userPage =
	 * User.dao.paginate(1, 10, "select *", "from user where age > ?", 18); 以上用法将
	 * dao 对象声明在了 model 中仅为方便展示，在实际应用中应该将 dao 对象放在 Service 中，并且让其成为 private，这样可以保障
	 * sql 以及数据库操作被限定在 service 层中。可以通过下载官网首页的 jfinal_demo_for_maven 来参考 dao 在
	 * Service 层中的用法。
	 * 
	 * 特别注意：User中定义的 public static final User
	 * dao对象是全局共享的，只能用于数据库查询，不能用于数据承载对象。数据承载需要使用new User().set(…)来实现。
	 * 
	 * 
	 * 
	 * 2、常见错误 有不少用户经常在使用 model.find(....) 这类方法时碰到 NullPointerException 异常，通常是由于该
	 * model 没有使用 ActionRecordPlugin.addMapping(....) 进行过映射。
	 * 建议通过生成器自动化生成映射，无需手工添加这类代码，生成器在本站首页下载 jfinal demo，里面有提供。
	 */

	/*
	 * 50.Generator与JavaBean 1、生成器的使用 ActiveRecord 模块的
	 * com.jfinal.plugin.activerecord.generator 包下，提供了一个 Generator 工具类，可自动生成
	 * Model、BaseModel、MappingKit、DataDictionary 四类文件。
	 * 
	 * 生成后的 Model 与 java bean 合体，立即拥有了 getter、setter 方法，使之遵守传统的 java bean 规范，立即拥有了传统
	 * JavaBean 所有的优势，开发过程中不再需要记忆字段名。
	 * 
	 * 使用生成器通常只需配置Generator的四个参数即可，以下是具体使用示例：
	 * 
	 * // base model 所使用的包名 String baseModelPkg = "model.base"; // base model 文件保存路径
	 * String baseModelDir = PathKit.getWebRootPath() + "/../src/model/base";
	 * 
	 * // model 所使用的包名 String modelPkg = "model"; // model 文件保存路径 String modelDir =
	 * baseModelDir + "/..";
	 * 
	 * Generator gernerator = new Generator(dataSource, baseModelPkg, baseModelDir,
	 * modelPkg, modelDir); // 在 getter、setter 方法上生成字段备注内容
	 * gernerator.setGenerateRemarks(true); gernerator.generate();
	 * baseModelPackageName、baseModelOutputDir、modelPackageName、modelOutputDir。
	 * 四个参数分别表示 base model 的包名，baseModel的输出路径，model 的包名，model 的输出路径。
	 * 
	 * 可在官网下载jfinal-demo项目，其中的生成器可直接用于项目：http://www.jfinal.com
	 * 
	 * 生成器的各部分组件都可以扩展，例如，MetaBuilder 可以指定 table
	 * 的过滤规则：https://jfinal.com/feedback/7290
	 * 
	 * 
	 * 
	 * 2、相关生成文件
	 * BaseModel是用于被最终的Model继承的基类，所有的getter、setter方法都将生成在此文件内，这样就保障了最终Model的清爽与干净，
	 * BaseModel不需要人工维护，在数据库有任何变化时重新生成一次即可。
	 * 
	 * MappingKit用于生成table到Model的映射关系，并且会生成主键/复合主键的配置，也即无需在configPlugin(Plugins
	 * me)方法中书写任何样板式的映射代码。
	 * 
	 * DataDictionary是指生成的数据字典，会生成数据表所有字段的名称、类型、长度、备注、是否主键等信息。
	 * 
	 * 3、Model与Bean合体后主要优势 充分利用海量的针对于Bean设计的第三方工具，例如jackson、freemarker
	 * 
	 * 快速响应数据库表变动，极速重构，提升开发效率，提升代码质量
	 * 
	 * 拥有IDE代码提示不用记忆数据表字段名，消除记忆负担，避免手写字段名出现手误
	 * 
	 * BaseModel设计令Model中依然保持清爽，在表结构变化时极速重构关联代码
	 * 
	 * 自动化table至Model映射
	 * 
	 * 自动化主键、复合主键名称识别与映射
	 * 
	 * MappingKit承载映射代码，JFinalConfig保持干净清爽
	 * 
	 * 有利于分布式场景和无数据源时使用Model
	 * 
	 * 新设计避免了以往自动扫描映射设计的若干缺点：引入新概念(如注解)增加学习成本、性能低、jar包扫描可靠性与安全性低
	 * 
	 * 4、Model与Bean合体后注意事项
	 * 合体后JSP模板输出Bean中的数据将依赖其getter方法，输出的变量名即为getter方法去掉”get”前缀字符后剩下的字符首字母变小写，
	 * 如果希望JSP仍然使用之前的输出方式，可以在系统启动时调用一下ModelRecordElResolver.
	 * setResolveBeanAsModel(true);
	 * 
	 * Controller之中的getModel()需要表单域名称对应于数据表字段名，而getBean()则依赖于setter方法，
	 * 表单域名对应于setter方法去掉”set”前缀字符后剩下的字符串字母变小写。
	 * 
	 * 许多类似于jackson、fastjson的第三方工具依赖于Bean的getter方法进行操作，所以只有合体后才可以使用jackson、fastjson
	 * 
	 * JFinalJson将Model转换为json数据时，json的keyName是原始的数据表字段名，而jackson、
	 * fastjson这类依赖于getter方法转化成的json的keyName是数据表字段名转换而成的驼峰命名
	 * 
	 * 建议mysql数据表的字段名直接使用驼峰命名，这样可以令json的keyName完全一致，也可以使JSP在页面中取值时使用完全一致的属性名。注意：
	 * mysql数据表的名称仍然使用下划线命名方式并使用小写字母，方便在linux与windows系统之间移植。
	 * 
	 * 总之，合体后的Bean在使用时要清楚使用的是其BaseModel中的getter、setter方法还是其Model中的get(String
	 * attrName)方法
	 * 
	 * 5、常见问题解决 Sql
	 * Server数据库在使用生成器之时，会获取到系统自带的表，需要对这些表进行过滤，具体办法参考：http://www.jfinal.com/share/
	 * 211
	 */

	/*
	 * 51.独创Db + Record模式 1､常见用法
	 * Db类及其配套的Record类，提供了在Model类之外更为丰富的数据库操作功能。使用Db与Record类时，无需对数据库表进行映射，
	 * Record相当于一个通用的Model。以下为Db + Record模式的一些常见用法：
	 * 
	 * // 创建name属性为James,age属性为25的record对象并添加到数据库 Record user = new
	 * Record().set("name", "James").set("age", 25); Db.save("user", user);
	 * 
	 * // 删除id值为25的user表中的记录 Db.deleteById("user", 25);
	 * 
	 * // 查询id值为25的Record将其name属性改为James并更新到数据库 user = Db.findById("user",
	 * 25).set("name", "James"); Db.update("user", user);
	 * 
	 * // 获取user的name属性 String userName = user.getStr("name"); // 获取user的age属性
	 * Integer userAge = user.getInt("age");
	 * 
	 * // 查询所有年龄大于18岁的user List<Record> users =
	 * Db.find("select * from user where age > 18");
	 * 
	 * // 分页查询年龄大于18的user,当前页号为1,每页10个user Page<Record> userPage = Db.paginate(1,
	 * 10, "select *", "from user where age > ?", 18);
	 * 
	 * 
	 * 以下为事务处理示例：
	 * 
	 * boolean succeed = Db.tx(new IAtom(){ public boolean run() throws SQLException
	 * { int count = Db.update("update account set cash = cash - ? where id = ?",
	 * 100, 123); int count2 =
	 * Db.update("update account set cash = cash + ? where id = ?", 100, 456);
	 * return count == 1 && count2 == 1; }});
	 * 以上两次数据库更新操作在一个事务中执行，如果执行过程中发生异常或者run()方法返回false，则自动回滚事务。
	 * 
	 * 2、Db.query(...) 第一种用法：当 select 后的字段只有一个时，可以使用合适的泛型接收数据：
	 * 
	 * List<String> titleList = Db.query("select title from blog"); 以上 sql 中 select
	 * 后面只有一个 title 字段，所以使用 List<String> 来接收数据。接收数据的泛型变量可根据返回值类理来变动，例如当前返回值为 Integer
	 * 时，代码如下：
	 * 
	 * List<Integer> idList = Db.query("select id from blog"); 以上 sql 中的字段 id 返回值为
	 * Integer，所以接收变量为 List<Integer>
	 * 
	 * 第二种用法：当 select 后的字段有多个时，必须使用 List<Object[]> 接收数据，例如：
	 * 
	 * List<Object[]> list = Db.query("select id, title, content from blog");
	 * List<Object[]> list = Db.query("select * from blog");
	 * 
	 * 
	 * 3、Db.queryXxx(...) Db.queryXxx 系方法有：queryInt、queryLong、queryStr
	 * 等等，这些方法对于使用聚合函数这类的 sql 十分方便，例如：
	 * 
	 * int total = Db.queryInt("select count(*) from account"); 以上 sql 使用了 count(*)
	 * 聚合函数，使用 Db.queryInt 不仅方便而且性能是最好的。
	 * 
	 * 除了聚合函数以外，还可以用于查询某条记录的某个字段值，例如：
	 * 
	 * String nickName =
	 * Db.queryStr("select nickName from account where id = ? limit 1", 123); 以上代码通过
	 * queryStr 可以很方便去查询 id 值为 123 的 account 的 nickName。
	 * 
	 * 至此可以看出来，Db.queryXxx 系方法要求 select 后面必须只能有一个字段名，或者说只能返回一个 column 值（例如
	 * count(*)）。
	 * 
	 * 
	 * 
	 * 4、Db.find(...) 系与 Db.query(...)/Db.queryXxx(...) 系的区别 前者将返回值一律封装到一个 Record
	 * 对象中，而后者不封装，只将数据原样返回。查询所使用的 sql 与参数用法完全一样。
	 * 
	 * 
	 * 
	 * 5､扩展 Db 功能 Db 工具类所有功能都依赖于底层的 DbPro，而 DbPro 可以通过继承来定制自己想要的功能，例如：
	 * 
	 * public class MyDbPro extends DbPro { public MyDbPro(String configName) {
	 * super(configName); }
	 * 
	 * public List<Record> find(String sql, Object... paras) {
	 * System.out.println("Sql: " + sql); System.out.println("Paras: " +
	 * Arrays.toString(paras)); return super.find(sql, paras); } } 以上代码扩展了 DbPro
	 * 并覆盖了父类的 find(String, Object...) 方法，该方法在调用 super.find(...) 之前输出了 sql 及其 para
	 * 值。
	 * 
	 * 然后配置一下即可让 MyDbPro 取代 DbPro 的功能：
	 * 
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
	 * arp.setDbProFactory(configName -> new MyDbPro(configName)); 通过如上配置，在使用
	 * Db.find(String, Object...) 方法时用到的将是自己在 MyDbPro 中实现的 find 方法。通过此方法可以替换、增强、改变所有
	 * DbPro 中 public、protected 方法的行为，极为灵活方便
	 */

	/*
	 * 52.paginate 分页 1、常用 paginate Model 与 Db 中提供了最常用的分页API：paginate(int
	 * pageNumber, int pageSize, String select, String sqlExceptSelect, Object...
	 * paras)
	 * 
	 * 其中的参数含义分别为：当前页的页号、每页数据条数、sql语句的select部分、sql语句除了select以外的部分、查询参数。
	 * 绝大多数情况下使用这个API即可。以下是使用示例：
	 * 
	 * dao.paginate(1, 10, "select *", "from girl where age > ? and weight < ?", 18,
	 * 50); 2、sql 最外层带 group by 的 paginate API 原型：paginate(int pageNumber, int
	 * pageSize, boolean isGroupBySql, String select, String sqlExceptSelect,
	 * Object... paras)，相对于第一种仅仅多了一个boolean isGroupBySql参数，以下是代码示例：
	 * 
	 * dao.paginate(1, 10, true, "select *", "from girl where age > ? group by age",
	 * 18); 以上代码中 sql 的最外层有一个 group by age，所以第三个参数 isGroupBySql 要传入 true 值。
	 * 
	 * 如果是嵌套型sql，但是 group by 不在最外层，那么第三个参数必须为 false，例如：select * from (select x from
	 * t group by y) as temp。
	 * 
	 * 再次强调：isGroupBy 参数只有在最外层 sql 具有 group by 子句时才能为 true 值，嵌套 sql 中仅仅内层具有 group by
	 * 子句时仍然要使用 false。
	 * 
	 * 
	 * 
	 * 3、paginateByFullSql API 原型：paginateByFullSql(int pageNumber, int pageSize,
	 * String totalRowSql, String findSql, Object... paras)。
	 * 
	 * 相对于其它 paginate API，将查询总行数与查询数据的两条sql独立出来，这样处理主要是应对具有复杂order
	 * by语句或者select中带有distinct的情况，只有在使用第一种paginate出现异常时才需要使用该API，以下是代码示例：
	 * 
	 * String from = "from girl where age > ?"; String totalRowSql =
	 * "select count(*) " + from; String findSql = "select * " + from +
	 * " order by age"; dao.paginateByFullSql(1, 10, totalRowSql, findSql, 18);
	 * 上例代码中的order by子句并不复杂，所以仍然可以使用第一种API搞定。
	 * 
	 * 重点：paginateByFullSql 最关键的地方是 totalRwoSql、findSql 这两条 sql 要能够共用最后一个参数
	 * Object... paras，相当于 dao.find(totalRwoSql, paras) 与 dao.find(findSql, paras)
	 * 都要能正确执行，否则断然不能使用 paginateByFullSql。
	 * 
	 * 当 paginate、paginateByFullSql 仍然无法满足业务需求时，可以通过使用 Model.find、Db.query
	 * 系列方法组合出自己想要的分页方法。jfinal 只为最常见场景提供支持。
	 * 
	 * 
	 * 
	 * 4、使用SqlPara 参数的 paginate API原型： paginate(int pageNumber, int pageSize,
	 * SqlPara sqlPara)，用于配合sql管理功能使用，将在sql管理功能那章做介绍。
	 * 
	 * 
	 * 
	 * 5、常见问题解决 首先必须要介绍一下 paginate 底层的基本实现原理，才能有效呈现和解决问题，假定有如下分页代码：
	 * 
	 * paginate(1, 5, "select *", "from article where id > ? order by id", 10);
	 * 底层首先会利用上面第三个与第四个 String 参数生成如下 sql 去获取分页所需要的满足查询条件的所有记录数，也叫 totalRow：
	 * 
	 * "select count(*)" + "from article where id > 10" 注意看上面的 sql，第一部分的
	 * "select count(*)" 是固定写死的，第二部分是根据用户的第四个参数，去除 order by id 而得到的。
	 * 
	 * 去除 order by 子句这部分，一是因为很多数据库根本不支持 select count(*) 型 sql 带有 order by
	 * 子句，必须要去掉否则出错。二是因为 select count(*) 查询在有没有 order by 子句时结果是完全一样的，所以去除后有助于提升性能。
	 * 
	 * 
	 * 
	 * 第一类错误： 如果用户分页的第二个参数不是 "select *" ，而是里面带有一个或多个问号占位符，这种情况下最后面的 para 部分不仅仅只有上例中的
	 * 10 这一个参数
	 * 
	 * 以下是这种问题的一个例子：
	 * 
	 * paginate(1, 5, "select (... where x = ?)", "from article where id=?", 8, 10)
	 * 注意看上面的例子中有两个问号占位符，对应的参数值是 8 和 10。但是生成的用于计算 totalRow 的代码如下：
	 * 
	 * queryLong("select count(*) from article where id=?", 8, 10); 因此，多出来一个参数 8
	 * 是多余的，从而造成异常。出现这种情况只需要在外层再套一个 sql 就可解决：
	 * 
	 * paginate(1, 5, "select *", "from (" + 原sql在此 + ") as t", 8, 10); 也就是将原来的 sql
	 * 外层再套一个 select * from (...) as t ，让第三个参数中没有问号占位符而是一个 "select *"
	 * 。这样处理就避免掉了第一个问号占位符被生成 totalRow 的 sql 吃掉了。
	 * 
	 * 
	 * 
	 * 第二类错误： 如果 order by 子句使用了子查询，或者使用了函数调用，例如：
	 * 
	 * paginate(1, 5, "select *", "from ... order by concat(...)"); 如上所示，order by
	 * 子句使用了 concat 函数，由于 jfinal 是使用了一个简单的正则来移除 order by 子句的，但是无法完全移除带有函数的 order by
	 * 子句，也就是移除不干净，结果就是 sql 是错误的。
	 * 
	 * jfinal 也曾使用复杂的正则来将 order by 子句移除干净，但性能低得无法忍受，最后只能取舍使用简单正则。 由于 order by
	 * 子句可以是极为复杂的嵌套 sql ，所以要移除干净的代价就是性能的急剧下降，jfinal 也是不得以这样设计。
	 * 
	 * 所以，解决第二类常见错误就是使用 paginateByFullSql 方法，这个方法让你的计算 totalRow 的 sql 与查询数据的 sql
	 * 完全手写，jfinal 也就不再需要处理 select 部分与移除 order by 这部分，全交由用户自己手写。
	 * 
	 * 
	 * 
	 * 综上，只要了解了 paginate 在底层的工作机制，解决问题就很容易了。最终可以通过 paginateByFullSql 来稍多写点代码来解决。
	 */

	/*
	 * 53.数据库事务处理 1、Db.tx 事务 在 Db 工具类里面，提供了一个系列的 tx(...) 方法支持数据库事务，以下是 Java 8 的
	 * lambda 语法使用示例：
	 * 
	 * Db.tx(() -> { Db.update("update t1 set f1 = ?", 123);
	 * Db.update("update t2 set f2 = ?", 456); return true; }); 以上代码中的两个 Db.update
	 * 数据库操作将开启事务，return true 提交事务，return false 则回滚事务。Db.tx(...)
	 * 做事务的好处是控制粒度更细，也即不必抛出异常即可回滚。
	 * 
	 * Db.tx 方法 默认针对主数据源 进行事务处理，如果希望对其它数据源开启事务，使用 Db.use(configName).tx(...)
	 * 即可。此外，Db.tx(...) 还支持指定事务级别：
	 * 
	 * Db.tx(Connection.TRANSACTION_SERIALIZABLE, () -> { Db.update(...); new
	 * User().setNickName("james").save(); return true; }); 以上代码中的 Db.tx(...)
	 * 第一个参数传入了事务级别参数 Connection.TRANSACTION_SERIALIZABLE，该方法对于需要灵活控制事务级的场景十分方便实用。
	 * 
	 * 注意：MySql数据库表必须设置为InnoDB引擎时才支持事务，MyISAM并不支持事务。
	 * 
	 * 
	 * 
	 * 2､声明式事务 ActiveRecord支持声明式事务，声明式事务需要使用ActiveRecordPlugin提供的拦截器来实现，
	 * 拦截器的配置方法见Interceptor有关章节。以下代码是声明式事务示例：
	 * 
	 * // 本例仅为示例, 并未严格考虑账户状态等业务逻辑
	 * 
	 * @Before(Tx.class) public void trans_demo() { // 获取转账金额 Integer transAmount =
	 * getParaToInt("transAmount"); // 获取转出账户id Integer fromAccountId =
	 * getParaToInt("fromAccountId"); // 获取转入账户id Integer toAccountId =
	 * getParaToInt("toAccountId"); // 转出操作
	 * Db.update("update account set cash = cash - ? where id = ?", transAmount,
	 * fromAccountId); // 转入操作
	 * Db.update("update account set cash = cash + ? where id = ?", transAmount,
	 * toAccountId); } 以上代码中，仅声明了一个Tx拦截器即为action添加了事务支持。
	 * 
	 * 
	 * 
	 * 当事务拦截器 Tx 配置在 Controller 层，并且希望使用 try catch 对其进行响应控制，在 jfinal 3.6
	 * 及更高版本中可以像下面这样使用：
	 * 
	 * @Before(Tx.class) public void trans { try { service.justDoIt(...);
	 * render("ok.html"); } catch (Exception e) { render("error.html"); throw e; } }
	 * 如上所示，只需要在 catch 块中直接使用 render(....) 就可以在异常发生时指定响应的模板。最后一定要使用 throw e
	 * 将异常向上抛出，处于上层的 Tx 拦截器才能感知异常并回滚事务。（注意：这个功能是 jfinal 3.6 才添加的）
	 * 
	 * 
	 * 
	 * 除此之外ActiveRecord还配备了TxByActionKeys、TxByActionKeyRegex、TxByMethods、
	 * TxByMethodRegex，分别支持actionKeys、actionKey正则、actionMethods、actionMethod正则声明式事务，
	 * 以下是示例代码：
	 * 
	 * public void configInterceptor(Interceptors me) { me.add(new
	 * TxByMethodRegex("(.*save.*|.*update.*)")); me.add(new TxByMethods("save",
	 * "update"));
	 * 
	 * me.add(new TxByActionKeyRegex("/trans.*")); me.add(new
	 * TxByActionKeys("/tx/save", "/tx/update")); }
	 * 上例中的TxByRegex拦截器可通过传入正则表达式对action进行拦截，当actionKey被正则匹配上将开启事务。
	 * TxByActionKeys可以对指定的actionKey进行拦截并开启事务，TxByMethods可以对指定的method进行拦截并开启事务。
	 * 
	 * 特别注意：声明式事务默认只针对主数据源进行回滚，如果希望针对 “非主数据源” 进行回滚，需要使用注解进行配置，以下是示例：
	 * 
	 * @TxConfig("otherConfigName")
	 * 
	 * @Before(Tx.class) public void doIt() { ... } 以上代码中的 @TxConfig 注解可以配置针对
	 * otherConfigName 进行回滚。Tx 拦截器是通过捕捉到异常以后才回滚事务的，所以上述代码中的 doIt() 方法中如果有 try catch
	 * 块捕获到异常，必须再次抛出，才能让 Tx 拦截器感知并回滚事务。
	 * 
	 * 
	 * 
	 * 3、使用技巧 建议优先使用 Db.tx(...)
	 * 做数据库事务，一是该方式可以让事务覆盖的代码量最小，性能会最好。二是该方式可以利用返回值来控制是否回滚事务，而 Tx
	 * 拦截器只能通过捕捉到异常来回滚事务。三是 Java 8 的 lambda 语法使其代码也很简洁。
	 * 
	 * Tx 事务拦截器在捕捉到异常后回滚事务，会再次抛向外抛出异常，所以在使用 Tx 拦截器来做事务处理时，通常需要再额外添加一个
	 * ExceptionInterceptor，放在 Tx 拦截器之前去再次捕捉到 Tx 所抛出的异常，然后在这里做 renderJson/render
	 * 之类的动作向客户端展示不同的数据与页面。如果不添加这样的机制，会展示一个统一默认的 500 error 页面，无法满足所有需求。
	 * 
	 * 综上，强烈建议优先使用 Db.tx(...) 做事务处理。
	 * 
	 * 4、事务级别与性能 JDBC
	 * 默认的事务级别为：Connection.TRANSACTION_READ_COMMITTED。为了避免某些同学的应用场景下对事务级别要求较高，jfinal
	 * 的 ActiveRecordPlugin 默认使用的是
	 * Connection.TRANSACTION_REPEATABLE_READ，但这在对某个表存在高并发锁争用时性能会下降，
	 * 这时可以通过配置事务级别来提升性能：
	 * 
	 * public void configPlugin(Plugins me) { ActiveRecordPlugin arp = new
	 * ActiveRecordPlugin(...);
	 * arp.setTransactionLevel(Connection.TRANSACTION_REPEATABLE_READ); me.add(arp);
	 * } 有一位同学就碰到了由事务级别引起的性能问题：
	 * 
	 * http://www.jfinal.com/feedback/4703?p=1#reply_start
	 */

	/*
	 * 54.Cache 缓存 1、使用 Ehcache 缓存 ActiveRecord 可以使用缓存以大大提高性能，默认的缓存实现是
	 * ehcache，使用时需要引入 ehcache 的 jar 包及其配置文件，以下代码是Cache使用示例：
	 * 
	 * public void list() { List<Blog> blogList = Blog.dao.findByCache("cacheName",
	 * "key", "select * from blog"); setAttr("blogList",
	 * blogList).render("list.html"); }
	 * 上例findByCache方法中的cacheName需要在ehcache.xml中配置如：<cache name="cacheName"
	 * …>。此外Model.paginateByCache(…)、Db.findByCache(…)、Db.paginateByCache(…)
	 * 方法都提供了cache支持。在使用时，只需传入cacheName、key以及在ehccache.xml中配置相对应的cacheName就可以了。
	 * 
	 * 
	 * 
	 * 2、使用任意缓存实现 除了要把使用默认的 ehcache 实现以外，还可以通过实现 ICache
	 * 接口切换到任意的缓存实现上去，下面是个简单提示意性代码实现：
	 * 
	 * public class MyCache implements ICache { public <T>T get(String cacheName,
	 * Object key) { }
	 * 
	 * public void put(String cacheName, Object key, Object value) { }
	 * 
	 * public void remove(String cacheName, Object key) { }
	 * 
	 * public void removeAll(String cacheName) { } } 如上代码所示，MyCache 需要实现 ICache
	 * 中的四个抽象方法，然后通过下面的配置方式即可切换到自己的 cache 实现上去：
	 * 
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(...); arp.setCache(new
	 * MyCache()); 如上代码所示，通过调用 ActiveRecordPlugin.setCache(...) 便可切换 cache 实现。
	 */
	/*
	 * 56.表关联操作 JFinal ActiveRecord
	 * 天然支持表关联操作，并不需要学习新的东西，此为无招胜有招。表关联操作主要有两种方式：一是直接使用sql得到关联数据；
	 * 二是在Model中添加获取关联数据的方法。
	 * 
	 * 假定现有两张数据库表：user、blog，并且user到blog是一对多关系，blog表中使用user_id关联到user表。
	 * 如下代码演示使用第一种方式得到user_name：
	 * 
	 * public void relation() { String sql =
	 * "select b.*, u.user_name from blog b inner join user u on b.user_id=u.id where b.id=?"
	 * ; Blog blog = Blog.dao.findFirst(sql, 123); String name =
	 * blog.getStr("user_name"); } 以下代码演示第二种方式在Blog中获取相关联的User以及在User中获取相关联的Blog：
	 * 
	 * public class Blog extends Model<Blog>{ public static final Blog dao = new
	 * Blog().dao();
	 * 
	 * public User getUser() { return User.dao.findById(get("user_id")); } }
	 * 
	 * public class User extends Model<User>{ public static final User dao = new
	 * User().dao();
	 * 
	 * public List<Blog> getBlogs() { return
	 * Blog.dao.find("select * from blog where user_id=?", get("id")); } } 上面代码在具体的
	 * Model 中 new 了一个 dao 对象出来，这种用法仅用于表关联操作，其它情况的 dao 对象应该让 Service 层持有。
	 */

	/*
	 * 57.复合主键 JFinal ActiveRecord 从 2.0 版本开始，采用极简设计支持复合主键，对于 Model
	 * 来说需要在映射时指定复合主键名称，以下是具体例子：
	 * 
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin); //
	 * 多数据源的配置仅仅是如下第二个参数指定一次复合主键名称 arp.addMapping("user_role", "userId, roleId",
	 * UserRole.class);
	 * 
	 * //同时指定复合主键值即可查找记录 UserRole.dao.findByIds(123, 456);
	 * 
	 * //同时指定复合主键值即可删除记录 UserRole.dao.deleteByIds(123, 456);
	 * 
	 * 
	 * 如上代码所示，对于Model来说，只需要在添加Model映射时指定复合主键名称即可开始使用复合主键，
	 * 在后续的操作中JFinal会对复合主键支持的个数进行检测，当复合主键数量不正确时会报异常，尤其是复合主键数量不够时能够确保数据安全。
	 * 复合主键不限定只能有两个，可以是数据库支持下的任意多个。
	 * 
	 * 对于 Db + Record 模式来说，复合主键的使用不需要配置，直接用即可：
	 * 
	 * Db.findByIds("user_role", "roleId, userId", 123, 456);
	 * Db.deleteByIds("user_role", "roleId, userId", 123, 456);
	 */

	/*
	 * 58.Oracle支持
	 * Oracle数据库具有一定的特殊性，JFinal针对这些特殊性进行了一些额外的支持以方便广大的Oracle使用者。以下是一个完整的Oracle配置示例：
	 * 
	 * public class DemoConfig extends JFinalConfig { public void
	 * configPlugin(Plugins me) { DruidPlugin dp = new DruidPlugin(……); me.add(dp);
	 * //配置Oracle驱动，使用 DruidPlugin 时可以省略下面这行配置
	 * dp.setDriverClass("oracle.jdbc.driver.OracleDriver");
	 * 
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(dp); me.add(arp); //
	 * 配置Oracle方言 arp.setDialect(new OracleDialect()); // 配置属性名(字段名)大小写不敏感容器工厂
	 * arp.setContainerFactory(new CaseInsensitiveContainerFactory());
	 * arp.addMapping("user", "user_id", User.class); } }
	 * 
	 * 
	 * 由于Oracle数据库会自动将属性名(字段名)转换成大写，所以需要手动指定主键名为大写，如：arp.addMaping(“user”, “ID”,
	 * User.class)。如果想让ActiveRecord对属性名（字段名）
	 * 的大小写不敏感可以通过设置CaseInsensitiveContainerFactory来达到，有了这个设置，则arp.addMaping(“user”,
	 * “ID”, User.class)不再需要了。
	 * 
	 * 
	 * 
	 * 另外，Oracle并未直接支持自增主键，JFinal为此提供了便捷的解决方案。要让Oracle支持自动主键主要分为两步：一是创建序列，
	 * 二是在model中使用这个序列，具体办法如下：
	 * 
	 * 1：通过如下办法创建序列，本例中序列名为：MY_SEQ
	 * 
	 * CREATE SEQUENCE MY_SEQ INCREMENT BY 1 MINVALUE 1 MAXVALUE 9999999999999999
	 * START WITH 1 CACHE 20; 2：在YourModel.set(…)中使用上面创建的序列
	 * 
	 * // 创建User并使用序列 User user = new User().set("id", "MY_SEQ.nextval").set("age",
	 * 18); user.save(); // 获取id值 Integer id = user.get("id"); 序列的使用很简单，只需要
	 * yourModel.set(主键名, 序列名 + “.nextval”)就可以了。特别注意这里的 “.nextval”
	 * 后缀一定要是小写，OracleDialect对该值的大小写敏感。
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 注意：Oracle下分页排序Sql语句必须满足2个条件：
	 * 
	 * Sql语句中必须有排序条件；
	 * 
	 * 排序条件如果没有唯一性，那么必须在后边跟上一个唯一性的条件，比如主键
	 * 
	 * 相关博文：http://database.51cto.com/art/201010/231533.htm
	 * 
	 * 相关反馈：http://www.jfinal.com/feedback/64#replyContent
	 */

	/*
	 * 59.Enjoy SQL 模板 JFinal利用自带的 Enjoy Template Engine 极为简洁的实现了 Sql
	 * 模板管理功能。一如既往的极简设计，仅有 #sql、#para、#namespace 三个指令，学习成本依然低到极致。
	 * 
	 * 重要：除了以上三个 sql 管理专用指令以外，jfinal 模板引擎的所有指令和功能也可以用在 sql 管理，jfinal 模板引擎用法见第 6
	 * 章：http://www.jfinal.com/doc/6-1
	 * 
	 * 1、基本配置 在ActiveRecordPlugin中使用sql管理功能示例代码如下：
	 * 
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
	 * arp.addSqlTemplate("all.sql"); _MappingKit.mapping(arp); me.add(arp);
	 * 如上例所示，ar.addSqlTemplate("all.sql") 将从 class path 或者 jar 包中读取 "all.sql" 文件。
	 * 
	 * 可以通过多次调用addSqlTemplate来添加任意多个外部 sql 文件，并且对于不同的 ActiveRecordPlugin
	 * 对象都是彼此独立配置的，有利于多数据源下对 sql 进行模块化管理。
	 * 
	 * 可以将 sql 文件放在maven项目下的 src/main/resources 之下，编译器会自动将其编译至 class path
	 * 之下，进而可以被读取到，打包进入 jar 包中以后也可以被读到。
	 * 
	 * 如果希望在开发阶段可以对修改的sql文件实现热加载，可以配置 arp.setDevMode(true)，如果不配置则默认使用
	 * configConstant中的 me.setDevMode(…) 配置。
	 * 
	 * 特别注意：sql 管理模块使用的 Engine 对象并非在 configEngine(Engine me)配置，因此在对其配置 shared
	 * method、directive 等扩展时需要使用 activeRecordPlugin.getEngine() 先得到 Engine 对象，然后对该
	 * Engine 对象进行配置。
	 * 
	 * 2、#sql 指令 #sql 指令用于定义 sql 模板，如下是代码示例：
	 * 
	 * #sql("findGirl") select * from girl where age > ? and age < ? and weight < 50
	 * #end 上例通过 #sql 指令在模板文件中定义了 sqlkey 为 "findGirl" 的 sql 模板，在java 代码中的获取方式如下：
	 * 
	 * String sql = Db.getSql("findGirl"); Db.find(sql, 16, 23); 上例中第一行代码通过
	 * Db.getSql() 方法获取到定义好的sql语句，第二行代码直接将 sql 用于数据库查询。
	 * 
	 * 此外，还可以通过 Model.getSql(key) 方法来获取sql语句，功能与Db.getSql(key) 完全一样。
	 * 
	 * 3、#para 指令 3.1 使用 int 常量 #para(int) #para 指令用于生成 sql
	 * 模板中的问号占位符以及问号占位符所对应的参数值，两者分别保存在 SqlPara对象的 sql 和 paraList 属性之中。
	 * 
	 * #para指令支持两种用法，一种是传入 int型常量参数 的用法，如下示例展示的是 int 型常量参数的用法：
	 * 
	 * #sql("findGirl") select * from girl where age > #para(0) and weight <
	 * #para(1) #end 上例代码中两个 #para 指令，传入了两个 int 型常量参数，所对应的 java 后端代码必须调用
	 * getSqlPara(String key, Object… paras)，如下是代码示例：
	 * 
	 * // Db.template 用法（jfinal 4.0 新增） Db.template("findGirl", 18, 50).find();
	 * 
	 * // Model.template 用法完全一样，以下假定 girl 为 Model girl.template("findGirl", 18,
	 * 50).find();
	 * 
	 * // getSqlPara 用法 SqlPara sqlPara = Db.getSqlPara("findGirl", 18, 50);
	 * Db.find(sqlPara); 以上第一行代码中的 18 与 50 这两个参数，分别被前面 #sql 指令中定义的 #para(0) 与
	 * #para(1) 所使用。
	 * 
	 * Db.template(String key, Object... paras) 与 Db.getSqlPara(String key,
	 * Object... paras) 方法的第二个参数 Object... paras，在传入实际参数时，下标值从 0 开始算起与 #para(int)
	 * 指令中使用的 int 型常量一一对应。
	 * 
	 * jfinal 4.0 新增的 template(...) 用法与 getSqlPara(...) 所接受的参数完全一样，所以两者在本质上完全一样。
	 * 
	 * 新增的 template(...) 方法仅仅是为了减少代码量，提升开发体验，在功能上与 getSqlPara 完全一样，对于已经熟悉 getSqlPara
	 * 用法的同学不会增加学习成本。
	 * 
	 * 3.2 使用非 int 常量 #para(expr) #para 指令的另一种用法是传入除了 int 型常量以外的任意表达式参数
	 * (注意：两种用法处在同一个 #sql 模板之中时只能选择其中一种)，如下是代码示例：
	 * 
	 * #sql("findGirl") select * from girl where age > #para(age) and weight <
	 * #para(weight) #end 与上例模板文件配套的java代码如下所示：
	 * 
	 * // 构造参数 Kv cond = Kv.by("age", 18).set("weight", 50);
	 * 
	 * // 使用 Db 的 template 方法 Db.template("findGirl", cond).find();
	 * 
	 * // 使用 Model 的 template 方法，以下假定 girl 为 Model girl.template("findGirl",
	 * cond).find(); 上例代码获取到的 SqlPara 对象 sqlPara 中封装的 sql 为：select * from girl where
	 * age > ? and weight < ?，封装的与 sql 问号占位符次序一致的参数列表值为：[18, 50]。
	 * 
	 * 3.3 #para(int) 与 #para(expressioin) 比较 指令参数：#para(int) 参数必须传入 int
	 * 型常量，#para(expression) 参数是除了 int 型常量以外的任意表达式
	 * 
	 * java 参数：template 的第二个参数，对应 #para(int) 时必须是 Object... paras，对应
	 * #para(expression) 时必须是 Map。
	 * 
	 * #para(int) 用法示例：
	 * 
	 * // #para(int) 用法 #sql("findGirl") select * from girl where age > #para(0) and
	 * weight < #para(1) #end
	 * 
	 * // 对应于 #para(int) 指令，第二个参数必须是 Object... paras Db.template("findGirl", 18,
	 * 50).find();
	 * 
	 * // Model.template 使用方法完全一样 girl.template("findGirl", 18, 50).find();
	 * 
	 * 
	 * #para(expression) 用法示例：
	 * 
	 * // #para(expression) 用法 #sql("findGirl") select * from girl where age >
	 * #para(age) and weight < #para(weight) #end
	 * 
	 * // 构造 Nap 参数，下面的 Kv 是 Map 的子类 Kv cond = Kv.by("age", 18,).set("weight", 50);
	 * 
	 * // 对应于 #para(expression) 指令，第二个参数必须是 Map 或者其子类 Db.template("findGirl",
	 * cond).find()
	 * 
	 * // Model.template 使用方法完全一样 girl.template("findGirl", cond).find()
	 * 简单一句话：#para(int) 用下标获取参数值，#para(expr) 用名称获取参数值，所对应的
	 * getSqlPara(...)、template(...) 方法参数自然就是 Object... 与 Map。
	 * 
	 * 以上两个示例，获取到的 SqlPara 对象中的值完全一样。其中的sql值都为：select * from girl where age > ? and
	 * weight < ?，其中的参数列表值也都为 [18、50]。
	 * 
	 * 重要： #para
	 * 指令所到之处永远是生成一个问号占位符，并不是参数的值，参数值被生成在了SqlPara对象的paraList属性之中，通过sqlPara.getPara()
	 * 可获取。如果想生成参数值用一下模板输出指令即可：#(value)
	 * 
	 * 极其重要的通用技巧：如果某些数据库操作 API 不支持 SqlPara 参数，而只支持 String sql 和 Object… paras
	 * 这两个参数，可以这样来用：method(sqlPara.getSql(), sqlPara.getPara())。这样就可以让所有这类API都能用上
	 * Sql 管理功能。
	 * 
	 * 加餐：有些同学希望在 sql 文件中获取 template(String, Object... paras)、getSqlPara(String,
	 * Object… paras) 方法传入的paras参数，可以通过表达式 _PARA_ARRAY_[index] 来获取到下标为index的参数值。
	 * 
	 * 
	 * 
	 * 由于经常有人问到 mysql 数据库 sql 语句的 like 子句用法，补充如下示例：
	 * 
	 * #sql("search") select * from article where title like concat('%',
	 * #para(title), '%') #end 以上示例的like用法完全是 JDBC 决定的，JFinal仅仅是生成了如下sql而已：
	 * 
	 * select * from article where title like concat('%', ?, '%')，也就是仅仅将
	 * #para(title) 替换生成为一个问号占位 ”?” 而已。
	 * 
	 * 4、#namespace 指令 #namespace 指令为 sql
	 * 语句指定命名空间，不同的命名空间可以让#sql指令使用相同的key值去定义sql，有利于模块化管理，如下所示：
	 * 
	 * #namespace("japan") #sql("findGirl") select * from girl where age > #para(0)
	 * and age < #para(1) and weight < 50 #end #end
	 * 上面代码指定了namespace为”japan”，在使用的时候，只需要在key前面添加namesapce值前缀 + 句点符 + key即可：
	 * 
	 * Db.template("japan.findGirl", 17, 21).find(); 5、template() 与 getSqlPara()
	 * 为了进一步减少代码量，提升开发体验，jfinal 4.0 新增了 template 方法，该方法在本质上与 getSqlPara
	 * 用法完全一样，所以传递的参数完全一样，都是：String sqlKey、Object... paras 或者 String sqlKey、Map
	 * paras。
	 * 
	 * 在 template 方法可以使用链式调用直接去查询，而 getSqlPara 则是先得到 sql + para ，然后再将其传递给查询方法进行查询。
	 * 
	 * 6、templateByString() 如果希望将 sql 模板放在 String 变量之中而不是放在外部的文件之中，可以使用
	 * templateByString 方法，例如：
	 * 
	 * // 将 sql 模板存放在 String 变量中，注意：此时不再需要 #sql、#namespace 指令 String sqlTemplate =
	 * "select * from girl where age > #para(0) and weight < #para(1)"
	 * 
	 * // templateByString 将从 String 变量中加载模板 Db.templateByString(sqlTemplate, 18,
	 * 50).find();
	 * 
	 * // Model 也支持 templateByString 方法 girl.templateByString(sqlTemplate, 18,
	 * 50).find(); templateByString 与 template 的用法基本一样，区别在于前者从 String 变量中获取模板并且不支持
	 * #namespace、#sql 指令，后者从外部文件中获取模板。
	 * 
	 * 7、分页用法 Sql 管理实现分页功能，在使用 #sql 定义 sql 时，与普通查询完全一样，不需要使用额外的指令，以下是代码示例：
	 * 
	 * // Db.template 用法 Db.template("findGirl", 18, 50).paginate(1, 10);
	 * 
	 * // Model.template 用法，以下假定 girl 为 Model girl.template("findGirl", 18,
	 * 50).paginate(1, 10);
	 * 
	 * // getSqlPara 用法 SqlPara sqlPara = Db.getSqlPara("findGirl", 18, 50);
	 * Db.paginate(1, 10, sqlPara); template 与 getSqlPara 用法参数完全一样，都是
	 * "findGirl"、18、50 这三个，前者更省代码。
	 * 
	 * 8、高级用法 除了#sql、#para、#namespace之外，还可以使用JFinal Template
	 * Engine中所有存在的指令，生成复杂条件的sql语句，以下是相对灵活的示例：
	 * 
	 * #sql("find") select * from girl #for(x : cond) #(for.first ? "where": "and")
	 * #(x.key) #para(x.value) #end #end
	 * 以上代码#for指令对Map类型的cond参数进行迭代，动态生成自由的查询条件。上图中的三元表达式表示在第一次迭代时生成 where，后续则生成 and
	 * 。#(x.key) 输出参数 key 值，#para(x.value) 输出一个问号占位符，以及将参数 value 值输出到
	 * SqlPara.paraList 中去。
	 * 
	 * 以上sql 模板所对应的 java 代码如下：
	 * 
	 * Kv cond = Kv.by("age > ", 16).set("sex = ", "female"); Db.template("find",
	 * Kv.by("cond", cond)).find(); 以上第一行代码是 JFinal
	 * 独创的参数带有比较运算符的用法，可以同时生成sql查询条件名称、条件运算符号、参数列表，一石三鸟。甚至可以将此法用于 and or not再搭配一个
	 * LinkedHashMap 生成更加灵活的逻辑组合条件sql。
	 * 
	 * 更加好玩的用法，可以用jfinal 模板引擎的 #define 指令将常用的 sql 定义成通用的模板函数，以便消除重复性 sql 代码。总之，利用
	 * #sql、#para、#namespace 这三个指令再结合模板引擎已有指令自由组合，可非常简洁地实现极为强大的 sql管理功能。
	 * 
	 * 注意：以上例子中的Kv是 JFinal 提供的用户体验更好的 Map 实现，使用任意的 Map 实现都可以，不限定为Kv。
	 * 
	 * 9、多数源支持 多数源下，Model.getSql(...) 与 Model.getSqlPara(...) 会自动从该 Model 所对应的
	 * ActiveRecordPlugin 配置的 sql 模板中去取 sql。
	 * 
	 * Db.getSql(...) 与 Db.getSqlPara(...) 会从主数据源对应的 ActiveRecordPlugin 配置的 sql
	 * 模板中去取 sql。如果要使用别的数据源的 sql，使用一下 Db.use(...) 即可：
	 * 
	 * // template 用法 Db.use(otherDataSource).template(sqlKey, para).find();
	 * 
	 * // getSqlPara 用法 SqlPara sqlPara = Db.use(otherDataSource).getSqlPara(sqlKey,
	 * para); Db.use(otherDataSource).find(sqlPara); 在多数据源下， template 用法显然比
	 * getSqlPara 方法更省代码，在可以满足需求的前提下尽可能使用 template 方法。
	 * 
	 * 
	 * 
	 * 10、对 sql 内容进行压缩 jfinal 4.9 对 Engine 添加了空白压缩功能，该功能也可以用于 sql 模板：
	 * 
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(...); Engine engine =
	 * arp.getEngine();
	 * 
	 * engine.setCompressorOn(' '); 以上第三行代码配置开启了空白压缩功能，除了配置空格为压缩分隔字符以外，还可以配置换行字符
	 * '\n' 为压缩分隔字符：
	 * 
	 * engine.setCompressorOn('\n');
	 */

	/*
	 * 60.多数据源支持 ActiveRecordPlugin可同时支持多数据源、多方言、多缓存、多事务级别等特性，对每个 ActiveRecordPlugin
	 * 可进行彼此独立的配置。简言之 JFinal 可以同时使用多数据源，并且可以针对这多个数据源配置独立的方言、缓存、事务级别等。
	 * 
	 * 当使用多数据源时，只需要对每个 ActiveRecordPlugin指定一个 configName即可，如下是代码示例：
	 * 
	 * public void configPlugin(Plugins me) { // mysql 数据源 DruidPlugin dsMysql = new
	 * DruidPlugin(…); me.add(dsMysql);
	 * 
	 * // mysql ActiveRecrodPlugin 实例，并指定configName为 mysql ActiveRecordPlugin
	 * arpMysql = new ActiveRecordPlugin("mysql", dsMysql); me.add(arpMysql);
	 * arpMysql.addMapping("user", User.class);
	 * 
	 * // oracle 数据源 DruidPlugin dsOracle = new DruidPlugin(…); me.add(dsOracle);
	 * 
	 * // oracle ActiveRecrodPlugin 实例，并指定configName为 oracle ActiveRecordPlugin
	 * arpOracle = new ActiveRecordPlugin("oracle", dsOracle); me.add(arpOracle);
	 * arpOracle.setDialect(new OracleDialect()); arpOracle.addMapping("blog",
	 * Blog.class); } 以上代码创建了创了两个ActiveRecordPlugin实例arpMysql与arpOrace，
	 * 特别注意创建实例的同时指定其configName分别为mysql与oracle。arpMysql与arpOracle分别映射了不同的Model，
	 * 配置了不同的方言。
	 * 
	 * 对于Model的使用，不同的Model会自动找到其所属的ActiveRecrodPlugin实例以及相关配置进行数据库操作。
	 * 假如希望同一个Model能够切换到不同的数据源上使用，也极度方便，这种用法非常适合不同数据源中的table拥有相同表结构的情况，
	 * 开发者希望用同一个Model来操作这些相同表结构的table，以下是示例代码：
	 * 
	 * public void multiDsModel() { // 默认使用arp.addMapping(...)时关联起来的数据源 Blog blog =
	 * Blog.dao.findById(123);
	 * 
	 * // 只需调用一次use方法即可切换到另一数据源上去 blog.use("backupDatabase").save(); }
	 * 上例中的代码，blog.use(“backupDatabase”)方法切换数据源到backupDatabase并直接将数据保存起来。
	 * 
	 * 特别注意：只有在同一个Model希望对应到多个数据源的table时才需要使用use方法，如果同一个Model唯一对应一个数据源的一个table，
	 * 那么数据源的切换是自动的，无需使用use方法。
	 * 
	 * 对于Db +
	 * Record的使用，数据源的切换需要使用Db.use(configName)方法得到数据库操作对象，然后就可以进行数据库操作了，以下是代码示例：
	 * 
	 * // 查询 dsMysql数据源中的 user List<Record> users =
	 * Db.use("mysql").find("select * from user"); // 查询 dsOracle数据源中的 blog
	 * List<Record> blogs = Db.use("oracle").find("select * from blog");
	 * 以上两行代码，分别通过configName为mysql、oracle得到各自的数据库操作对象，然后就可以如同单数据完全一样的方式来使用数据库操作
	 * API了。简言之，对于 Db + Record来说，多数据源相比单数据源仅需多调用一下Db.use(configName)，随后的API使用方式完全一样。
	 * 
	 * 注意最先创建的 ActiveRecrodPlugin实例将会成为主数据源，可以省略configName。最先创建的
	 * ActiveRecrodPlugin实例中的配置将默认成为主配置，此外还可以通过设置configName为
	 * DbKit.MAIN_CONFIG_NAME常量来设置主配置。
	 */

	/*
	 * 61.独立使用ActiveRecord ActiveRecordPlugin可以独立于java web
	 * 环境运行在任何普通的java程序中，使用方式极度简单，相对于web项目只需要手动调用一下其start() 方法即可立即使用。以下是代码示例：
	 * 
	 * public class ActiveRecordTest { public static void main(String[] args) {
	 * DruidPlugin dp = new DruidPlugin("localhost", "userName", "password");
	 * ActiveRecordPlugin arp = new ActiveRecordPlugin(dp); arp.addMapping("blog",
	 * Blog.class);
	 * 
	 * // 与 jfinal web 环境唯一的不同是要手动调用一次相关插件的start()方法 dp.start(); arp.start();
	 * 
	 * // 通过上面简单的几行代码，即可立即开始使用 new Blog().set("title", "title").set("content",
	 * "cxt text").save(); Blog.dao.findById(123); } }
	 * 注意：ActiveRecordPlugin所依赖的其它插件也必须手动调用一下start()方法，如上例中的dp.start()。
	 * 
	 * 
	 * 
	 * jfinal 的 activerecord 模块已被独立发布到了 maven 库，如果只想使用 jfinal activerecord 模块而不想引入整个
	 * jfinal 的可以使用下面的坐标：
	 * 
	 * <dependency> <groupId>com.jfinal</groupId>
	 * <artifactId>activerecord</artifactId> <version>4.9.02</version> </dependency>
	 * 独立使用该模块的用法与在 jfinal 中使用时完全一样
	 * 
	 * 特别注意：activerecord 模块中包含了 enjoy template 模块，如果要使用 enjoy 模板引擎，直接使用就好，无需引入 enjoy
	 * template 模块的 maven 依赖，否则会造成冲突。
	 * 
	 * 
	 * 
	 * 如果是与 spring boot
	 * 整合使用，可以参考这篇文章：https://blog.csdn.net/suxiaoqiuking/article/details/78999857
	 */

	/*
	 * 62.调用存储过程 使用工具类 Db 可以很方便调用存储过程，以下是代码示例：
	 * 
	 * Db.execute((connection) -> { CallableStatement cs =
	 * connection.prepareCall(...); cs.setObject(1, ...); cs.setObject(2, ...);
	 * cs.execute(); cs.close(); return cs.getObject(1); }); 如上所示，使用 Db.execute(...)
	 * 可以很方便去调用存储过程，其中的 connection 是一个 Connection 类型的对象，该对象在使用完以后，不必 close()，因为
	 * jfinal 在上层会默认帮你 close() 掉。
	 * 
	 * 
	 * 
	 * 此外，MySQL 之下还可以使用更简单的方式调用存储过程：
	 * 
	 * // 调用存储过程，查询 salary 表 Record first =
	 * Db.findFirst("CALL FindSalary (1,\"201901\")");
	 * 
	 * // 调用存储过程，插入 salary 表 int update2 = Db.update("CALL InsertSalary (3, 123)");
	 * 
	 * // 调用存储过程，更新 salary 表 int update = Db.update("CALL UpdateSalary (3, 99999)");
	 * 
	 * // 调用存储过程，删除 salary 表 int delete = Db.delete("CALL DeleteSalary(3)");
	 */

	/*
	 * 63.Enjoy模板引擎，概述 Enjoy Template Engine 采用独创的 DKFF (Dynamic Key Feature
	 * Forward)词法分析算法以及独创的DLRD (Double Layer Recursive
	 * Descent)语法分析算法，极大减少了代码量，降低了学习成本，并提升了用户体验。
	 * 
	 * 与以往任何一款 java 模板引擎都有显著的不同，极简设计、独创算法、极爽开发体验，这里是发布时的盛况，传送门：JFinal 3.0
	 * 发布，重新定义模板引擎
	 * 
	 * 从 JFinal 3.0 到 JFinal 3.3，Enjoy 引擎进行了精细化的打磨，这里是与 Enjoy 引擎打磨有关的近几个版本发布时的盛况:
	 * 
	 * JFinal 3.0 发布，重新定义模板引擎
	 * 
	 * JFinal 3.1 发布，没有繁琐、没有复杂，只有妙不可言
	 * 
	 * JFinal 3.2 发布，星星之火已成燎原之势
	 * 
	 * JFinal 3.3 发布，天下武功，唯快不破
	 * 
	 * 
	 * 
	 * Enjoy 模板引擎专为 java 开发者打造，所以坚持两个核心设计理念：一是在模板中可以直接与 java 代码通畅地交互，二是尽可能沿用 java
	 * 语法规则，将学习成本降到极致。
	 * 
	 * 因此，立即掌握 90% 的用法，只需要记住一句话：Enjoy 模板引擎表达式与 Java 是直接打通的。
	 * 
	 * 
	 * 
	 * 记住了上面这句话，就可以像下面这样愉快地使用模板引擎了：
	 * 
	 * // 算术运算 1 + 2 / 3 * 4 // 比较运算 1 > 2 // 逻辑运算 !a && b != c || d == e // 三元表达式 a
	 * > 0 ? a : b // 方法调用 "abcdef".substring(0, 3) target.method(p1, p2, pn) Enjoy
	 * 模板引擎核心概念只有指令与表达式这两个。而表达式是与 Java 直接打通的，所以没有学习成本，剩下来只有
	 * #if、#for、#define、#set、#include、#switch、#(...) 七个指令需要了解，而这七个指令的学习成本又极低。
	 * 
	 * Enjoy 模板引擎发布以来，得到了非常多用户的喜爱，反馈证明体验极好，强烈建议还没能尝试过的同学们试用。
	 */

	/*
	 * 65.表达式 JFinal Template
	 * Engine表达式规则设计在总体上符合java表达式规则，仅仅针对模板引擎的特征进行极其少量的符合直觉的有利于开发体验的扩展。
	 * 
	 * 对于表达式的使用，再次强调一个关键点：表达式与Java是直接打通的。掌握了这个关键点立即就掌握了模板引擎90%的用法。如下是代码示例：
	 * 
	 * 123 + "abc" "abcd".substring(0, 1) userList.get(0).getName()
	 * 以上代码第一、第二行，与Java表达式的用法完全一样。第三行代码中，假定userList中有User对象，并且User具有getName()方法，
	 * 只要知道变量的类型，就可以像使用Java表达式一样调用对象的方法。
	 * 
	 * 1、与java规则基本相同的表达式 算术运算： + - * / % ++ --
	 * 
	 * 比较运算： > >= < <= == != (基本用法相同，后面会介绍增强部分)
	 * 
	 * 逻辑运算： ! && ||
	 * 
	 * 三元表达式： ? :
	 * 
	 * Null 值常量: null
	 * 
	 * 字符串常量： "jfinal club"
	 * 
	 * 布尔常量：true false
	 * 
	 * 数字常量： 123 456F 789L 0.1D 0.2E10
	 * 
	 * 数组存取：array[i](Map被增强为额外支持 map[key]的方式取值)
	 * 
	 * 属性取值：object.field(Map被增强为额外支持map.key 的方式取值)
	 * 
	 * 方法调用：object.method(p1, p2…, pn) (支持可变参数)
	 * 
	 * 逗号表达式：123, 1>2, null, "abc", 3+6 (逗号表达式的值为最后一个表达式的值)
	 * 
	 * 小技巧：如果从java端往map中传入一个key为中文的值，可以通过map["中文"] 的方式去访问到，而不能用 "map.中文"
	 * 访问。因为引擎会将之优先当成是object.field的访问形式，而目前引擎暂时还不支持中文作为变量名标识符。
	 * 
	 * 2、属性访问 由于模板引擎的属性取值表达式极为常用，所以对其在用户体验上进行了符合直觉的扩展，field 表达式取值优先次序，以 user.name
	 * 为例：
	 * 
	 * 如果 user.getName() 存在，则优先调用
	 * 
	 * 如果 user 具有 public 修饰过的name 属性，则取 user.name 属性值（注意：jfinal 4.0 之前这条规则的优先级最低）
	 * 
	 * 如果 user 为 Model 子类，则调用 user.get("name")
	 * 
	 * 如果 user 为 Record，则调用 user.get("name")
	 * 
	 * 如果 user 为 Map，则调用 user.get("name")
	 * 
	 * 
	 * 
	 * 此外，还支持数组的length长度访问：array.length，与java语言一样
	 * 
	 * 最后，属性访问表达式还可以通过 FieldGetter 抽象类扩展，具体方法参考
	 * com.jfinal.template.expr.ast.FieldGetters，这个类中已经给出了多个默认实现类，以下配置将支持 user.girl
	 * 表达式去调用 user 对象的 boolean isGirl() 方法：
	 * 
	 * Engine.addFieldGetterToFirst(new
	 * com.jfinal.template.expr.ast.FieldGetters.IsMethodFieldGetter()); 3、方法调用
	 * 模板引擎被设计成与 java 直接打通，可以在模板中直接调用对象上的任何public方法，使用规则与java中调用方式保持一致，以下代码示例：
	 * 
	 * #("ABCDE".substring(0, 3)) #(girl.getAge()) #(list.size()) #(map.get(key))
	 * 以上第一行代码调用了String对象上的substring(0, 3)方法输出值为
	 * "ABC"。第二行代码在girl对象拥有getAge()方法时可调用。第三行代码假定map为一个Map类型时可调用其 get(...) 方法。
	 * 
	 * 简单来说：模板表达式中可以直接调用对象所拥有的public方法，方法调用支持可变参数，例如支持这种方法被调用：obj.find(String sql,
	 * Object … args)。
	 * 
	 * 对象方法调用与java直接打通式设计，学习成本为0、与java交互极其方便、并且立即拥有了非常强大的扩展机制。
	 * 
	 * 4、静态属性访问 在模板中通常要访问java代码中定义的静态变量、静态常量，以下是代码示例：
	 * 
	 * #if(x.status == com.demo.common.model.Account::STATUS_LOCK_ID)
	 * <span>(账号已锁定)</span> #end
	 * 如上所示，通过类名加双冒号再加静态属性名即为静态属性访问表达式，上例中静态属性在java代码中是一个int数值，
	 * 通过这种方式可以避免在模板中使用具体的常量值，从而有利于代码重构。
	 * 
	 * 由于静态属性访问需要包名前缀，代码显得比较长，在实际使用时如果多次用到同一个值，可以用 #set(STATUS_LOCK_ID = ...)
	 * 指令将常量值先赋给一个变量，可以节省一定的代码。
	 * 
	 * 注意，这里的属性必须是public static修饰过的才可以被访问。此外，这里的静态属性并非要求为final修饰。
	 * 
	 * 如果某个静态属性要被经常使用，建议通过 addSharedObject(...) 将其配置成共享对象，然后通过 field
	 * 表达式来引用，从而节省代码，例如先配置 shared object：
	 * 
	 * public void configEngine(Engine me) { me.addSharedObject("Account", new
	 * Account()); } 然后在模板中就可以使用 field 表达式来代替原有的静态属性访问表达式了：
	 * 
	 * #if(x.status == Account.STATUS_LOCK_ID) <span>(账号已锁定)</span> #end 5、静态方法调用
	 * JFinal Template Engine 可以以非常简单的方式调用静态方法，以下是代码示例：
	 * 
	 * #if(com.jfinal.kit.StrKit::isBlank(title)) .... #end
	 * 使用方式与前面的静态属性访问保持一致，仅仅是将静态属性名换成静态方法名，并且后面多一对小括号与参数：类名 + :: +
	 * 方法名(参数)。静态方法调用支持可变参数。与静态属性相同，被调用的方法需要使用public static 修饰才可访问。
	 * 
	 * 如果觉得类名前方的包名书写很麻烦，可以使用后续即将介绍的me.addSharedMethod(…)方法将类中的方法添加为共享方法，
	 * 调用的时候直接使用方法名即可，连类名都不再需要。
	 * 
	 * 此外，还可以调用静态属性上的方法，以下是代码示例：
	 * 
	 * (com.jfinal.MyKit::me).method(paras)
	 * 上面代码中需要先用一对小扩号将静态属性取值表达式扩起来，然后再去调用它的方法，小括号在此仅是为了改变表达式的优先级。
	 * 
	 * 6、空合并安全取值调用操作符 JFinal Template Engine
	 * 引入了swift与C#语言中的空合操作符，并在其基础之上进行了极为自然的扩展，该表达式符号为两个紧靠的问号：??。代码示例：
	 * 
	 * seoTitle ?? "JFinal 社区" object.field ?? object.method() ??
	 * 以上第一行代码的功能与swift语言功能完全一样，也即在seoTitle
	 * 值为null时整个表达式取后面表达式的值。而第二行代码表示对object.field进行空安全(Null
	 * Safe)属性取值，即在object为null时表达式不报异常，并且值为null。
	 * 
	 * 第三行代码与第二行代码类似，仅仅是属性取值变成了方法调用，并称之为空安全(Null
	 * Safe)方法调用，表达式在object为null时不报异常，其值也为null。
	 * 
	 * 当然，空合并与空安全可以极为自然地混合使用，如下是示例：
	 * 
	 * object.field ?? "默认值" object.method() ?? value 以上代码中，第一行代码表示左侧null safe
	 * 属性取值为null时，整个表达式的值为后方的字符串中的值，而第二行代码表示值为null时整个表达式取value这个变量中的值。
	 * 
	 * 特别注意：?? 操作符的优先级高于数学计算运算符：+、-、*、/、%，低于单目运算符：!、++、--。强制改变优先级使用小括号即可。
	 * 
	 * 例子：a.b ?? && expr 表达式中，其 a.b ?? 为一个整体被求值，因为 ?? 优先级高于数学计算运算符，而数学计算运算符又高于 &&
	 * 运算符，进而推导出 ?? 优先级高于&&
	 * 
	 * 7、单引号字符串 针对Template Engine 经常用于html的应用场景，添加了单引号字符串支持，以下是代码示例：
	 * 
	 * <a href="/" class="#(menu == 'index' ? 'current' : 'normal')" 首页 </a>
	 * 以上代码中的三元表达式中有三处使用了单引号字符串，好处是可以与最外层的双引号协同工作，也可以反过来，最外层用单引号字符串，而内层表达式用双引号字符串。
	 * 
	 * 这个设计非常有利于在模板文件中已有的双引号或单引号内容之中书写字符串表达式。
	 * 
	 * 8、相等与不等比较表达式增强 相等不等表达式 == 与 !=
	 * 会对左右表达式进行left.equals(right)比较操作，所以可以对字符串进行直接比较，如下所示：
	 * 
	 * #if(nickName == "james") ... #end 注意：Controller.keepPara(…)
	 * 方法会将任何数据转换成String后传递到view层，所以原本可以用相等表达式比较的两个Integer型数据，在keepPara(…)后变得不可比较，
	 * 因为变为了String与Integer型的比较。解决方法见本章的Extionsion Method小节。
	 * 
	 * 9、布尔表达式增强 布尔表达式在原有java基础之下进行了增强，可以减少代码输入量，具体规则自上而下优先应用如下列表：
	 * 
	 * null 返回 false
	 * 
	 * boolean 类型，原值返回
	 * 
	 * String、StringBuilder等一切继承自 CharSequence 类的对象，返回 length > 0
	 * 
	 * 其它返回 true
	 * 
	 * 以上规则可以减少模板中的代码量，以下是示例：
	 * 
	 * #if(user && user.id == x.userId) ... #end 以上代码中的 user 表达式实质上代替了java表达式的 user
	 * != null 这种写法，减少了代码量。当然，上述表达式如果使用 ?? 运算符，还可以更加简单顺滑：if (user.id ?? == x.userId)
	 * 
	 * 10、Map 定义表达式
	 * Map定义表达式的最实用场景是在调用方法或函数时提供极为灵活的参数传递方式，当方法或函数需要传递的参数名与数量不确定时极为有用，以下是基本用法：
	 * 
	 * #set(map = {k1:123, "k2":"abc", "k3":object}) #(map.k1) #(map.k2)
	 * #(map["k1"]) #(map["k2"]) #(map.get("k1")) 如上图所示，map的定义使用一对大括号，每个元素以key :
	 * value的形式定义，多个元素之间用逗号分隔。
	 * 
	 * key 只允许是合法的 java 变量名标识符或者 String 常量值（jfinal 3.4 起将支持
	 * int、long、float、double、boolean、null 等等常量值），注意：上例中使用了标识符 k1 而非 String 常量值 "k1"
	 * 只是为了书写时的便利，与字符串是等价的，并不会对标识符 k1 进行表达式求值。
	 * 
	 * 上图中通过#set指令将定义的变量赋值给了map变量，第二与第三行中以object.field的方式进行取值，第四第五行以 map[key]
	 * 的方式进行取值，第六行则是与 java 表达式打通式的用法。
	 * 
	 * 特别注意：上例代码如果使用 map[k1] 来取值，则会对 k1 标识符先求值，得到的是 null，也即map[k1] 相当于
	 * map[null]，因此上述代码中使用了 map["k1"] 这样的形式来取值。
	 * 
	 * 
	 * 
	 * 此外，map 取值还支持在定义的同时来取值，如下所示：
	 * 
	 * #({1:'自买', 2:'跟买'}.get(1)) #({1:'自买', 2:'跟买'}[2])
	 * 
	 * ### 与双问号符联合使用支持默认值 #({1:'自买', 2:'跟买'}.get(999) ?? '其它') 上述 key 为 int 常量，自
	 * jfinal 3.4 版本才开始支持。
	 * 
	 * 11､数组定义表达式 直接举例：
	 * 
	 * // 定义数组 array，并为元素赋默认值 #set(array = [123, "abc", true])
	 * 
	 * // 获取下标为 1 的值，输出为: "abc" #(array[1])
	 * 
	 * // 将下标为 1 的元素赋值为 false，并输出 #(array[1] = false, array[1])
	 * 以上代码演示了数组的定义与初始化，以及数据获取与赋值。其中最后一行代码并没有使用 #set 指令，也就是说数组定义表达式可以脱离 #set
	 * 指令，以任意表达式为形式使用在任何指令内部（Map 定义表达式也一样可以）
	 * 
	 * 数组定义表达式的初始化元素除了可以使用常量值以外，还可以使用任意的表达式，包括变量、方法调用返回值等等：
	 * 
	 * #set(array = [ 123, "abc", true, a && b || c, 1 + 2, obj.doIt(x) ])
	 * 
	 * 
	 * 12、范围数组定义表达式 直接举例：
	 * 
	 * #for(x : [1..10]) #(x) #end 上图中的表达式 [1..10]
	 * 定义了一个范围数组，其值为从1到10的整数数组，该表达式通常用于在开发前端页面时，模拟迭代输出多条静态数据，而又不必从后端读取数据。
	 * 
	 * 此外，还支持递减的范围数组，例如：[10..1] 将定义一个从10到1的整数数组。上例中的#for指令与#()输出指令后续会详细介绍。
	 * 
	 * 
	 * 
	 * 13、逗号表达式 将多个表达式使用逗号分隔开来组合而成的表达式称为逗号表达式，逗号表达式整体求值的结果为最后一个表达式的值。例如：1+2, 3*4
	 * 这个逗号表达式的值为12。
	 * 
	 * 14、从java中去除的运算符 针对模板引擎的应用场景，去除了位运算符，避免开发者在模板引擎中表述过于复杂，保持模板引擎的应用初衷，同时也可以提升性能。
	 * 
	 * 15、表达式总结 以上各小节介绍的表达式用法，主要是在 java 表达式规则之上做的有利于开发体验的精心扩展，你也可以先无视这些用法，而是直接当成是
	 * java 表达式去使用，则可以免除掉上面的学习成本。
	 * 
	 * 上述这些在 java 表达式规则基础上做的精心扩展，一是基于模板引擎的实际使用场景而添加，例如单引号字符串。二是对过于啰嗦的 java
	 * 语法的改进，例如字符串的比较 str == "james" 取代 str.equals("james")，所以是十分值得和必要的。
	 */

	/*
	 * 66.指令 Enjoy Template Engine一如既往地坚持极简设计，核心只有
	 * #if、#for、#switch、#set、#include、#define、#(…)
	 * 这七个指令，便实现了传统模板引擎几乎所有的功能，用户如果有任意一门程序语言基础，学习成本几乎为零。
	 * 
	 * 如果官方提供的指令无法满足需求，还可以极其简单地在模板语言的层面对指令进行扩展，在com.jfinal.template.ext.directive
	 * 包下面就有五个扩展指令，Active Record 的 sql 模块也针对sql管理功能扩展了三个指令，参考这些扩展指令的代码，便可无师自通，极为简单。
	 * 
	 * 注意，Enjoy
	 * 模板引擎指令的扩展是在词法分析、语法分析的层面进行扩展，与传统模板引擎的自定义标签类的扩展完全不是一个级别，前者可以极为全面和自由的利用模板引擎的基础设施
	 * ，在更加基础的层面以极为简单直接的代码实现千变万化的功能。参考 Active Record的 sql 管理模块，则可知其强大与便利。
	 * 
	 * 1、输出指令#( ) 与几乎所有 java 模板引擎不同，Enjoy Template
	 * Engine消灭了插值指令这个原本独立的概念，而是将其当成是所有指令中的一员，仅仅是指令名称省略了而已。因此，该指令的定界符与普通指令一样为小括号，
	 * 从而不必像其它模板引擎一样引入额外的如大括号般的定界符。
	 * 
	 * #(…) 输出指令的使用极为简单，只需要为该指令传入前面6.4节中介绍的任何表达式即可，指令会将这些表达式的求值结果进行输出，特别注意，当表达式的值为
	 * null 时没有任何输出，更不会报异常。所以，对于 #(value) 这类输出不需要对 value 进行 null 值判断，如下是代码示例：
	 * 
	 * #(value) #(object.field) #(object.field ??) #(a > b ? x : y) #(seoTitle ??
	 * "JFinal 俱乐部") #(object.method(), null) 如上图所示，只需要对输出指令传入表达式即可。注意上例中第一行代码 value
	 * 参数可以为 null，而第二行代码中的 object 为 null
	 * 时将会报异常，此时需要使用第三行代码中的空合安全取值调用运算符：object.field ??
	 * 
	 * 此外，注意上图最后一行代码中的输出指令参数为一个逗号表达式，逗号表达式的整体求值结果为最后一个表达式的值，而输出指令对于null值不做输出，
	 * 所以这行代码相当于是仅仅调用了 object.method() 方法去实现某些操作。
	 * 
	 * 输出指令可以自由定制，只需要继承 OutputDirectiveFactory 类并覆盖其中的 getOutputDirective 方法，然后在
	 * configEngine(Engine me)方法中，通过 me. setOutputDirectiveFactory(…) 切换即可。
	 * 
	 * 2、#if 指令 直接举例：
	 * 
	 * #if(cond) ... #end 如上图所示，if指令需要一个 cond 表达式作为参数，并且以 #end 为结尾符，cond 可以为 6.3
	 * 章节中介绍的所有表达式，包括逗号表达式，当 cond 求值为 true 时，执行 if 分支之中的代码。
	 * 
	 * if 指令必然支持 #else if 与 #else 分支块结构，以下是示例：
	 * 
	 * #if(c1) ... #else if(c2) ... #else if (c3) ... #else ... #end 由于#else
	 * if、#else用法与java语法完全一样，在此不在赘述。（注意：jfinal 3.3 之前的版本 #else if
	 * 之间不能有空格字符需要写成：#elseif，否则会报异常：Can not match the #end of directive #if ）
	 * 
	 * 3、#for 指令 Enjoy Template Engine 对 for 指令进行了极为人性化的扩展，可以对任意类型数据进行迭代输出，包括支持 null
	 * 值迭代。以下是代码示例：
	 * 
	 * // 对 List、数组、Set 这类结构进行迭代 #for(x : list) #(x.field) #end
	 * 
	 * // 对 Map 进行迭代 #for(x : map) #(x.key) #(x.value) #end 上例代码中的第一个 for 指令是对 list
	 * 进行迭代，用法与 java 语法完全一样。
	 * 
	 * 第二个 for 指令是对 map 进行迭代，取值方式为 item.key 与 item.value。该取值方式是 enjoy 对 map
	 * 迭代的增强功能，可以节省代码量。仍然也可以使用传统的 java map 迭代方式：#for( x : map.entrySet() ) #(x.key)
	 * #(x.value) #end
	 * 
	 * 注意：当被迭代的目标为 null 时，不需要做 null 值判断，for 指令会自动跳过，不进行迭代。从而可以避免 if 判断，节省代码提高效率。
	 * 
	 * for指令还支持对其状态进行获取，代码示例：
	 * 
	 * #for(x : listAaa) #(for.index) #(x.field)
	 * 
	 * #for(x : listBbb) #(for.outer.index) #(for.index) #(x.field) #end #end 以上代码中的
	 * #(for.index)、#(for.outer.index) 是对 for 指令当前状态值进行获取，前者是获取当前 for
	 * 指令迭代的下标值(从0开始的整数)，后者是内层for指令获取上一层for指令的状态。这里注意 for.outer 这个固定的用法，专门用于在内层 for
	 * 指令中引用上层for指令状态。
	 * 
	 * 注意：for指令嵌套时，各自拥有自己的变量名作用域，规则与java语言一致，例如上例中的两个#(x.field)处在不同的for指令作用域内，
	 * 会正确获取到所属作用域的变量值。
	 * 
	 * for指令支持的所有状态值如下示例：
	 * 
	 * #for(x : listAaa) #(for.size) 被迭代对象的 size 值 #(for.index) 从 0 开始的下标值
	 * #(for.count) 从 1 开始的记数值 #(for.first) 是否为第一次迭代 #(for.last) 是否为最后一次迭代
	 * #(for.odd) 是否为奇数次迭代 #(for.even) 是否为偶数次迭代 #(for.outer) 引用上层 #for 指令状态 #end
	 * 具体用法在上面代码中用中文进行了说明，在此不再赘述。
	 * 
	 * 除了 Map、List 以外，for指令还支持 Collection、Iterator、array
	 * 普通数组、Iterable、Enumeration、null 值的迭代，用法在形式上与前面的List迭代完全相同，都是 #for(id : target)
	 * 的形式，对于 null 值，for指令会直接跳过不迭代。
	 * 
	 * 此外，for指令还支持对任意类型进行迭代，此时仅仅是对该对象进行一次性迭代，如下所示：
	 * 
	 * #for(x : article) #(x.title) #end
	 * 上例中的article为一个普通的java对象，而非集合类型对象，for循环会对该对象进行一次性迭代操作，for表达式中的x即为article对象本身，
	 * 所以可以使用 #(x.title) 进行输出。
	 * 
	 * for 指令还支持 #else 分支语句，在for指令迭代次数为0时，将执行 #else 分支内部的语句，如下是示例：
	 * 
	 * #for(blog : blogList) #(blog.title) #else 您还没有写过博客，点击此处<a
	 * href="/blog/add">开博</a> #end 以上代码中，当blogList.size()
	 * 为0或者blogList为null值时，也即迭代次数为0时，会执行#else分支，这种场景在web项目中极为常见。
	 * 
	 * 最后，除了上面介绍的for指令迭代用法以外，还支持更常规的for语句形式，以下是代码示例：
	 * 
	 * #for(i = 0; i < 100; i++) #(i) #end
	 * 与java语法基本一样，唯一的不同是变量声明不需要类型，直接用赋值语句即可，Enjoy Template Engine中的变量是动态弱类型。
	 * 
	 * 注意：以上这种形式的for语句，比前面的for迭代少了for.size与for.last两个状态，只支持如下几个状态：for.index、for.
	 * count、for.first、for.odd、for.even、for.outer
	 * 
	 * #for 指令还支持 #continue、#break 指令，用法与java完全一致，在此不再赘述。
	 * 
	 * 4、#switch 指令（3.6 版本新增指令） #switch 指令对标 java 语言的 switch
	 * 语句。基本用法一致，但做了少许提升用户体验的改进，用法如下：
	 * 
	 * #switch (month) #case (1, 3, 5, 7, 8, 10, 12) #(month) 月有 31 天 #case (2)
	 * #(month) 月平年有28天，闰年有29天 #default 月份错误: #(month ?? "null") #end 如上代码所示，#case
	 * 分支指令支持以逗号分隔的多个参数，这个功能就消解掉了 #break 指令的必要性，所以 enjoy 模板引擎是不需要 #break 指令的。
	 * 
	 * #case 指令参数还可以是任意表达式，例如：
	 * 
	 * #case (a, b, x + y, "abc", "123") 上述代码中用逗号分隔的表达式先会被求值，然后再逐一与 #switch(value)
	 * 指令中的 value 进行比较，只要有一个值与其相等则该 case 分支会被执行。
	 * 
	 * #case 支持逗号分隔的多参数，从而无需引入 #break 指令，不仅减少了代码量，而且避免了忘写 #break 指令时带来的错误隐患。还有一个与
	 * java 语法有区别的地方是 #case、#default 指令都未使用冒号字符。
	 * 
	 * 5、#set 指令
	 * set指令用于声明变量同时对其赋值，也可以是为已存在的变量进行赋值操作。set指令只接受赋值表达式，以及用逗号分隔的赋值表达式列表，如下是代码示例：
	 * 
	 * #set(x = 123) #set(a = 1, b = 2, c = a + b) #set(array[0] = 123)
	 * #set(map["key"] = 456)
	 * 
	 * #(x) #(c) #(array[0]) #(map.key) #(map["key"])
	 * 以上代码中，第一行代码最为简单为x赋值为123，第二行代码是一个赋值表达式列表，会从左到右依次执行赋值操作，如果等号右边出现表达式，
	 * 将会对表达式求值以后再赋值。最后一行代码是输出上述赋值以后各变量的值，其她所有指令也可以像输出指令一样进行变量的访问。
	 * 
	 * 请注意，#for、#include、#define这三个指令会开启新的变量名作用域，#set指令会首先在本作用域中查找变量是否存在，
	 * 如果存在则对本作用域中的变量进行操作，否则继续向上层作用域查找，找到则操作，如果找不到，则将变量定义在顶层作用域中，
	 * 这样设计非常有利于在模板中传递变量的值。
	 * 
	 * 当需要明确指定在本层作用域赋值时，可以使用#setLocal指令，该指令所需参数与用法与#set指令完全一样，只不过作用域被指定为当前作用域。#
	 * setLocal 指令通常用于#define、#include指令之内，用于实现模块化，从而希望其中的变量名不会与上层作用域发生命名上的冲突。
	 * 
	 * 重要：由于赋值表达式本质也是表达式，而其它指令本质上支持任意表达式，所以 #set 指令对于赋值来说并不是必须的，例如可以在 #()
	 * 输出指令中使用赋值表达式：
	 * 
	 * #(x = 123, y = "abc", array = [1, "a", true], map = {k1:v1}, null)
	 * 以上代码在输出指令中使用了多个赋值表达式，可以实现 #set 的功能，在最后通过一个 null
	 * 值来避免输出表达式输出任何东西。类似的，别的指令内部也可以这么来使用赋值表达式。
	 * 
	 * 6、#include 指令 include指令用于将外部模板内容包含进来，被包含的内容会被解析成为当前模板中的一部分进行使用，如下是代码示例：
	 * 
	 * #include("sidebar.html") #include 指令第一个参数必须为 String 常量，当以 ”/” 打头时将以
	 * baseTemplatePath 为相对路径去找文件，否则将以使用 #include 指令的当前模板的路径为相对路径去找文件。
	 * 
	 * baseTemplatePath 可以在 configEngine(Engine me) 中通过 me.setBaseTemplatePath(…)
	 * 进行配置。
	 * 
	 * 此外，include指令支持传入无限数量的赋值表达式，十分有利于模块化，例如：如下名为 ”_hot_list.html”
	 * 的模板文件用于展示热门项目、热门新闻等等列表：
	 * 
	 * <div class="hot-list"> <h3>#(title)</h3> <ul> #for(x : list) <li> <a
	 * href="#(url)/#(x.id)">#(x.title)</a> </li> #end </ul> </div> 上图中的
	 * title、list、url 是该html片段需要的变量，使用include指令分别渲染“热门项目”与“热门新闻”的用法如下：
	 * 
	 * #include("_hot_list.html", title="热门项目", list=projectList, url="/project")
	 * #include("_hot_list.html", title="热门新闻", list=newsList, url="/news")
	 * 上面两行代码中，为“_hot_list.html”中用到的三个变量title、list、url分别传入了不同的值，实现了对“_hot_list.html”
	 * 的模块化重用。
	 * 
	 * 7、#render 指令 render指令在使用上与include指令几乎一样，同样也支持无限量传入赋值表达式参数，主要有两点不同：
	 * 
	 * render指令支持动态化模板参数，例如：#render(temp)，这里的temp可以是任意表达式，而#include指令只能使用字符串常量：#
	 * include(“abc.html”)
	 * 
	 * render指令中#define定义的模板函数只在其子模板中有效，在父模板中无效，这样设计非常有利于模块化
	 * 
	 * 引入 #render 指令的核心目的在于支持动态模板参数。
	 * 
	 * 8、#define 指令 #define指令是模板引擎主要的扩展方式之一，define指令可以定义模板函数(Template
	 * Function)。通过define指令，可以将需要被重用的模板片段定义成一个一个的 template
	 * function，在调用的时候可以通过传入参数实现千变万化的功能。
	 * 
	 * 在此给出使用define指令实现的layout功能，首先创建一个layout.html文件，其中的代码如下：
	 * 
	 * #define layout() <html> <head> <title>JFinal俱乐部</title> </head> <body>
	 * #@content() </body> </html> #end 以上代码中通过#define
	 * layout()定义了一个名称为layout的模板函数，定义以#end结尾，其中的 #@content() 表示调用另一个名为 content
	 * 的模板函数。
	 * 
	 * 特别注意：模板函数的调用比指令调用多一个@字符，是为了与指令调用区分开来。
	 * 
	 * 接下来再创建一个模板文件，如下所示：
	 * 
	 * #include("layout.html") #@layout()
	 * 
	 * #define content() <div> 这里是模板内容部分，相当于传统模板引擎的 nested 的部分 </div> #end
	 * 上图中的第一行代码表示将前面创建的模板文件layout.html包含进来，第二行代码表示调用layout.html中定义的layout模板函数，
	 * 而这个模板函数中又调用了content这个模板函数，该content函数已被定义在当前文件中，简单将这个过程理解为函数定义与函数调用就可以了。注意，
	 * 上例实现layout功能的模板函数、模板文件名称可以任意取，不必像velocity、freemarker需要记住
	 * nested、layoutContent这样无聊的概念。
	 * 
	 * 通常作为layout的模板文件会在很多模板中被使用，那么每次使用时都需要#include指令进行包含，本质上是一种代码冗余，可以在configEngine
	 * (Engine
	 * me)方法中，通过me.addSharedFunction("layout.html")方法，将该模板中定义的所有模板函数设置为共享的，那么就可以省掉#
	 * include(…)，通过此方法可以将所有常用的模板函数全部定义成类似于共享库这样的集合，极大提高重用度、减少代码量、提升开发效率。
	 * 
	 * Enjoy Template
	 * Engine彻底消灭掉了layout、nested、macro这些无聊的概念，极大降低了学习成本，并且极大提升了扩展能力。模板引擎本质是一门程序语言，
	 * 任何可用于生产环境的语言可以像呼吸空气一样自由地去实现 layout 这类功能。
	 * 
	 * 此外，模板函数必然支持形参，用法与java规则基本相同，唯一不同的是不需要指定参数类型，只需要参数名称即可，如下是代码示例：
	 * 
	 * #define test(a, b, c) #(a) #(b) #(c) #end
	 * 以上代码中的模板函数test，有a、b、c三个形参，在函数体内仅简单对这三个变量进行了输出，注意形参必须是合法的java标识符，
	 * 形参的作用域为该模板函数之内符合绝大多数程序语言习惯，以下是调用该模板函数的例子代码：
	 * 
	 * #@test(123, "abc", user.name) 以上代码中，第一个参数传入的整型123，第二个是字符串，第三个是一个 field
	 * 取值表达式，从例子可以看出，实参可以是任意表达式，在调用时模板引擎会对表达式求值，并逐一赋值给模板函数的形参。
	 * 
	 * 注意：形参与实参数量要相同，如果实参偶尔有更多不确定的参数要传递进去，可以在调用模板函数代码之前使用#set指令将值传递进去，
	 * 在模板函数内部可用空合安全取值调用表达式进行适当控制，具体用法参考 jfinal-club 项目中的 _paginate.html 中的 append
	 * 变量的用法。
	 * 
	 * define 还支持 return 指令，可以在模板函数中返回，但不支持返回值。
	 * 
	 * 9、模板函数调用与 #call 指令 调用define定义的模板函数的格式为：#@name(p1, p2…,
	 * pn)，模板函数调用比指令调用多一个@字符，多出的@字符用来与指令调用区别开来。
	 * 
	 * 此外，模板函数还支持安全调用，格式为：#@name?(p1, p2…,
	 * pn)，安全调用只需在模板函数名后面添加一个问号即可。安全调用是指当模板函数未定义时不做任何操作。
	 * 
	 * 安全调用适合用于一些模板中可有可无的内容部分，以下是一个典型应用示例：
	 * 
	 * #define layout() <html> <head> <link rel="stylesheet" type="text/css"
	 * href="/assets/css/jfinal.css"> #@css?() </head>
	 * 
	 * <body> <div class="content"> #@main() </div>
	 * 
	 * <script type="text/javascript" src="/assets/js/jfinal.js"></script> #@js?()
	 * </body> </html> #end 以上代码示例定义了一个web应用的layout模板，注意看其中的两处：#@css?() 与 #@js?()
	 * 就是模板函数安全调用。
	 * 
	 * 上述模板中引入的 jfinal.css 与 jfinal.js
	 * 是两个必须的资源文件，对大部分模块已经满足需要，但对于有些模块，除了需要这两个必须的资源文件以外，还需要额外的资源文件，那么就可以通过#define
	 * css() 与 #define js() 来提供，如下是代码示例：
	 * 
	 * #@layout() ### 调用 layout.html 中定义的模板函数 layout()
	 * 
	 * #define main() 这里是 body 中的内容块 #end
	 * 
	 * #define css() 这里可以引入额外的 css 内容 #end
	 * 
	 * #define js() 这里可以引入额外的 js 内容 #end
	 * 以上代码中先是通过#@layout()调用了前面定义过的layout()这个模板函数，而这个模板函数中又分别调用了#@main()、#@css?()、#@
	 * js?()这三个模板函数，其中后两个是安全调用，所以对于不需要额外的css、js文件的模板，则不需要定义这两个方法，
	 * 安全调用在调用不存在的模板函数时会直接跳过。
	 * 
	 * 
	 * 
	 * #call 指令是 jfinal 3.6 版本新增指令，使用 #call 指令，模板函数的名称与参数都可以动态指定，提升模板函数调用的灵活性，用法如下：
	 * 
	 * #call(funcName, p1, p2, ..., pn) 上述代码中的 funcName 为函数名，p1、p2、pn
	 * 为被调用函数所使用的参数。如果希望模板函数不存在时忽略其调用，添加常量值 true 在第一个参数位置即可：
	 * 
	 * #call(true, funcName, p1, p2, ..., pn)
	 * 
	 * 
	 * 10、#date 指令 date指令用于格式化输出日期型数据，包括Date、Timestamp等一切继承自Date类的对象的输出，使用方式极其简单：
	 * 
	 * #date(account.createAt) #date(account.createAt, "yyyy-MM-dd HH:mm:ss")
	 * 上面的第一行代码只有一个参数，那么会按照默认日期格式进行输出，默认日期格式为：“yyyy-MM-dd
	 * HH:mm”。上面第二行代码则会按第二个参数指定的格式进行输出。
	 * 
	 * 如果希望改变默认输出格式，只需要通过engine.setDatePattern()进行配置即可。
	 * 
	 * keepPara 问题：如果日期型表单域提交到后端，而后端调用了 Controller 的 keepPara() 方法，会将这个日期型数据转成
	 * String 类型，那么 #date(...) 指令在输出这个 keepPara 过来的 String 时就会抛出异常，对于这种情况可以指令 keep
	 * 住其类型：
	 * 
	 * // keepPara() 用来 keep 住所有表单提交数据，全部转换成 String 类型 keepPara();
	 * 
	 * // 再用一次带参的 keepPara，指定 createAt 域 keep 成 Date 类型 keepPara(Date.class,
	 * "createAt"); 如上所示，第二行代码用 Date.class 参数额外指定了 createAt 域 keep 成 Date 类型，那么在页面
	 * #date(createAt) 指令就不会抛出异常了。keepModel(...)、keepBean(...) 会保持原有类型，无需做上述处理。
	 * 
	 * 
	 * 
	 * 11、#number 指令 number 指令用于格式化输出数字型数据，包括 Double、Float、Integer、Long、BigDecimal
	 * 等一切继承自Number类的对象的输出，使用方式依然极其简单：
	 * 
	 * #number(3.1415926, "#.##") #number(0.9518, "#.##%") #number(300000,
	 * "光速为每秒，### 公里。") 上面的 #number指令第一个参数为数字类型，第二个参数为String类型的pattern。
	 * Pattern参数的用法与JDK中DecimalFormat中pattern的用法完全一样。
	 * 当不知道如何使用pattern时可以在搜索引擎中搜索关键字DecimalFormat，可以找到非常多的资料。
	 * 
	 * #number指令的两个参数可以是变量或者复杂表达式，上例参数中使用常量仅为了方便演示。
	 * 
	 * 12､#escape 指令 escape 指令用于 html 安全转义输出，可以消除 XSS 攻击。escape 将类似于 html
	 * 形式的数据中的大于号、小于号这样的字符进行转义，例如将小于号转义成：&lt; 将空格转义成 &nbsp;
	 * 
	 * 使用方式与输出指令类似：
	 * 
	 * #escape(blog.content) 13、指令扩展 由于采用独创的 DKFF 和 DLRD 算法，Enjoy Template Engine
	 * 可以极其便利地在语言层面对指令进行扩展，而代码量少到不可想象的地步，学习成本无限逼近于 0。以下是一个代码示例：
	 * 
	 * public class NowDirective extends Directive { public void exec(Env env, Scope
	 * scope, Writer writer) { write(writer, new Date().toString()); } }
	 * 以上代码中，通过继承Directive并实现exec方法，三行代码即实现一个#now指令，可以向模板中输出当前日期，在使用前只需通过me.
	 * addDirective(“now”, NowDirective.class) 添加到模板引擎中即可。以下是在模板中使用该指令的例子：
	 * 
	 * 今天的日期是： #now() 除了支持上述无#end块，也即无指令body的指令外，Enjoy Template
	 * Engine还直接支持包含#end与body的指令，以下是示例：
	 * 
	 * public class Demo extends Directive {
	 * 
	 * // ExprList 代表指令参数表达式列表 public void setExprList(ExprList exprList) { //
	 * 在这里可以对 exprList 进行个性化控制 super.setExprList(exprList); }
	 * 
	 * public void exec(Env env, Scope scope, Writer writer) { write(writer,
	 * "body 执行前"); stat.exec(env, scope, writer); // 执行 body write(writer,
	 * "body 执行后"); }
	 * 
	 * public boolean hasEnd() { return true; // 返回 true 则该指令拥有 #end 结束标记 } }
	 * 如上所示，Demo继承Directive覆盖掉父类中的hasEnd方法，并返回true，表示该扩展指令具有#end结尾符。上例中public void
	 * exec 方法中的三行代码，其中stat.exec(…)表示执行指令body中的代码，而该方法前后的write(…)方法分别输出一个字符串，
	 * 最终的输出结果详见后面的使用示例。此外通过覆盖父类的setExprList(…)方法可以对指令的参数进行控制，该方法并不是必须的。
	 * 
	 * 通过me.addDirective(“demo”, Demo.class)添加到引擎以后，就可以像如下代码示例中使用：
	 * 
	 * #demo() 这里是 demo body 的内容 #end 最后的输出结果如下：
	 * 
	 * body 执行前 这里是 demo body 的内容 body 执行后
	 * 上例中的#demo指令body中包含一串字符，将被Demo.exec(…)方法中的stat.exec(…)所执行，而stat.exec(…)
	 * 前后的write(…)两个方法调用产生的结果与body产生的结果生成了最终的结果。
	 * 
	 * 重要：指令中声明的属性是全局共享的，所以要保障指令中的属性是线程安全的。如下代码以
	 * com.jfinal.template.ext.directive.DateDirective 的代码片段为例：
	 * 
	 * public class DateDirective extends Directive {
	 * 
	 * private Expr valueExpr; private Expr datePatternExpr; private int paraNum;
	 * 
	 * ... } 以上代码中有三个属性，类型是 Expr、Expr、int，其中 Expr 是线程安全的，而 int paraNum
	 * 虽然表面上看不是线程安全的，但在整个 DateDirective 类中只有构造方法对该值初始化的时候有写入操作，其它所有地方都是读操作，所以该 int
	 * 属性在这里是线程安全的。
	 * 
	 * 
	 * 
	 * 14､常见错误 Enjoy 模板引擎的使用过程中最常见的错误就是分不清 “表达式” 与
	 * “非表达式”，所谓表达式是指模板函数调用、指令调用时小括号里面的所有东西，例如：
	 * 
	 * #directiveName(这里所有东西是表达式)
	 * 
	 * #@functionName(这里所有东西是表达式) 上例中的两行代码分别是调用指令与调用模板函数，小括号内的东西是表达式，而表达式的用法与 Java
	 * 几乎一样，该这么来用：
	 * 
	 * #directiveName( user.name ) 最常见错误的用法如下：
	 * 
	 * #directiveName ( #(user.name) ) 简单来说这种错误就是在该使用表达式的地方使用指令，在表达式中永远不要出现字符
	 * '#'，而是直接使用 java 表达式。
	 */

	/*
	 * 67. 注释 JFinal Template Engine支持单行与多行注释，以下是代码示例：
	 * 
	 * ### 这里是单行注释
	 * 
	 * #-- 这里是多行注释的第一行 这里是多行注释的第二行 --# 如上所示，单行注释使用三个#字符，多行注释以#--打头，以--#结尾。
	 * 
	 * 与传统模板引擎不同，这里的单行注释采用三个字符，主要是为了减少与文本内容相冲突的可能性，模板是极其自由化的内容，使用三个字符，冲突的概率降低一个数量级。
	 * 
	 * 
	 * 
	 * 
	 * 
	 * jfinal 4.4 之前的版本注意：注释在与指令放在同一行时，输出结果会删掉注释后方的换行字符，例如：
	 * 
	 * #("AAA") ### 这里是注释 BBB 以上模板的输出结果是："AAABBB"，如果希望输出结果严格遵守模板中的换行，只需将注释单独放在一行，例如：
	 * 
	 * ### 这里是注释，被单独放在了一行 #("AAA") BBB 以上模板的输出结果将会带有严格的换行，结果如下：
	 * 
	 * AAA BBB 多行注释与单行注释也类似，只需将其单独放即可。
	 * 
	 * 除了以上情况以外，其它任何情况都是严格按模板换行输出的，不必关注。jfinal 4.4 版本解决了此问题，建议升级到 4.4 或更高版本
	 */

	/*
	 * 68.原样输出 原样输出是指不被解析，而仅仅当成纯文本的内容区块，如下所示：
	 * 
	 * #[[ #(value) #for(x : list) #(x.name) #end ]]# 如上所示，原样输出以 #[[ 三个字符打头，以 ]]#
	 * 三个字符结尾，中间被包裹的内容虽然是指令，但仍然被当成是纯文本，这非常有利于解决与前端javascript模板引擎的指令冲突问题。
	 * 
	 * 无论是单行注释、多行注释，还是原样输出，都是以三个字符开头，目的都是为了降低与纯文本内容冲突的概率。
	 * 
	 * 注意：用于注释、原样输出的三个控制字符之间不能有空格
	 */

	/*
	 * 68.Shared Method 扩展 1､基本用法 Enjoy 模板引擎可以极其简单的直接使用任意的 java 类中的 public 方法，并且被使用的
	 * java 类无需实现任何接口也无需继承任何抽象类，完全无耦合。以下代码以 JFinal 之中的 com.jfinal.kit.StrKit 类为例：
	 * 
	 * public void configEngine(Engine me) { me.addSharedMethod(new
	 * com.jfinal.kit.StrKit()); } 以上代码已将StrKit类中所有的public方法添加为shared
	 * method，添加完成以后便可以直接在模板中使用，以下是代码示例：
	 * 
	 * #if(isBlank(nickName)) ... #end
	 * 
	 * #if(notBlank(title)) ... #end 上例中的isBlank 和 notBlank
	 * 方法都来自于StrKit类，这种扩展方式简单、便捷、无耦合。
	 * 
	 * 
	 * 
	 * 2、默认 Shared Method 配置扩展 Enjoy 模板引擎默认配置添加了
	 * com.jfinal.template.ext.sharedmethod.SharedMethodLib 为 Shared
	 * Method，所以其中的方法可以直接使用不需要配置。里头有 isEmpty(...) 与 notEmpty(...) 两个方法可以使用。
	 * 
	 * isEmpty(...) 用来判断 Collection、Map、数组、Iterator、Iterable 类型对象中的元素个数是否为 0，其规如下：
	 * 
	 * null 返回 true
	 * 
	 * List、Set 等一切继承自 Collection 的，返回 isEmpty()
	 * 
	 * Map 返回 isEmpty()
	 * 
	 * 数组返回 length == 0
	 * 
	 * Iterator 返回 ! hasNext()
	 * 
	 * Iterable 返回 ! iterator().hasNext()
	 * 
	 * 
	 * 
	 * 以下是代码示例：
	 * 
	 * #if ( isEmpty(list) ) list 中的元素个数等于 0 #end
	 * 
	 * #if ( notEmpty(map) ) map 中的元素个数大于 0 #end 如上所示，isEmpty(list) 判断 list 中的元素是否大于
	 * 0。notEmpty(...) 的功能与 isEmpty(...) 恰好相反，等价于 ! isEmpty(...)
	 */

	/*
	 * 69.Shared Object扩展 通过使用addSharedObject方法，将某个具体对象添加为共享对象，可以全局进行使用，以下是代码示例：
	 * 
	 * public void configEngine(Engine me) { me.addSharedObject("RESOURCE_HOST",
	 * "http://res.jfinal.com"); me.addSharedObject("sk", new
	 * com.jfinal.kit.StrKit()); }
	 * 以上代码中的第二行，添加了一个名为RESOURCE_HOST的共享对象，而第三行代码添加了一个名为sk的共享对象，以下是在模板中的使用例子：
	 * 
	 * <img src="#(RESOURCE_HOST)/img/girl.jpg" /> #if(sk.isBlank(title)) ... #end
	 * 以上代码第一行中使用输出指令输出了RESOUCE_HOST这个共享变量，对于大型web应用系统，通过这种方式可以很方便地规划资源文件所在的服务器。
	 * 以上第二行代码调用了名为sk这个共享变量的isBlank方法，使用方式符合开发者直觉。
	 * 
	 * 注意：由于对象被全局共享，所以需要注意线程安全问题，尽量只共享常量以及无状态对象。
	 */

	/*
	 * 70.Extension Method扩展 Extension Method
	 * 用于对已存在的类在其外部添加扩展方法，该功能类似于ruby语言中的mixin特性。
	 * 
	 * JFinal Template Engine 默认已经为String、Integer、Long、Float、Double、Short、Byte
	 * 这七个基本的 java
	 * 类型，添加了toInt()、toLong()、toFloat()、toDouble()、toBoolean()、toShort()、toByte()
	 * 七个extension method。以下是使用示例：
	 * 
	 * #set(age = "18") #if(age.toInt() >= 18) join to the party #end
	 * 上例第一行代码中的age为String类型，由于String被添加了toInt()扩展方法，所以调用其toInt()
	 * 方法后便可与后面的Integer型的值18进行比较运算。
	 * 
	 * Extension
	 * Method特性有两大应用场景，第一个应用场景是为java类库中的现存类进行功能性扩展，下面给出一个为Integer添加扩展方法的例子：
	 * 
	 * public class MyIntegerExt { public Integer square(Integer self) { return self
	 * * self; }
	 * 
	 * public Double power(Integer self, Double exponent) { return Math.pow(self,
	 * exponent); }
	 * 
	 * public Boolean isOdd(Integer self) { return self % 2 != 0; } }
	 * 如上面代码所示，由于是对Integer进行扩展，所以上述三个扩展方法中的第一个参数必须是Integer类型，以便调用该方法时让这个参数承载调用者自身。
	 * 其它参数可以是任意类型，例如上述power方法中的exponent参数。扩展方法至少要有一个参数。以下代码是对上述扩展方法进行配置：
	 * 
	 * Engine.addExtensionMethod(Integer.class, MyIntegerExt.class);
	 * 上述代码第一个参数Integer是被扩展的类，第二个参数MyIntegerExt是扩展类，通过上面简单的两步，即可在模板中使用：
	 * 
	 * #set(num = 123) #(num.square()) #(num.power(10)) #(num.isOdd())
	 * 上述代码第二、三、四行调用了MyIntegerExt中的三个扩展方法，分别实现了对123的求平方、求10次方、判断是否为奇数的功能。
	 * 
	 * 如上例所示，extension method可以在类自身以外的地方对其功能进行扩展，在模板中使用时的书写也变得非常方便。
	 * 
	 * Extension Method
	 * 的另一个重要的应用场景，是将不确定类型变得确定，例如Controller.keepPara()方法会将所有参数当成String的类型来keep住，
	 * 所以表单中的Integer类型在keepPara()后变成了String型，引发模板中的类型不正确异常，以下是解决方案：
	 * 
	 * <select name="type"> #for(x : list) <option value="#(x.type)" #if(x.type ==
	 * type.toInt()) selected #end /> #end </select> 假定type为Integer类型，上面的
	 * select域在表单提交后，如果在后端使用了keepPara()，那么再次渲染该模板时type会变为String类型，从而引发类型不正确异常，通过type
	 * .toInt()即可解决。当然，你也可以使用keepPara(Integer.class, “type”) 进行解决。
	 */

	/*
	 * 71.Spring boot 整合 1、maven 坐标 Spring 整合可以在pom.xml中配置 jfinal 坐标，也可以配置 Enjoy
	 * Template Engine 的独立发布版本坐标，其 maven 坐标如下：
	 * 
	 * <dependency> <groupId>com.jfinal</groupId> <artifactId>enjoy</artifactId>
	 * <version>4.9.02</version> </dependency> JFinal Template Engine 的独立发布版本 Enjoy
	 * 只有 207K 大小，并且无任何第三方依赖。
	 * 
	 * 
	 * 
	 * 3、Spring Boot 整合 Spring boot 下整合配置如下：
	 * 
	 * @Configuration public class SpringBootConfig {
	 * 
	 * @Bean(name = "jfinalViewResolver") public JFinalViewResolver
	 * getJFinalViewResolver() {
	 * 
	 * // 创建用于整合 spring boot 的 ViewResolver 扩展对象 JFinalViewResolver jfr = new
	 * JFinalViewResolver();
	 * 
	 * // 对 spring boot 进行配置 jfr.setSuffix(".html");
	 * jfr.setContentType("text/html;charset=UTF-8"); jfr.setOrder(0);
	 * 
	 * // 获取 engine 对象，对 enjoy 模板引擎进行配置，配置方式与前面章节完全一样 Engine engine =
	 * JFinalViewResolver.engine;
	 * 
	 * // 热加载配置能对后续配置产生影响，需要放在最前面 engine.setDevMode(true);
	 * 
	 * // 使用 ClassPathSourceFactory 从 class path 与 jar 包中加载模板文件
	 * engine.setToClassPathSourceFactory();
	 * 
	 * // 在使用 ClassPathSourceFactory 时要使用 setBaseTemplatePath // 代替
	 * jfr.setPrefix("/view/") engine.setBaseTemplatePath("/view/");
	 * 
	 * // 添加模板函数 engine.addSharedFunction("/common/_layout.html");
	 * engine.addSharedFunction("/common/_paginate.html");
	 * 
	 * // 更多配置与前面章节完全一样 // engine.addDirective(...) // engine.addSharedMethod(...);
	 * 
	 * return jfr; } } 如上所示，jfr.setToClassPathSourceFactory() 配置的
	 * ClassPathSourceFactory 将从class path和jar包中加载模板文件。jfr.addSharedFunction(...)
	 * 配置共享模板函数。其上有关 enjoy 的配置本质上与对 Engine 对象的配置是一致的
	 * 
	 * 如果从项目的 webapp 路径下加载模板文件则无需配置为 ClassPathSourceFactory。
	 * 
	 * 
	 * 
	 * 2、Spring MVC 整合 在 Spring mvc下整合 Enjoy 非常简单，只需要配置一个 bean 即可，如下是具体配置方式：
	 * 
	 * <bean id="viewResolver"
	 * class="com.jfinal.template.ext.spring.JFinalViewResolver"> <!-- 是否热加载模板文件 -->
	 * <property name="devMode" value="true"/> <!-- 配置shared function，多文件用逗号分隔 -->
	 * <property name="sharedFunction"
	 * value="/view/_layout.html, /view/_paginate.html"/>
	 * 
	 * <!-- 是否支持以 #(session.value) 的方式访问 session --> <property name="sessionInView"
	 * value="true"/> <property name="prefix" value="/view/"/> <property
	 * name="suffix" value=".html"/> <property name="order" value="1"/> <property
	 * name="contentType" value="text/html; charset=utf-8"/> </bean>
	 * 更多、更详细的配置项及其说明，可以通过查看 JFinalViewResolver 头部的注释来了解，在绝大部分情况下，上面的配置项可以满足需求。
	 */

	/*
	 * 72.任意环境下使用Engine Enjoy Template Engine 的使用不限于 web，可以使用在任何 java 开发环境中。Enjoy
	 * 常被用于代码生成、email 生成、模板消息生成等具有模板特征数据的应用场景，使用方式极为简单。
	 * 
	 * 由于很多同学提出要在非 jfinal 环境下使用 Enjoy，所以 Enjoy 模板引擎发布了独立版本，maven 坐标如下：
	 * 
	 * <dependency> <groupId>com.jfinal</groupId> <artifactId>enjoy</artifactId>
	 * <version>4.9.02</version> </dependency> 独立引入 enjoy 项目，可以用于任何 java 项目中。
	 * 
	 * 1､Engine 与 Template Engine 是使用 Enjoy 的配置入口和使用入口，主要功能之一是配置 Enjoy 各种参数，其二是通过
	 * getTemplate、getTemplateByString 方法获取到 Template 对象，例如：
	 * 
	 * Engine engine = Engine.use();
	 * 
	 * engine.setDevMode(true); engine.setToClassPathSourceFactory();
	 * 
	 * engine.getTemplate("index.html"); Template 代表对模板的抽象，可以调用其 render
	 * 系方法对模板进行渲染，例如：
	 * 
	 * Kv kv = Kv.by("key", 123); Template template =
	 * engine.getTemplate("index.html");
	 * 
	 * // 字节流模式输出到 OutputStream ByteArrayOutputStream baos = new
	 * ByteArrayOutputStream(); template.render(kv, baos);
	 * 
	 * // 字符流模式输出到 StringWriter StringWriter sw = new StringWriter();
	 * template.render(kv, sw);
	 * 
	 * // 直接输出到 String 变量 String str = template.renderToString(kv); 以上代码中的
	 * template.render(kv. baos) 将数据渲染到 OutputStream 用于 web 项目可极大提升性能。后续的两种 render
	 * 方式分别将渲染结果输出到 Writer 与 String 变量中，方便 "非 web" 项目中使用。
	 * 
	 * 以上代码中的 render 系列方法的第一个参数类型为 Map。Kv 继承自 Map，并添加了一些提升用户体验的方法，可用于替代使用 Map 的场景。
	 * 
	 * 2、基本用法 直接举例：
	 * 
	 * Engine.use().getTemplate("demo.html").renderToString(Kv.by("k", "v"));
	 * 一行代码搞定模板引擎在任意环境下的使用，将极简贯彻到底。上例中的use()方法将从Engine中获取默认存在的main
	 * Engine对象，然后通过getTemplate获取Template对象，最后再使用renderToString将模板渲染到String之中。
	 * 
	 * 3、进阶用法 直接举例：
	 * 
	 * Engine engine = Engine.create("myEngine"); engine.setDevMode(true);
	 * engine.setToClassPathSourceFactory(); Template template =
	 * engine.getTemplate("wxAppMsg.txt"); String wxAppMsg =
	 * template.renderToString(Kv.by("toUser", "james"));
	 * 
	 * engine = Engine.use("myEngine"); 上例第一行代码创建了名为 "myEngine"
	 * 的Engine对象，第二行代码设置了热加载模板文件，第三行代码设置引擎从 class path 以及 jar
	 * 包中加载模板文件，第四行代码利用wxAppMsg.txt这个模板创建一个Template对象，第五行代码使用该对象渲染内容到String对象中，
	 * 从而生成了微信小程序消息内容。
	 * 
	 * 注意，最后一行代码使用use方法获取到了第一行代码创建的engine对象，
	 * 意味着使用正确的engineName可以在任何地方获取到之前创建的engine对象，极为方便。
	 * 
	 * 除了可以将模板渲染到String中以外，还可以渲染到任意的Writer之中，只需要用一下Template.render(Map data,
	 * java.io.Writer wirter)方法即可实现，例如：Writer接口如果指向文件，那么就将其内容渲染到文件之中，
	 * 甚至可以实现Writer接口将内容渲染到socket套接字中。
	 * 
	 * 除了外部模板文件可以作为模板内容的来源以外，还可以通过String数据或者IStringSource接口实现类作为模板数据的来源，以下是代码示例：
	 * 
	 * Template template = engine.getTemplateByString("#(x + 123)"); String result =
	 * template.renderToString(Kv.by("x", 456));
	 * 
	 * template = engine.getTemplate(new MySource()); result =
	 * template.renderToString(Kv.by("k", "v")); 上例代码第一行通过 getTemplateByString 来获取
	 * Template 对象，而非从外部模板文件来获取，这种用法非常适合模板内容非常简短的情况，避免了创建外部模板文件，例如：非常适合用于替换 JDK 中的
	 * String.format(…) 方法。
	 * 
	 * 上例中的第三行代码，传入的参数是new
	 * MySource()，MySource类是ISource接口的实现类，通过该接口可以实现通过任意方式来获取模板内容，例如，通过网络socket连接来获取，
	 * ISource接口用法极其简单，在此不再赘述。
	 * 
	 * 4、Engine对象管理 Engine对象的创建方式有两种，一种是通过 Engine.create(name) 方法，另一种是直接使用 new
	 * Engine() 语句，前者创建的对象是在 Engine 模块管辖之内，可以通过 Engine.use(name) 获取到，而后者创建的对象脱离了
	 * Engine 模块管辖，无法通过 Engine.use(name) 获取到，开发者需要自行管理。
	 * 
	 * JFinal 的 render 模块以及 activerecord 模块使用 new Engine() 创建实例，无法通过
	 * Engine.use(name) 获取到，前者可以通过RenderManager.me().getEngine() 获取到，后者可以通过
	 * activeRecordPlugin.getEngine() 获取到。
	 * 
	 * Engine对象管理的设计，允许在同一个应用程序中使用多个 Engine 实例用于不同的用途，JFinal 自身的 render、activerecord
	 * 模块对 Engine 的使用就是典型的例子。
	 * 
	 * 强烈建议加入 JFinal 俱乐部，获取极为全面的 jfinal 最佳实践项目源代码 jfinal-club，项目中包含大量的
	 * 模板引擎使用实例，可以用最快的速度，几乎零成本的轻松方式，掌握 JFinal Template
	 * Engine最简洁的用法，省去看文档的时间：http://www.jfinal.com/club
	 */

	/*
	 * 73.EhCachePlugin：概述
	 * EhCachePlugin是JFinal集成的缓存插件，通过使用EhCachePlugin可以提高系统的并发访问速度。
	 */

	/*
	 * 75.CacheInterceptor
	 * CacheInterceptor可以将action所需数据全部缓存起来，下次请求到来时如果cache存在则直接使用数据并render，
	 * 而不会去调用action。此用法可使action完全不受cache相关代码所污染，即插即用，以下是示例代码：
	 * 
	 * @Before(CacheInterceptor.class) public void list() { List<Blog> blogList =
	 * Blog.dao.find("select * from blog"); User user =
	 * User.dao.findById(getParaToInt()); setAttr("blogList", blogList);
	 * setAttr("user", user); render("blog.html"); }
	 * 上例中的用法将使用actionKey作为cacheName，在使用之前需要在ehcache.xml中配置以actionKey命名的cache如：<
	 * cache name="/blog/list"
	 * …>，注意actionKey作为cacheName配置时斜杠”/”不能省略。此外CacheInterceptor还可以与CacheName
	 * 注解配合使用，以此来取代默认的actionKey作为cacheName，以下是示例代码：
	 * 
	 * @Before(CacheInterceptor.class)
	 * 
	 * @CacheName("blogList") public void list() { List<Blog> blogList =
	 * Blog.dao.find("select * from blog"); setAttr("blogList", blogList);
	 * render("blog.html"); } 以上用法需要在ehcache.xml中配置名为blogList的cache如：<cache
	 * name="blogList" …>。
	 */

	/*
	 * 76.EvictInterceptor EvictInterceptor可以根据CacheName注解自动清除缓存。以下是示例代码：
	 * 
	 * @Before(EvictInterceptor.class)
	 * 
	 * @CacheName("blogList") public void update() { getModel(Blog.class).update();
	 * redirect("blog.html"); } 上例中的用法将清除cacheName为blogList的缓存数据，
	 * 与其配合的CacheInterceptor会自动更新cacheName为blogList的缓存数据。
	 * 
	 * jfinal 3.6 版本支持 @CacheName 参数使用逗号分隔多个 cacheName，方便针对多个 cacheName 进行清除，例如：
	 * 
	 * @Before(EvictInterceptor.class)
	 * 
	 * @CacheName("blogList, hotBlogList") // 逗号分隔多个 cacheName public void update()
	 * { ... }
	 */

	/*
	 * 77.CacheKit CacheKit是缓存操作工具类，以下是示例代码：
	 * 
	 * public void list() { List<Blog> blogList = CacheKit.get("blog", "blogList");
	 * if (blogList == null) { blogList = Blog.dao.find("select * from blog");
	 * CacheKit.put("blog", "blogList", blogList); } setAttr("blogList", blogList);
	 * render("blog.html"); } CacheKit 中最重要的两个方法是get(String cacheName, Object
	 * key)与put(String cacheName, Object key, Object
	 * value)。get方法是从cache中取数据，put方法是将数据放入cache。参数cacheName与ehcache.xml中的<cache
	 * name="blog" …>name属性值对应；参数key是指取值用到的key；参数value是被缓存的数据。
	 * 
	 * 以下代码是CacheKit中重载的CacheKit.get(String, String, IDataLoader)方法使用示例：
	 * 
	 * public void list() { List<Blog> blogList = CacheKit.get("blog", "blogList",
	 * new IDataLoader(){ public Object load() { return
	 * Blog.dao.find("select * from blog"); }}); setAttr("blogList", blogList);
	 * render("blog.html"); }
	 * CacheKit.get方法提供了一个IDataLoader接口，该接口中的load()方法在缓存值不存在时才会被调用。该方法的具体操作流程是：
	 * 首先以cacheName=blog以及key=blogList为参数去缓存取数据，如果缓存中数据存在就直接返回该数据，不存在则调用IDataLoader.
	 * load()方法来获取数据。
	 */

	/*
	 * 78.ehcache.xml简介
	 * EhCache的使用需要有ehcache.xml配置文件支持，该配置文件中配置了很多cache节点，每个cache节点会配置一个name属性，例如：<
	 * cache name="blog"
	 * …>，该属性是CacheKit取值所必须的。其它配置项如eternal、overflowToDisk、timeToIdleSeconds、
	 * timeToLiveSeconds详见EhCache官方文档。
	 */

	/*
	 * 79.RedisPlugin概述 RedisPlugin是支持
	 * Redis的极速化插件。使用RedisPlugin可以极度方便的使用redis，该插件不仅提供了丰富的API，而且还同时支持多redis服务端。
	 * Redis拥有超高的性能，丰富的数据结构，天然支持数据持久化，是目前应用非常广泛的nosql数据库。对于redis的有效应用可极大提升系统性能，
	 * 节省硬件成本。
	 */

	/*
	 * 81.Redis与Cache Redis与Cache联合起来可以非常方便地使用Redis服务，Redis对象通过use()方法来获取到Cache对象，
	 * Cache对象提供了丰富的API用于使用Redis服务，下面是具体使用示例：
	 * 
	 * public void redisDemo() { // 获取名称为bbs的Redis Cache对象 Cache bbsCache =
	 * Redis.use("bbs"); bbsCache.set("key", "value"); bbsCache.get("key");
	 * 
	 * // 获取名称为news的Redis Cache对象 Cache newsCache = Redis.use("news");
	 * newsCache.set("k", "v"); newsCache.get("k");
	 * 
	 * // 最先创建的Cache将成为主Cache，所以可以省去cacheName参数来获取 bbsCache = Redis.use(); //
	 * 主缓存可以省去cacheName参数 bbsCache.set("jfinal", "awesome"); }
	 * 以上代码中通过”bbs”、”news”做为use方法的参数分别获取到了两个Cache对象，使用这两个对象即可操作其所对应的Redis服务端。
	 * 
	 * 通常情况下只会创建一个RedisPlugin连接一个redis服务端，使用Redis.use().set(key,value)即可。
	 * 
	 * 注意：使用 incr、incrBy、decr、decrBy 方法操作的计数器，需要使用 getCounter(key) 进行读取而不能使用
	 * get(key)，否则会抛反序列化异常。同理：incrBy(key, value) 操作不能使用 set(key, value) 。
	 */

	/*
	 * 82.非web环境使用RedisPlugin
	 * RedisPlugin也可以在非web环境下使用，只需引入jfinal.jar然后多调用一下redisPlugin.start()即可，以下是代码示例：
	 * 
	 * public class RedisTest { public static void main(String[] args) { RedisPlugin
	 * rp = new RedisPlugin("myRedis", "localhost"); // 与web下唯一区别是需要这里调用一次start()方法
	 * rp.start();
	 * 
	 * Redis.use().set("key", "value"); Redis.use().get("key"); } }
	 */

	/*
	 * 83.Cron4jPlugin概述
	 * Cron4jPlugin是JFinal集成的任务调度插件，通过使用Cron4jPlugin可以使用通用的cron表达式极为便利的实现任务调度功能。
	 */

	/*
	 * 85.使用外部配置文件
	 * 上一个示例仅展示了java硬编码式的配置，更多的应用场景是使用外部配置文件，灵活配置调度策略，以便于随时改变调度策略，如下是外部配置的代码示例：
	 * 
	 * cron4j=task1, task2 task1.cron=* * * * * task1.class=com.xxx.TaskAaa
	 * task1.daemon=true task1.enable=true
	 * 
	 * task2.cron=* * * * * task2.class=com.xxx.TaskBbb task2.daemon=true
	 * task2.enable=false
	 * 上例中的cront4j是所谓的配置名称：configName，可以随便取名，这个名称在创建Cron4jPlugin对象时会被用到，
	 * 如果创建Cron4jPlugin对象时不提供名称则默认值为 "cron4j"。
	 * 
	 * 上例中的configName后面紧跟着的是task1、task2，表示当前配置的两个task
	 * 的名称，这两个名称规定了后续的配置将以其打头，例如后面的task1.cron、task2.cron都是以这两个task名称打头的。
	 * 
	 * 上例中的task1.cron是指该task的cron表达式，task1.class是指该task要调度的目标java类，
	 * 该java类需要实现Runnable接口，task1.daemon是指被调度的任务线程是否为守护线程，task1.enable是指该task是开启还是停用
	 * ，这个配置不是必须的，可以省略，省略时默认表示开启。同理task2的配置与task1的意义相同，只是taskName不同。
	 * 
	 * 总结一下：configName指定taskName，多个taskName可以逗号分隔，而每个taskName指定具体的task，
	 * 每个具体的task有四项配置：cron、class、deamon、enable，每个配置以taskName打头。
	 * 
	 * 假定配置文件名为config.txt，配置完成以后Cron4jPlugin的创建方式可以如下：
	 * 
	 * cp = new Cron4jPlugin("config.txt"); cp = new Cron4jPlugin("config.txt",
	 * "cron4j");
	 * 
	 * cp = new Cron4jPlugin(PropKit.use("config.txt")); cp = new
	 * Cron4jPlugin(PropKit.use("config.txt"), "cron4j");
	 * 
	 * me.add(cp);
	 * 以上代码中，前四行代码是利用配置文件创建Cron4jPlugin对象的四种方式，第一行代码只传入了配置文件名，省去了configName，那么默认值为“
	 * cron4j”。第二代码与第一行代码本质一样，只是指定了其configName。第三与第四行代码与前两行代码类似，仅仅是利用PropKit对其进行了加载。
	 * 
	 * 请注意：这里所说的configName，就是前面示例中配置项 cron4j=task1, task2 中的
	 * "cron4j"，这个configName相当于就是Cron4jPlugin寻找的配置入口。
	 */

	/*
	 * 86.高级用法
	 * 除了可以对实现了Runnable接口的java类进行调度以外，还可以直接调度外部的应用程序，例如windows或linux下的某个可执行程序，
	 * 如下是代码示例：
	 * 
	 * String[] command = { "C:\\tomcat\\bin\\catalina.bat", "start" }; String[]
	 * envs = { "CATALINA_HOME=C:\\tomcat", "JAVA_HOME=C:\\jdks\\jdk5" }; File
	 * directory = "C:\\MyDirectory"; ProcessTask task = new ProcessTask(command,
	 * envs, directory);
	 * 
	 * cron4jPlugin.addTask(task); me.add(cron4jPlugin);
	 * 
	 * 如上所示，只需要创建一个ProcessTask对象，并让其指向某个应用程序，再通过addTask添加进来，就可以实现对其的调度，
	 * 这种方式实现类似于每天半夜备份服务器数据库并打包成zip的功能，变得极为简单便捷。更加详细的用法，可以看一下Cron4jPlugin.
	 * java源代码中的注释。
	 */

	/*
	 * 87.Validator 概述 Validator是JFinal校验组件，在Validator类中提供了非常方便的校验方法，学习简单，使用方便。
	 */

	/*
	 * 88.Validator 1､基本用法
	 * Validator自身实现了Interceptor接口，所以它也是一个拦截器，配置方式与拦截器完全一样。以下是Validator示例：
	 * 
	 * public class LoginValidator extends Validator { protected void
	 * validate(Controller c) { validateRequiredString("name", "nameMsg", "请输入用户名");
	 * validateRequiredString("pass", "passMsg", "请输入密码"); } protected void
	 * handleError(Controller c) { c.keepPara("name"); c.render("login.html"); } }
	 * protected void validator(Controller
	 * c)方法中可以调用validateXxx(…)系列方法进行后端校验，protected void handleError(Controller
	 * c)方法中可以调用c.keepPara(…)方法将提交的值再传回页面以便保持原先输入的值，还可以调用c.render(…)方法来返回相应的页面。
	 * 注意handleError(Controller c)只有在校验失败时才会调用。
	 * 
	 * 以上代码handleError方法中的keepXxx方法用于将页面表单中的数据保持住并传递回页，以便于用户无需再重复输入已经通过验证的表单域。
	 * 
	 * 如果传递过来的是 model 对象，可以使用keepModel(...) 方法来保持住用户输入过的数据。同理，如果传递过来的是传统 java bean
	 * 对象，可以使用 keepBean(...) 方法来保持住用户输入过的数据。
	 * 
	 * keepPara(…) 方法默认将所有数据keep成String类型传给客户端，如果希望keep成为特定的类型，使用keepPara(Class, …)
	 * 即可，例如：keepPara(Integer.class, “age”)。
	 * 
	 * 注意：如果keepPara() 造成模板中出现类型相关异常，解决方法参见Template Engine这章的Extension Method小节。
	 * 
	 * 
	 * 
	 * 2、setRet(...) 与 getRet() jfinal 4.0 版本新增了 setRet(Ret) 方法与 getRet() 方法，典型示例如下：
	 * 
	 * public class LoginValidator extends Validator {
	 * 
	 * protected void validate(Controller c) { setRet(Ret.fail());
	 * 
	 * validateRequired("userName", "msg", "邮箱不能为空"); validateEmail("userName",
	 * "msg", "邮箱格式不正确"); validateRequired("password", "msg", "密码不能为空");
	 * validateCaptcha("captcha", "msg", "验证码不正确"); }
	 * 
	 * protected void handleError(Controller c) { c.renderJson(getRet()); } }
	 * 如上例所示，setRet(Ret.fail()) 将向 LoginValidator 注入一个 Ret 对象，从而后续的 validateRequired
	 * 等等 validate 方法会将所有验证结果存放在该 Ret 对象之中。
	 * 
	 * 然后，在 handleError 中的 c.renderJson( getRet() ) 这行代码就可以通过 getRet() 拿到前面注入的 Ret
	 * 对象，然后再进行 renderJson(ret)
	 * 
	 * 这样使用的好处是与 controller 层的 renderJson(Ret) 用法统一起来，因为你的 controller 中可能是这样用的：
	 * 
	 * public void login() { Ret ret = loginService.login(...); renderJson(ret); }
	 * 上面代码中的 loginService.login(...) 返回的 ret 对象与 LoginValidator 中的 ret 对象统一使用
	 * renderJson(ret) 以后，前端的 JavaScript 对其的处理方式就可以完全统一。
	 * 
	 * 
	 * 
	 * 3、高级用法 虽然 Validator 中提供了丰富的 validateXxx(...) 系列方法，但毕竟方法个数是有限的，当
	 * validateXxx(...) 系列的方法不能满足需求时，除了可以用 validateRegex(...) 定制正则表达式来满足需求以外，还可以通过普通
	 * java 代码来实现，例如：
	 * 
	 * protected void validate(Controller c) { String nickName =
	 * c.getPara("nickName"); if (userService.isExists(nickName)) { addError("msg",
	 * "昵称已被注册，请使用别的昵称！")； } } 如上代码所示，只需要利用普通的 java 代码配合一个 addError(...)
	 * 方法就可以无限制、灵活定制验证功能。
	 * 
	 * 这里特别强调：addError(...) 方法是自由定制验证的关键。
	 * 
	 * 
	 * 
	 * 此外，Validator
	 * 在碰到验证失败项时，默认会一直往下验证所有剩下的验证项，如果希望程序在碰到验证失败项时略过后续验证项立即返回，可以通过如下代码来实现：
	 * 
	 * protected void validate(Controller c) { this.setShortCircuit(true); .... }
	 * setShortCircuit(boolean) 用于设置验证方式是否为 “短路型验证”。
	 */

	/*
	 * 89.Validator配置 Validator配置方式与拦截器完全一样，见如下代码：
	 * 
	 * public class UserController extends Controller {
	 * 
	 * @Before(LoginValidator.class) // 配置方式与拦截器完全一样 public void login() { } }
	 */

	/*
	 * 90.国际化 概述 JFinal 为国际化提供了极速化的支持，国际化模块仅三个类文件，使用方式要比spring这类框架容易得多。
	 */

	/*
	 * 91.I18n与Res
	 * I18n对象可通过资源文件的baseName与locale参数获取到与之相对应的Res对象，Res对象提供了API用来获取国际化数据。
	 * 
	 * 以下给出具体使用步骤：
	 * 
	 * 创建i18n_en_US.properties、i18n_zh_CN.properties资源文件，i18n即为资源文件的baseName，可以是任意名称
	 * ，在此示例中使用”i18n”作为baseName
	 * 
	 * i18n_en_US.properties文件中添加如下内容
	 * 
	 * msg=Hello {0}, today is{1}.
	 * 
	 * i18n_zh_CN.properties文件中添加如下内容
	 * 
	 * msg=你好{0}, 今天是{1}.
	 * 
	 * 在YourJFinalConfig中使用me.setI18nDefaultBaseName("i18n")配置资源文件默认baseName
	 * 
	 * 特别注意，java国际化规范要求properties文件的编辑需要使用专用的编辑器，否则会出乱码，常用的有Properties
	 * Editor，在此可以下载：http://www.oschina.net/p/properties+editor
	 * 
	 * 
	 * 
	 * 以下是基于以上步骤以后的代码示例：
	 * 
	 * // 通过locale参数en_US得到对应的Res对象 Res resEn = I18n.use("en_US"); // 直接获取数据 String
	 * msgEn = resEn.get("msg"); // 获取数据并使用参数格式化 String msgEnFormat =
	 * resEn.format("msg", "james", new Date());
	 * 
	 * // 通过locale参数zh_CN得到对应的Res对象 Res resZh = I18n.use("zh_CN"); // 直接获取数据 String
	 * msgZh = resZh.get("msg"); // 获取数据并使用参数格式化 String msgZhFormat =
	 * resZh.format("msg", "詹波", new Date());
	 * 
	 * // 另外,I18n还可以加载未使用me.setI18nDefaultBaseName()配置过的资源文件，唯一的不同是 //
	 * 需要指定baseName参数，下面例子需要先创建otherRes_en_US.properties文件 Res otherRes =
	 * I18n.use("otherRes", "en_US"); otherRes.get("msg");
	 * 
	 * 
	 * 以下是在 jfinal template engine 中的使用示例：
	 * 
	 * #(_res.get("msg")) 注意，上面的用法需要添加 I18nInterceptor，将在后面一小节进行介绍。
	 */

	/*
	 * 92.I18nInterceptor
	 * I18nInterceptor拦截器是针对于web应用提供的一个国际化组件，以下是在freemarker模板中使用的例子：
	 * 
	 * //先将I18nInterceptor配置成全局拦截器 public void configInterceptor(Interceptors me) {
	 * me.add(new I18nInterceptor()); }
	 * 
	 * // 然后在 jfinal 模板引擎中即可通过 _res 对象来获取国际化数据 #(_res.get("msg"))
	 * 以上代码通过配置了I18nInterceptor拦截action请求，然后即可在freemarker模板文件中通过名为_res对象来获取国际化数据，
	 * I18nInterceptor的具体工作流程如下：
	 * 
	 * 试图从请求中通过controller.getPara(“_locale”)获取locale参数，如果获取到则将其保存到cookie之中
	 * 
	 * 如果controller.getPara(“_locale”)没有获取到参数值，则试图通过controller.getCookie(“_locale”)
	 * 得到locale参数
	 * 
	 * 如果以上两步仍然没有获取到locale参数值，则使用I18n. defaultLocale的值做为locale值来使用
	 * 
	 * 使用前面三步中得到的locale值，通过I18n.use(locale)得到Res对象，并通过controller.setAttr(“_res”,
	 * res)将Res对象传递给页面使用
	 * 
	 * 如果I18nInterceptor. isSwitchView为true值的话还会改变render的view值，实现整体模板文件的切换，详情可查看源码。
	 * 
	 * 
	 * 
	 * 以上步骤I18nInterceptor中的变量名”_locale”、”_res”都可以在创建I18nInterceptor对象时进行指定，
	 * 不指定时将使用默认值。还可以通过继承I18nInterceptor并且覆盖getLocalPara、getResName、
	 * getBaseName来定制更加个性化的功能。
	 * 
	 * 在有些 web 系统中，页面需要国际化的文本过多，并且 css 以及 html
	 * 也因为国际化而大不相同，对于这种应用场景先直接制做多套同名称的国际化视图，并将这些视图以 locale
	 * 为子目录分类存放，最后使用I18nInterceptor拦截器根据 locale
	 * 动态切换视图，而不必对视图中的文本逐个进行国际化切换，只需将I18nInterceptor.isSwitchView设置为true即可，省时省力。
	 */

	/*
	 * 93.Json 转换 概述 jfinal 的 json 模块以抽象类 Json 为核心，方便扩展第三方实现，jfinal 官方给出了三个 Json
	 * 实现，分别是 JFinalJson、FastJson、Jackson，这三个实现继承自抽象类 Json。
	 * 
	 * 抽象类 Json 的核心抽象如下：
	 * 
	 * public abstract class Json { public abstract String toJson(Object object);
	 * public abstract <T> T parse(String jsonString, Class <T> type); } 如上代码可以看出
	 * Json 抽象就是 Object 与 json string 互转的两个方法，toJson(...)将任意 java 类型转成 json string，而
	 * parse 将 json string 再反向转成范型指定的对象。
	 */

	/*
	 * 97.JFinal架构及扩展 概述 JFinal
	 * 采用微内核全方位扩展架构，全方位是指其扩展方式在空间上的表现形式。JFinal由Handler、Interceptor、Controller、Render
	 * 、Plugin五大部分组成。本章将简单介绍此架构以及基于此架构所做的一些较为常用的扩展。
	 */

	/*
	 * 98.架构 JFinal 顶层架构图如下：
	 * 
	 * 
	 * 
	 * 17.png
	 */

	/*
	 * 99.升级到 4.9.02 极速升级 一、jfinal 3.0 之前版本的升级 jfinal 3.0 是大版本升级，此前版本升到 jfinal 3.0
	 * 请移步 14.2、14.3、14.4、14.5 小节，这几个小节中的内容极少，升级很方便。
	 * 
	 * 
	 * 
	 * 二、jfinal 3.0 之后版本的升级 1、升级到 3.1 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 2、升级到 3.2 IStringSource 更名为 ISource
	 * 
	 * 按照 14.2 小节 升级 Ret
	 * 
	 * 
	 * 
	 * 3、升级到 3.3 指令扩展中的 java.io.Writer 改为 com.jfinal.template.io.Writer，eclipse/IDEA
	 * 开发工具会主动给出提示
	 * 
	 * 
	 * 
	 * 4、升级到 3.4 由于 Json 中的 defaultDatePattern 初始值由 null 改为
	 * "yyyy-MM-dd HH:mm:ss"，JFinalJson 中删掉 datePattern 属性，所以要在
	 * configConstant(Constants me) 中配置：me.setJsonDatePattern(null) 或者具体值
	 * 
	 * 
	 * 
	 * 
	 * 5、升级到 3.5 ISource.getKey() 更名为 ISource.getCacheKey()
	 * 
	 * 
	 * 
	 * 6、升级到 3.6 Db、Model 针对多主键(联合主键)的 findById、deleteById 方法添加一个 's' 后缀，改成
	 * findByIds、deleteByIds
	 * 
	 * 用到 jfinal weixin 项目的 MsgController 时，需要在 configRoutes 中配置
	 * me.setMappingSuperClass(true)
	 * 
	 * 由于 jfinal 3.6 用于 sql 管理的 Engine 对象，默认配置了
	 * engine.setToClassPathSourceFactory()，engine 将从 class path 和 jar 包中加载 sql
	 * 文件，所以如果 sql 文件以往是存放在 src/main/webapp 的需要转移至 src/main/resources 之下。如果以往配置过
	 * arp.setBaseSqlTemplatePath(...)，需要删除该行代码，或改为适应于 ClassPathSourceFactory
	 * 的配置，参考：https://www.jfinal.com/doc/6-2
	 * 
	 * 
	 * 
	 * 7、升级到 3.8 用到 Aop 配置方法的改为使用 AopManager，例如：Aop.addMapping(...) 改为
	 * AopManager.me().addMapping(...)
	 * 
	 * 
	 * 
	 * 8、升级到 4.0 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 9、升级到 4.1 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 10、升级到 4.2 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 11、升级到 4.3 无需修改，平滑升级
	 * 
	 * 
	 * 12、升级到 4.4 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 13、升级到 4.5 默认不支持直接访问 .jsp 文件，如果需要直接访问 .jsp
	 * 文件，需添加配置：me.setDenyAccessJsp(false);
	 * 
	 * 
	 * 
	 * 14、升级到 4.6 无需修改，平滑升级
	 * 
	 * 
	 * 15、升级到 4.7 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 16、升级到 4.8 jfinal 4.8 之前的 Controller.getPara(String) 方法，在有表单域存在的时候就不可能返回 null
	 * 值，而是返回了 "" 值。jfinal 4.8 版本将之修改为与其它 getXxx 系方法一样，将 "" 处理为 null 值。
	 * 
	 * 需要快速升级老项目的同学，可以引入 BaseController 并使用老版本的实现：
	 * 
	 * public class BaseController extends Controller { public String getPara(String
	 * name) { return getRequest().getParameter(name); } } 这个方法的具体改变细节见 gitee.com ：
	 * https://gitee.com/jfinal/jfinal/commit/
	 * edfcc0015837ab0b1e6a1f980843ab88815ec1cd
	 * 
	 * 
	 * 
	 * 17、升级到 4.9 jfinal 4.9 对 JFinalJson.java 进行了重构，如果以前的项目通过继承 JFinalJson
	 * 类做过扩展，需要注意扩展接口有所变动，具体可以参考一下源码中有关扩展的注释，新的扩展接口使用起来比以前要方便得多。
	 * 
	 * 如无上述情况可平滑升级。
	 * 
	 * 
	 * 
	 * 17、升级到 4.9.01 无需修改，平滑升级
	 * 
	 * 
	 * 
	 * 
	 * 18、升级到 4.9.02 Engine 的 setWriterBufferSize(int) 更名为 setBufferSize(int)
	 */

	/*
	 * 100.Ret 如果待升级项目中未使用过 Ret，那么可以忽略本小节。如果是 jfinal 3.2、3.3、3.4 或者更高版本的 jfinal
	 * 也可以忽略。
	 * 
	 * JFinal 3.2
	 * 对Ret工具类进行了改进，使其更加适用于json数据格式交互的API类型项目。新版本状态名只有一个：state，取值为：ok/fail，
	 * 而老版本状态名有两个：isOk与isFail，取值为：true/false。
	 * 
	 * 所以，新旧版本Ret对象生成的json数据会有差异，对于大多数web项目来说，升级方法如下：
	 * 
	 * 利用查找替换功能将 html 与js文件中的 ret.isOk 替换为 ret.state == "ok"
	 * 
	 * 利用查找替换功能将 html 与js文件中的 ret.isFail 替换为 ret.state == "fail"
	 * 
	 * 
	 * 
	 * 如果希望尽可能小的改动代码进行升级，可以调用一次Ret.setToOldWorkMode()方法沿用老版本模式即可。
	 */

	/*
	 * 101.configEngine JFinal 3.0新增了模板引擎模块，继承JFinalConfig的实现类中需要添加public void
	 * configEngine(Engine me)方法，以便对模板引擎进行配置。以下是示例代码：
	 * 
	 * public void configEngine(Engine me) { me.setDevMode(true);
	 * 
	 * me.addSharedFunction("/view/common/layout.html");
	 * me.addSharedFunction("/view/common/paginate.html"); } 项目升级如果不使用Template
	 * Engine该方法可以留空。
	 * 
	 * JFinal 3.0 默认ViewType 为
	 * ViewType.JFINAL_TEMPLATE，如果老项目使用的是Freemarker模板，并且不希望改变模板类型，需要在 configConstant
	 * 方法中通过me.setViewType(ViewType.FREE_MARKER)进行指定，以前已经指定过 ViewType的则不必理会。
	 */

	/*
	 * 102.baseViewPath baseViewPath 设置由原来的configConstant(…) 方法中转移到了Routes
	 * 对象中，并且可以对不同的Routes对象分别设置，如下是示例：
	 * 
	 * public class FrontRoutes extends Routes {
	 * 
	 * public void config() { setBaseViewPath("/_view");
	 * 
	 * add("/", IndexController.class, "/index"); add("/project",
	 * ProjectController.class); } }
	 * 从configConstant(…)转移到configRoute(…)中的好处是可以分别对不同的Routes进行设置，
	 * 不同模块的baseViewPath很可能不相同，从而可以减少冗余代码。上面的代码示例是用于Routes拆分后的情况，
	 * 如果你的应用并没有对Routes进行拆分，只需要在configRoute 中如下配置即可：
	 * 
	 * public void configRoute(Routes me) { me.setBaseViewPath("/_view");
	 * 
	 * me.add("/", IndexController.class); }
	 */

	/*
	 * 103.RenderFactory JFinal 3.0 对 render
	 * 模块做了全面重构，抽取出了IRenderFactory接口，而原来的RenderFactory成为了接口的默认实现类，
	 * 去除了原来的IMainRenderFactory、IErrorRenderFactory、IXmlRenderFactory三个接口，所有对 render
	 * 的扩展与定制全部都可以通过继承RenderFactory来实现，3.0版本的render模块可对所有render进行切换与定制，并且扩展方式完全一致。
	 * 如果老项目对IMainRenderFactory做过扩展，只需要照如下方式进行升级：
	 * 
	 * public class MyRenderFactory extends RenderFactory { public Render
	 * getRender(String view) { return new MyRender(view); } } 同理，如果以前对
	 * IErrorRenderFactory 或者 IXmlRenderFactory
	 * 做过扩展的，只需要在上面的MyRenderFactory类中添加上getErrorRender(…) 与 getXmlRender(…)
	 * 方法即可。扩展完以后在configConstant 中进行如下配置：
	 * 
	 * public void configConstant(Constants me) { me.setRenderFactory(new
	 * MyRenderFactory()); } JFinal 3.0
	 * 对所有render扩展，采取了完全一致的扩展方式，学习成本更低，使用更方便，升级也很方便。此外，原来RenderFactory类中的me()
	 * 已经被取消，老项目对此有依赖的只需要将RenderFactory.me()
	 * 直接改为RenderManager.me().getRenderFactory() 即可。
	 */

	/*
	 * 104.其它
	 * Ret.put(…).put(…)这种链式用法，需要改成Ret.set(…).set(…)，因为Ret改为继承自HashMap，为了避免与HashMap.
	 * put(…)相冲突。Ret.get(…)方法返回泛型值的场景改为Ret.getAs(…)。
	 * 
	 * configConstant(…) 的Constants 参数中的setFreeMarkerExtension、setVelocityExtension
	 * 方法统一改为使用setViewExtension方法。setMainRenderFactory、
	 * setErrorRenderFactory被setRenderFactory取代。
	 * 
	 * renderXml(…)方法依赖的XmlRender由原来Freemarker语法实现改成了由JFinal Template
	 * Engine实现，用到renderXml(…)的项目需要修改模板内容。
	 * 
	 * 
	 * 
	 * JFinal 官方QQ群: 用”jfinal”关键字搜索QQ群
	 * 
	 * 
	 * 
	 * 强烈建议加入 JFinal 俱乐部，获取JFinal最佳实践项目源代码
	 * jfinal-club，以最快的速度、最轻松的方式掌握最简洁的用法，省去看文档的时间：http://www.jfinal.com/club
	 */
}
