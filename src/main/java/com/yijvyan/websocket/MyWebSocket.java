
/**  
 * @Title: MyWebSocket.java
 * @Package com.yijvyan.websocket
 * @Description: TODO(1.jfinal-undertow高级用法官方文档；)
 * @author Administrator
 * @date 2020-8-12 18:41:02 
 * @version V1.0  
 */

package com.yijvyan.websocket;

import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/**
 * @ClassName: MyWebSocket
 * @Description: TODO(2.一个关于webSocket开发的demo；)
 * @author Administrator
 * @date 2020-8-12
 *
 */
@ServerEndpoint("/myapp.ws")
public class MyWebSocket {

	@OnMessage
	public void message(String message, Session session) {
		System.out.println("13.3.websocket服务器接收到消息了......message：" + message);
		for (Session s : session.getOpenSessions()) {
			s.getAsyncRemote().sendText(message + "-->是猪！");
		}
	}

}
