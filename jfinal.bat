@echo off

rem 8.2.在项目根目录下面添加启动脚本；修改 MAIN_CLASS 变量值，即可投入使用。
rem -------------------------------------------------------------------------
rem
rem 使用说明：
rem
rem 1: 该脚本用于别的项目时只需要修改 MAIN_CLASS 即可运行
rem
rem 2: JAVA_OPTS 可通过 -D 传入 undertow.port 与 undertow.host 这类参数覆盖
rem    配置文件中的相同值此外还有 undertow.resourcePath, undertow.ioThreads
rem    undertow.workerThreads 共五个参数可通过 -D 进行传入
rem
rem 3: JAVA_OPTS 可传入标准的 java 命令行参数,例如 -Xms256m -Xmx1024m 这类常用参数
rem
rem
rem -------------------------------------------------------------------------
rem 8.2.2. 注意要首先根据项目入口类，修改一下上面内容中的 MAIN_CLASS 变量值，Windows 下的 jfinal.bat 脚本也同样如此；
rem windows 脚本的使用方法如下：启动项目命令：jfinal.bat start 关闭项目命令：jfinal.bat stop 重启项目命令：jfinal.bat restart
rem 8.2.3.特别注意：使用以上命令行时，先要使用 mvn clean package 将项目打包，然后使用 cd 命令跳转到打好的包的目录下面执行命令。而不是 cd 跳转到项目根目录下面去执行命令，很多人犯这种错误。
rem 注意：linux、mac 下的脚本文件换行字符必须是 '\n'，而 windows 下必须是 "\r\n"，否则脚本无法执行，并会输出无法理解的错误信息，难以排错。

setlocal & pushd


rem 启动入口类,该脚本文件用于别的项目时要改这里
set MAIN_CLASS=com.yijvyan.demo.DemoConfig

rem Java 命令行参数,根据需要开启下面的配置,改成自己需要的,注意等号前后不能有空格
rem set "JAVA_OPTS=-Xms256m -Xmx1024m -Dundertow.port=80 -Dundertow.host=0.0.0.0"
rem set "JAVA_OPTS=-Dundertow.port=80 -Dundertow.host=0.0.0.0"


if "%1"=="start" goto normal
if "%1"=="stop" goto normal
if "%1"=="restart" goto normal

goto error


:error
echo Usage: jfinal.bat start | stop | restart
goto :eof


:normal
if "%1"=="start" goto start
if "%1"=="stop" goto stop
if "%1"=="restart" goto restart
goto :eof


:start
set APP_BASE_PATH=%~dp0
set CP=%APP_BASE_PATH%config;%APP_BASE_PATH%lib\*
echo starting jfinal undertow
java -Xverify:none %JAVA_OPTS% -cp %CP% %MAIN_CLASS%
goto :eof


:stop
set "PATH=%JAVA_HOME%\bin;%PATH%"
echo stopping jfinal undertow
for /f "tokens=1" %%i in ('jps -l ^| find "%MAIN_CLASS%"') do ( taskkill /F /PID %%i )
goto :eof


:restart
call :stop
call :start
goto :eof

endlocal & popd
pause