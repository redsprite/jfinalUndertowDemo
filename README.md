# jfinalUndertowDemo
# 随便写写，jfinal官网文档的demo；
# 9.打包：打开命令行终端，cd 命令进入项目根目录，运行以下命令即可打包。mvn clean package
##  执行上述打包命令以后，在项目根下面的 target 目录下面会出现打好的 xxx.zip，解压该 zip 文件使用命令行 ./jfinal.sh start 即可运行。
##  除了 zip 文件还会在 target 下面生成一个目录，在该目录下面使用命令行 ./jfinal.sh start 可启动项目，该目录下面的内容与 zip 文件中的内容是完全一样的。

# 10.部署：将上述打包命令生成的 zip 文件上传到服务器并解压即完成了部署工作，基于 jfinal-undertow 开发项目的最大优势就是不需要下载、安装、配置 tomcat 这类 server。

# 14.3.试一下eclipse直接commitAndPush选项，看能不能成功。